package com.rtech.rparkout;

//import android.support.multidex.MultiDexApplication;

//import com.crashlytics.android.Crashlytics;
//import com.google.firebase.analytics.FirebaseAnalytics;
import androidx.multidex.MultiDexApplication;

import com.rtech.rparkout.component.util.PreferenceManager;

import java.net.URISyntaxException;

//import io.fabric.sdk.android.Fabric;
import io.socket.client.IO;
import io.socket.client.Socket;

//import com.crashlytics.android.Crashlytics;
//import io.fabric.sdk.android.Fabric;

/**
 * Created by Dhimas on 9/19/17.
 */

public class RTechOnStreet extends MultiDexApplication {
//    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onCreate() {
        super.onCreate();
//        Fabric.with(this, new Crashlytics());
        initPrefrences();
        // Obtain the FirebaseAnalytics instance.
//        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }

    private void initPrefrences() {
        new PreferenceManager(this);
    }

    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("https://livechat.pampasy.id");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public Socket getSocket() {
        return mSocket;
    }

//    public FirebaseAnalytics getFirebaseAnalytics(){
//        return mFirebaseAnalytics;
//    }
}
