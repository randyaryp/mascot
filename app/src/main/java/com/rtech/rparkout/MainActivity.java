package com.rtech.rparkout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.rtech.mascot.R;
import com.rtech.rparkout.component.network.NetworkManager;
import com.rtech.rparkout.component.network.NetworkService;
import com.rtech.rparkout.component.network.request.CarCallRequest;
import com.rtech.rparkout.component.network.response.CarCallResponse;
import com.rtech.rparkout.component.util.MethodUtil;
import com.rtech.rparkout.component.util.PreferenceManager;
import com.rtech.rparkout.modul.CommonInterface;
import com.rtech.rparkout.modul.carcall.CarCallPresenter;
import com.rtech.rparkout.modul.carcall.CarCallPresenterImpl;
import com.rtech.rparkout.modul.carcall.CarCallView;
import com.rtech.rparkout.modul.kendaraan.KendaraanFragment;
import com.rtech.rparkout.modul.menu.MenuActivity;
import com.rtech.rparkout.modul.posmasuk.PosMasukFragment;
import com.rtech.rparkout.modul.poskeluar.PosKeluarFragment;
import com.rtech.rparkout.modul.login.LoginActivity;
import com.rtech.rparkout.modul.navigation.DataModel;
import com.rtech.rparkout.modul.navigation.DrawerItemCustomAdapter;
import com.rtech.rparkout.modul.printersetting.PrinterSetting;
import com.rtech.rparkout.modul.setting.SettingPresenter;
import com.rtech.rparkout.modul.setting.SettingPresenterImpl;

public class MainActivity extends AppCompatActivity implements CommonInterface, CarCallView {
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private RelativeLayout mDrawerLinear;
    Toolbar toolbar;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    ActionBarDrawerToggle mDrawerToggle;

    private ImageView mainMenu;
    private ImageView notifications;
    public CarCallPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTitle = mDrawerTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.my_drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerLinear = findViewById(R.id.left_drawers);
        notifications = findViewById(R.id.notification_button);

        notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        mPresenter = new CarCallPresenterImpl(this, this);
        CarCallRequest carcallreq = new CarCallRequest(PreferenceManager.getSiteName(), PreferenceManager.getLoggedInLobbyId());
        mPresenter.carcall(carcallreq);

        setupDrawerToggle();
        DataModel[] drawerItem = new DataModel[5];
        drawerItem[0] = new DataModel("Pos Masuk");
        drawerItem[1] = new DataModel("Pos Keluar");
        drawerItem[2] = new DataModel("Printer Setting");
        drawerItem[3] = new DataModel("Kendaraan");
        drawerItem[4] = new DataModel("Logout");

        DrawerItemCustomAdapter adapter = new DrawerItemCustomAdapter(this, R.layout.list_view_item_row, drawerItem);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new MainActivity.DrawerItemClickListener());
        mDrawerLayout = (DrawerLayout) findViewById(R.id.my_drawer_layout);

        mainMenu = findViewById(R.id.mainMenu);
        mainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.openDrawer(mDrawerLinear);
            }
        });

        Intent intent = getIntent();
        String defaultFragment = PreferenceManager.getDefaultFragment();
        String jenisTransaksi = PreferenceManager.getJenisTransaksiLogin();
        if(defaultFragment != null && defaultFragment.equalsIgnoreCase("3")){
            PreferenceManager.removeDefaultFragment();
            FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.content_frame, new KendaraanFragment());
            tx.commit();
        }
        else if (jenisTransaksi != null && jenisTransaksi.equalsIgnoreCase("Transaksi Keluar")){
            FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.content_frame, new PosKeluarFragment());
            tx.commit();
        }
        else if (intent.getStringExtra("page").equalsIgnoreCase("poskeluar")){
            FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.content_frame, new PosKeluarFragment());
            tx.commit();
        }
        else if (intent.getStringExtra("page").equalsIgnoreCase("kendaraanlist")){
            FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.content_frame, new KendaraanFragment());
            tx.commit();
        }
        else if (intent.getStringExtra("page").equalsIgnoreCase("printersetting")){
            FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.content_frame, new PrinterSetting());
            tx.commit();
        }
        else{
            FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.content_frame, new PosMasukFragment());
            tx.commit();
        }
    }

    @Override
    public void showProgressLoading() {
        progressBar.show(this, "", false, null);
    }

    @Override
    public void hideProgresLoading() {
        progressBar.getDialog().dismiss();
    }

    @Override
    public NetworkService getService() {
        return NetworkManager.getInstance();
    }

    @Override
    public void onFailureRequest(String msg) {
        MethodUtil.showCustomToast(this, msg, R.drawable.ic_warning_red_24dp);
    }

    @Override
    public void onSuccessCarCall(CarCallResponse dev_response) {
        Log.i("kunam", "car call success");
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            selectItem(i);
            mDrawerList.deferNotifyDataSetChanged();
        }
    }

    private void selectItem(int position) {

        Fragment fragment = null;

        switch (position) {
            case 1:
                fragment = new PosKeluarFragment();
                break;
            case 2:
                fragment = new PrinterSetting();
                break;
            case 3:
                fragment = new KendaraanFragment();
                break;
            case 4:
                PreferenceManager.logOut();
                gotoLoginPage();
                break;
            default:
                fragment = new PosMasukFragment();
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            mDrawerLayout.closeDrawer(mDrawerLinear);

        } else {
            Log.e("MainActivity", "Error in creating fragment");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return false;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
//        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    void setupDrawerToggle(){
        mDrawerToggle = new ActionBarDrawerToggle(this,mDrawerLayout,toolbar,R.string.app_name, R.string.app_name);
    }

    private void gotoLoginPage() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}
