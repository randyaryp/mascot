package com.rtech.rparkout.modul.posmasuk;

import com.rtech.rparkout.component.network.response.SaveDataPosMasukRequest;
import com.rtech.rparkout.component.network.response.UploadImageRequest;

public interface PosMasukPresenter {
    void uploadImage(UploadImageRequest dev_id);
    void saveDataPosMasuk(SaveDataPosMasukRequest dev_id);
}
