package com.rtech.rparkout.modul.setting;

import com.rtech.rparkout.component.network.request.TicketNoteRequest;
import com.rtech.rparkout.component.network.response.CekDeviceRequest;
import com.rtech.rparkout.component.network.response.LobbyListRequest;
import com.rtech.rparkout.component.network.response.VehicleListRequest;

/**
 * Created by Dhimas on 12/22/17.
 */

public interface SettingPresenter {
    void cekDevice(CekDeviceRequest dev_id);
    void lobbyList(LobbyListRequest dev_id);
    void vehicleList(VehicleListRequest dev_id);
    void ticketNote(TicketNoteRequest dev_id);
    void vehicleType();
    void vehicleColor();
    void paymentType();
}
