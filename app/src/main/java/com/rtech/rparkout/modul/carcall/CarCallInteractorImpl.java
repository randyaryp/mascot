package com.rtech.rparkout.modul.carcall;

import com.rtech.rparkout.component.network.NetworkService;
import com.rtech.rparkout.component.network.request.CarCallRequest;
import com.rtech.rparkout.component.network.response.CarCallResponse;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class CarCallInteractorImpl implements CarCallInteractor {
    private NetworkService mService;

    public CarCallInteractorImpl(NetworkService service) {
        mService = service;
    }

    @Override
    public Observable<CarCallResponse> carcall(CarCallRequest carcall) {
        return mService.carcalllist(carcall)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io());
    }
}
