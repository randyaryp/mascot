package com.rtech.rparkout.modul.capture;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
//import android.support.v4.app.ActivityCompat;
//import android.support.v7.app.AlertDialog;
//import android.support.v7.app.AppCompatActivity;
import android.os.Handler;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

//import com.rtech.onstreet.R;

import com.google.android.gms.vision.CameraSource;
//import com.google.android.gms.samples.vision.ocrreader.ui.camera.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.rtech.mascot.R;
import com.rtech.rparkout.component.util.PreferenceManager;
import com.rtech.rparkout.modul.transaction.TransactionActivity;

import java.io.IOException;

public class CaptureActivity extends AppCompatActivity {

    CameraSource mCameraSource;
    SurfaceView mCameraView;
    TextView mTextView;
    Button capt;
    TextView hometoolbar_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture_small);

        // See if google play services are installed.
        boolean services = false;
        try
        {
            ApplicationInfo info = getPackageManager().getApplicationInfo("com.google.android.gms", 0);
            services = true;
        }
        catch(PackageManager.NameNotFoundException e)
        {
            services = false;
        }

        if (services)
        {
            // Ok, do whatever.
//            Toast.makeText(CaptureActivity.this, "Play Services are installed. Would start map now...", Toast.LENGTH_LONG).show();
//            return;
        }
        else
        {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(CaptureActivity.this);

            // set dialog message
            alertDialogBuilder
                    .setTitle("Google Play Services")
                    .setMessage("The map requires Google Play Services to be installed.")
                    .setCancelable(true)
                    .setPositiveButton("Install", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            dialog.dismiss();
                            // Try the new HTTP method (I assume that is the official way now given that google uses it).
                            try
                            {
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.google.android.gms"));
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                                intent.setPackage("com.android.vending");
                                startActivity(intent);
                            }
                            catch (ActivityNotFoundException e)
                            {
                                // Ok that didn't work, try the market method.
                                try
                                {
                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.android.gms"));
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                                    intent.setPackage("com.android.vending");
                                    startActivity(intent);
                                }
                                catch (ActivityNotFoundException f)
                                {
                                    // Ok, weird. Maybe they don't have any market app. Just show the website.

                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.google.android.gms"));
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                                    startActivity(intent);
                                }
                            }
                        }
                    })
                    .setNegativeButton("No",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            dialog.cancel();
                        }
                    })
                    .create()
                    .show();
        }



        mCameraView = findViewById(R.id.surfaceView);
        mTextView = findViewById(R.id.text_view);
        hometoolbar_title = findViewById(R.id.hometoolbar_title);
        hometoolbar_title.setText("CAPTURE");
        capt = findViewById(R.id.cnt_btn);
        startCameraSource();

        capt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(CaptureActivity.this, TransactionActivity.class);
                String [] a = mTextView.getText().toString().split("\n");
//                intent.putExtra("capture", a[0]);
//                intent.putExtra("vehicleType", getIntent().getStringExtra("vehicleType"));
//                startActivity(intent);
                PreferenceManager.setCaptureNoPol(a[0]);
                CaptureActivity.super.onBackPressed();
            }
        });
    }

    private void startCameraSource() {

        //Create the TextRecognizer
        final TextRecognizer textRecognizer = new TextRecognizer.Builder(getApplicationContext()).build();
//Set the TextRecognizer's Processor.
        textRecognizer.setProcessor(new Detector.Processor<TextBlock>() {
            @Override
            public void release() {
            }

            /**
             * Detect all the text from camera using TextBlock and the values into a stringBuilder
             * which will then be set to the textView.
             * */
            @Override
            public void receiveDetections(Detector.Detections<TextBlock> detections) {
                final SparseArray<TextBlock> items = detections.getDetectedItems();
                if (items.size() != 0 ){
                    try {
                        Thread.sleep(1500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    mTextView.post(new Runnable() {
                        @Override
                        public void run() {
                            TextBlock item = items.valueAt(0);
                            String a = item.getValue();
                            String [] b = a.split("\n");
                            mTextView.setText(b[0]);
                        }
                    });
                }
            }
        });

        if (!textRecognizer.isOperational()) {
            Log.i("kunam", "Detector dependencies not loaded yet");

            // Check for low storage.  If there is low storage, the native library will not be
            // downloaded, so detection will not become operational.
            IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = registerReceiver(null, lowstorageFilter) != null;

            if (hasLowStorage) {
                Toast.makeText(this, "MEMORY LOW", Toast.LENGTH_LONG).show();
                Log.i("kunam", "MEMORY LOW");
            }
        } else {
            Log.i("kunam", "success start camera");
            Log.i("kunam", Manifest.permission.CAMERA);
            //Initialize camerasource to use high resolution and set Autofocus on.

//            .setRequestedPreviewSize(1280, 1024)
            mCameraSource = new CameraSource.Builder(getApplicationContext(), textRecognizer)
                    .setFacing(CameraSource.CAMERA_FACING_BACK)
                    .setRequestedPreviewSize(300, 100)
                    .setAutoFocusEnabled(false)
                    .setRequestedFps(2.0f)
                    .build();

            /**
             * Add call back to SurfaceView and check if camera permission is granted.
             * If permission is granted we can start our cameraSource and pass it to surfaceView
             */
            mCameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder holder) {
                    try {

                        if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                            ActivityCompat.requestPermissions(CaptureActivity.this,
                                    new String[]{Manifest.permission.CAMERA},
                                    1);
                            return;
                        }
                        mCameraSource.start(mCameraView.getHolder());
//                        mCameraSource = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                }

                /**
                 * Release resources for cameraSource
                 */
                @Override
                public void surfaceDestroyed(SurfaceHolder holder) {
                    mCameraSource.stop();
                }
            });


        }
    }
}
