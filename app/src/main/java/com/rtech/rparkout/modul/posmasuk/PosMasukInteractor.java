package com.rtech.rparkout.modul.posmasuk;

import com.rtech.rparkout.component.network.response.SaveDataPosMasukRequest;
import com.rtech.rparkout.component.network.response.SaveDataPosMasukResponse;
import com.rtech.rparkout.component.network.response.UploadImageRequest;
import com.rtech.rparkout.component.network.response.UploadImageResponse;

import rx.Observable;

public interface PosMasukInteractor {
    Observable<UploadImageResponse> uploadImage(UploadImageRequest device_id);
    Observable<SaveDataPosMasukResponse> saveDataPosMasuk(SaveDataPosMasukRequest device_id);
}
