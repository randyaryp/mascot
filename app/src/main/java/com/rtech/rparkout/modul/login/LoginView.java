package com.rtech.rparkout.modul.login;

import com.rtech.rparkout.component.network.response.AuthResponse;
import com.rtech.rparkout.component.network.response.ParkingRateResponse;

/**
 * Created by Dhimas on 12/22/17.
 */

public interface LoginView {
    void onSuccessParkingRate(ParkingRateResponse dev_response);
    void onSuccessLogin(AuthResponse resp);
}
