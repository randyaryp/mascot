package com.rtech.rparkout.modul;

//import NetworkService;

import com.rtech.rparkout.component.dialog.CustomProgressBar;
import com.rtech.rparkout.component.network.NetworkService;

/**
 * Created by Dhimas on 11/23/17.
 */

public interface CommonInterface {

    public static CustomProgressBar progressBar = new CustomProgressBar();

    void showProgressLoading();

    void hideProgresLoading();

    NetworkService getService();

    void onFailureRequest(String msg);
}
