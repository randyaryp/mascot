package com.rtech.rparkout.modul.poskeluar;

import com.rtech.rparkout.component.network.NetworkService;
import com.rtech.rparkout.component.network.response.ParkingRateRequest;
import com.rtech.rparkout.component.network.response.ParkingRateResponse;
import com.rtech.rparkout.component.network.response.SaveDataPosKeluarRequest;
import com.rtech.rparkout.component.network.response.SaveDataPosKeluarResponse;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ParkingRateInteractorImpl implements ParkingRateInteractor{
    private NetworkService mService;

    public ParkingRateInteractorImpl(NetworkService service) {
        mService = service;
    }

    @Override
    public Observable<ParkingRateResponse> parkingrate(ParkingRateRequest device_id) {
        return mService.parkingRate(device_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io());
    }

    @Override
    public Observable<SaveDataPosKeluarResponse> saveDataPosKeluar(SaveDataPosKeluarRequest device_id) {
        return mService.saveDataPosKeluar(device_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io());
    }
}
