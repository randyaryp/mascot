package com.rtech.rparkout.modul.kendaraan;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rtech.mascot.R;
import com.rtech.rparkout.component.adapter.KendaraanListAdapter;
import com.rtech.rparkout.component.network.NetworkManager;
import com.rtech.rparkout.component.network.NetworkService;
import com.rtech.rparkout.component.network.gson.GKendaraanDetail;
import com.rtech.rparkout.component.network.request.KendaraanListRequest;
import com.rtech.rparkout.component.network.request.SearchKendaraanRequest;
import com.rtech.rparkout.component.network.response.EditKendaraanResponse;
import com.rtech.rparkout.component.network.response.KendaraanListResponse;
import com.rtech.rparkout.component.network.response.UploadImageResponse;
import com.rtech.rparkout.component.util.MethodUtil;
import com.rtech.rparkout.component.util.PreferenceManager;
import com.rtech.rparkout.modul.CommonInterface;
import com.rtech.rparkout.modul.navigation.MyRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class KendaraanFragment extends Fragment implements CommonInterface, KendaraanListView {
    public KendaraanListPresenter mPresenter;
    private RecyclerView ListKendaraanView;
    private KendaraanListAdapter adapter;
    private View rootView;
    private ImageView notFound;
    private EditText searchbar;
    List<GKendaraanDetail> data;

    public KendaraanFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_kendaraan, container, false);
        initComponent();
        initEvents();
        return rootView;
    }

    @Override
    public void onResume() {
        KendaraanListRequest listKendaraan = new KendaraanListRequest(PreferenceManager.getSite(), PreferenceManager.getLoggedInLobbyId());
        mPresenter.kendaraanList(listKendaraan);
        super.onResume();
    }

    private void initComponent() {
        mPresenter = new KendaraanListPresenterImpl(this, this);
//        KendaraanListRequest listKendaraan = new KendaraanListRequest(PreferenceManager.getSite(), PreferenceManager.getLoggedInLobbyId());
//        mPresenter.kendaraanList(listKendaraan);

        notFound = rootView.findViewById(R.id.not_found);
        searchbar = rootView.findViewById(R.id.searchbar);
        ListKendaraanView = rootView.findViewById(R.id.list_kendaraan);
        ListKendaraanView.setHasFixedSize(true);
        ListKendaraanView.setLayoutManager(new LinearLayoutManager(rootView.getContext()));

        MyRecyclerViewAdapter.ItemClickListener listener = new MyRecyclerViewAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Toast.makeText(getActivity(), "Item clicked: " + position, Toast.LENGTH_SHORT).show();
            }
        };
        if (data != null){
            adapter = new KendaraanListAdapter(data, getContext());
            ListKendaraanView.setAdapter(adapter);
            ListKendaraanView.setVisibility(View.VISIBLE);
            notFound.setVisibility(View.GONE);
        }else{
            ListKendaraanView.setVisibility(View.GONE);
            notFound.setVisibility(View.GONE);
        }
    }
    private void initEvents() {
        searchbar.addTextChangedListener(
                new TextWatcher() {
                    @Override public void onTextChanged(CharSequence s, int start, int before, int count) { }
                    @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

                    private Timer timer = new Timer();
                    private final long DELAY = 300; // Milliseconds

                    @Override
                    public void afterTextChanged(final Editable s) {
                        timer.cancel();
                        timer = new Timer();
                        timer.schedule(
                                new TimerTask() {
                                    @Override
                                    public void run() {
                                        // TODO: Do what you need here (refresh list).
                                        // You will probably need to use
                                        // runOnUiThread(Runnable action) for some
                                        // specific actions (e.g., manipulating views).
                                        SearchKendaraanRequest searchlistKendaraan = new SearchKendaraanRequest(PreferenceManager.getSite(), PreferenceManager.getLoggedInLobbyId(),s.toString());
                                        mPresenter.searchKendaraanList(searchlistKendaraan);
                                    }
                                },
                                DELAY
                        );
                    }
                }
        );
    }

    @Override
    public void showProgressLoading() {
        progressBar.show(getContext(), "", false, null);
    }

    @Override
    public void hideProgresLoading() {
        progressBar.getDialog().dismiss();
    }

    @Override
    public NetworkService getService() {
        return NetworkManager.getInstance();
    }

    @Override
    public void onFailureRequest(String msg) {
        MethodUtil.showCustomToast(getActivity(), msg, R.drawable.ic_warning_red_24dp);
    }

    @Override
    public void onSuccessGetKendaraanList(KendaraanListResponse dev_response) {
        if (dev_response.success){
            PreferenceManager.setListKendaraanInside(dev_response);
            data = dev_response.data;
            adapter = new KendaraanListAdapter(data, getContext());
            ListKendaraanView.setAdapter(adapter);
            ListKendaraanView.setVisibility(View.VISIBLE);
            if (dev_response.data.size() < 1){
                ListKendaraanView.setVisibility(View.GONE);
                notFound.setVisibility(View.VISIBLE);
            }else{
                ListKendaraanView.setVisibility(View.VISIBLE);
                notFound.setVisibility(View.GONE);
            }
        }else{
            MethodUtil.showCustomToast(getActivity(), "Failed get Kendaraan", R.drawable.ic_warning_red_24dp);
            ListKendaraanView.setVisibility(View.GONE);
            notFound.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onSuccessEditKendaraan(EditKendaraanResponse dev_response) {

    }

    @Override
    public void onSuccessUploadImage(UploadImageResponse dev_response) {

    }
}
