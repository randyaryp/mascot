package com.rtech.rparkout.modul.printersetting;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.mazenrashed.printooth.Printooth;
import com.mazenrashed.printooth.data.printable.Printable;
import com.mazenrashed.printooth.data.printable.TextPrintable;
import com.mazenrashed.printooth.data.printer.DefaultPrinter;
import com.mazenrashed.printooth.utilities.PrintingCallback;
import com.rtech.mascot.R;
import com.rtech.rparkout.MainActivity;
import com.rtech.rparkout.component.dialog.CustomAlert;
import com.rtech.rparkout.component.dialog.CustomProgressBar;
import com.rtech.rparkout.component.network.gson.GLobby;
import com.rtech.rparkout.component.util.PreferenceManager;
import com.rtech.rparkout.modul.login.LoginActivity;

import java.util.ArrayList;
import java.util.List;

public class PrinterSetting extends Fragment {
    private View viewFragment;
    private TextView user, location, lobby;
    private Button buttonPrinterSetting, buttonTestPrint;
    private EditText edit_nama_printer, edit_alamat_printer;

    private Dialog dialog;

    public PrinterSetting() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Printooth.INSTANCE.init(getContext());
        viewFragment = inflater.inflate(R.layout.fragment_printer_setting, container, false);
        initComponent();
        initEvent();
        return viewFragment;
    }

    private void initComponent() {
        buttonTestPrint = viewFragment.findViewById(R.id.buttonTestPrint);
        buttonPrinterSetting = viewFragment.findViewById(R.id.buttonPrinterSetting);
        edit_nama_printer = viewFragment.findViewById(R.id.edit_nama_printer);
        edit_nama_printer.setText(PreferenceManager.getNamaPrinter());
        edit_alamat_printer = viewFragment.findViewById(R.id.edit_alamat_printer);
        edit_alamat_printer.setText(PreferenceManager.getAlamatPrinter());
        user = viewFragment.findViewById(R.id.user);
        user.setText(PreferenceManager.getUser());

        location = viewFragment.findViewById(R.id.location);
        location.setText(PreferenceManager.getSite());

        lobby = viewFragment.findViewById(R.id.lobby);
        String lobbyName = "";
        List<GLobby> lobbyList = PreferenceManager.getLobbyList();
        for (int i = 0; i < lobbyList.size(); i++) {
            if (lobbyList.get(i).id.equalsIgnoreCase(PreferenceManager.getLoggedInLobbyId())) {
                lobbyName = lobbyList.get(i).lobby_name;
            }
        }
        lobby.setText(lobbyName);
    }

    private void initEvent() {
        buttonPrinterSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferenceManager.setNamaPrinter(edit_nama_printer.getText().toString());
                PreferenceManager.setAlamatPrinter(edit_alamat_printer.getText().toString());
                initDialog();
            }
        });
        buttonTestPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<Printable> printables = new ArrayList<Printable>();
                TextPrintable printable3 = (TextPrintable) new TextPrintable.Builder()
                        .setText("Test Print OK")
                        .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                        .setNewLinesAfter(3)
                        .build();
                TextPrintable printableenter = (TextPrintable) new TextPrintable.Builder()
                        .setText("\n")
                        .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                        .build();
                printables.add(printable3);
                printables.add(printableenter);
                Printooth.INSTANCE.setPrinter(PreferenceManager.getNamaPrinter(), PreferenceManager.getAlamatPrinter());
                Printooth.INSTANCE.printer().print(printables);
            }
        });
    }

    private void initDialog() {
        LayoutInflater inflator = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflator.inflate(R.layout.dialog, null);

        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        dialog = new Dialog(getContext(), R.style.CustomDialog);
        dialog.setContentView(view);
        dialog.setCancelable(false);
        dialog.setOnCancelListener(null);
        dialog.show();

        TextView alertTitle = view.findViewById(R.id.alertTitle);
        alertTitle.setText("Edit Printer Setting Sukses!");

        Button button = view.findViewById(R.id.buttonProses);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        Button buttonCancel = view.findViewById(R.id.buttonCancel);
        buttonCancel.setVisibility(View.GONE);
    }
}
