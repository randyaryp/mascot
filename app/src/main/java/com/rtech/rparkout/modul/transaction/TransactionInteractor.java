package com.rtech.rparkout.modul.transaction;

import com.rtech.rparkout.component.network.response.SaveTransactionRequest;
import com.rtech.rparkout.component.network.response.SaveTransactionResponse;

import rx.Observable;

/**
 * Created by Dhimas on 1/3/18.
 */

public interface TransactionInteractor {
    Observable<SaveTransactionResponse> saveTransaction(SaveTransactionRequest device_id);
}
