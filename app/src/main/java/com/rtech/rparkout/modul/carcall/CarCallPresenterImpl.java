package com.rtech.rparkout.modul.carcall;

import com.rtech.rparkout.component.network.NetworkService;
import com.rtech.rparkout.component.network.ResponeError;
import com.rtech.rparkout.component.network.request.CarCallRequest;
import com.rtech.rparkout.component.network.response.CarCallResponse;
import com.rtech.rparkout.component.network.response.CekDeviceResponse;
import com.rtech.rparkout.modul.CommonInterface;

import rx.Subscriber;

public class CarCallPresenterImpl implements CarCallPresenter{
    private CarCallView mView;
    private CommonInterface cInterface;
    private NetworkService mService;
    private CarCallInteractor mInteractor;

    public CarCallPresenterImpl(CommonInterface commonInterface, CarCallView view) {
        mView = view;
        cInterface = commonInterface;
        mService = cInterface.getService();
        mInteractor = new CarCallInteractorImpl(mService);
    }

    @Override
    public void carcall(CarCallRequest carcall) {
        cInterface.showProgressLoading();
        mInteractor.carcall(carcall).subscribe(new Subscriber<CarCallResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
//                Log.i("kunam", e.getMessage());
                cInterface.hideProgresLoading();
                cInterface.onFailureRequest(ResponeError.getErrorMessage(e));
            }

            @Override
            public void onNext(CarCallResponse gCreditCard) {
                cInterface.hideProgresLoading();
                mView.onSuccessCarCall(gCreditCard);
            }
        });
    }
}
