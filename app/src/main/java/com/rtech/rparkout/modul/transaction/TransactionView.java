package com.rtech.rparkout.modul.transaction;

import com.rtech.rparkout.component.network.response.SaveTransactionRequest;
import com.rtech.rparkout.component.network.response.SaveTransactionResponse;

/**
 * Created by Dhimas on 12/22/17.
 */

public interface TransactionView {
    void onSuccessSaveTransaction(SaveTransactionResponse dev_response);
    void onFailedSaveTransaction(SaveTransactionRequest req);
}
