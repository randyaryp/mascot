package com.rtech.rparkout.modul.login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
//import android.widget.Toolbar;

//import com.google.firebase.analytics.FirebaseAnalytics;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
//import androidx.appcompat.widget.Toolbar;

//import com.rtech.onstreet.R;
import com.rtech.mascot.R;
import com.rtech.rparkout.MainActivity;
import com.rtech.rparkout.component.adapter.JenisTransactionAdapter;
import com.rtech.rparkout.component.adapter.LobbyListAdapter;
import com.rtech.rparkout.component.network.NetworkManager;
import com.rtech.rparkout.component.network.NetworkService;
import com.rtech.rparkout.component.network.gson.GLobby;
import com.rtech.rparkout.component.network.response.AuthRequest;
import com.rtech.rparkout.component.network.response.AuthResponse;
import com.rtech.rparkout.component.network.response.ParkingRateResponse;
import com.rtech.rparkout.component.util.MethodUtil;
import com.rtech.rparkout.component.util.PreferenceManager;
import com.rtech.rparkout.modul.CommonInterface;
import com.rtech.rparkout.modul.menu.MenuActivity;
import com.rtech.rparkout.modul.setting.SettingActivity;
import com.rtech.rparkout.modul.transaction.TransactionActivity;

import java.util.ArrayList;
import java.util.List;

//import androidx.navigation.ui.AppBarConfiguration;

/**
 * Created by Dhimas on 10/5/17.
 */

public class LoginActivity extends AppCompatActivity implements CommonInterface, LoginView, AdapterView.OnItemSelectedListener {
    private TextView loct;
    private EditText username;
    private EditText password;
    private Button loginBtn;
    private ImageView loginImg;
    private RelativeLayout set;
    public LoginPresenter mPresenter;
    private Spinner dropdown;
//    private Spinner dropdownTransaksi;
    private List<GLobby> GLobbyList;
//    private List<String> JenisTransaksi;
    private int lobbyId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);
        initComponent();
        initEvent();
    }

    private void initComponent() {
        mPresenter = new LoginPresenterImpl(this, this);

        set = findViewById(R.id.setting_btn);
        loginBtn = findViewById(R.id.login_btn);
        loct = findViewById(R.id.loct);

        if (getIntent().getStringExtra("site_id") != null) {
            loct.setText(getIntent().getStringExtra("site_id").toUpperCase());
        }
        if (PreferenceManager.getSite() != null) {
            loct.setText(PreferenceManager.getSite().toUpperCase());
        }

        username = findViewById(R.id.input_username);
        password = findViewById(R.id.input_password);

        dropdown = findViewById(R.id.spinnerLobby);
        dropdown.setOnItemSelectedListener(this);
        GLobbyList = PreferenceManager.getLobbyList();
        if (GLobbyList != null) {
            LobbyListAdapter lobbyList = new LobbyListAdapter(getApplicationContext(), GLobbyList);
            dropdown.setAdapter(lobbyList);
        }

//        dropdownTransaksi = findViewById(R.id.spinnerTransaksi);
//        dropdownTransaksi.setOnItemSelectedListener(this);
//        JenisTransaksi = new ArrayList<String>();
//        JenisTransaksi.add("Transaksi Masuk");
//        JenisTransaksi.add("Transaksi Keluar");
//        JenisTransactionAdapter jenisTransaksiList = new JenisTransactionAdapter(getApplicationContext(), JenisTransaksi);
//        dropdownTransaksi.setAdapter(jenisTransaksiList);
    }

    private void initEvent() {
        set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SettingActivity.class);
                startActivity(intent);
            }
        });
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (username.getText().toString() == null || username.getText().toString().equalsIgnoreCase("")) {
                    MethodUtil.showCustomToast(LoginActivity.this, "Harap Menginputkan Username", R.drawable.ic_warning_red_24dp);
                } else if (password.getText().toString() == null || password.getText().toString().equalsIgnoreCase("")) {
                    MethodUtil.showCustomToast(LoginActivity.this, "Harap Menginputkan Password", R.drawable.ic_warning_red_24dp);
                } else if (PreferenceManager.getSite() == null) {
                    MethodUtil.showCustomToast(LoginActivity.this, "Harap Memasukkan Lokasi Parkir", R.drawable.ic_warning_red_24dp);
                } else {
                    AuthRequest rrq = new AuthRequest(PreferenceManager.getSite(), username.getText().toString(), password.getText().toString(), PreferenceManager.getImei(), lobbyId);
                    mPresenter.login(rrq);
//                    if(username.getText().toString().equals("RPARK") && password.getText().toString().equals("12345") && PreferenceManager.getSite() != null){
//                        ParkingRateRequest req = new ParkingRateRequest(PreferenceManager.getSite());
//                        mPresenter.parkingRate(req);
//                    }else{
//                        MethodUtil.showCustomToast(LoginActivity.this, "Username atau Password Salah!", R.drawable.ic_warning_red_24dp);
//                    }
                }

//                Intent intent = new Intent(LoginActivity.this, TransactionActivity.class);
//                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void showProgressLoading() {
        progressBar.show(this, "", false, null);
    }

    @Override
    public void hideProgresLoading() {
        progressBar.getDialog().dismiss();
    }

    @Override
    public NetworkService getService() {
        return NetworkManager.getInstance();
    }

    @Override
    public void onFailureRequest(String msg) {
        MethodUtil.showCustomToast(this, msg, R.drawable.ic_warning_red_24dp);
    }

    @Override
    public void onSuccessParkingRate(ParkingRateResponse dev_response) {
//        if(dev_response.content == 1){
//            MethodUtil.showCustomToast(LoginActivity.this, "Gagal Mengambil Rate Parkir!", R.drawable.ic_warning_red_24dp);
//        }else{
//            PreferenceManager.setParkingRate(dev_response.message);
//            Intent intent = new Intent(this, TransactionActivity.class);
//            startActivity(intent);
//        }
    }

    @Override
    public void onSuccessLogin(AuthResponse resp) {
        if (resp.success) {
            PreferenceManager.setLoginAuth(resp);
            PreferenceManager.logIn(username.getText().toString());
            PreferenceManager.setLoggedInLobbyId(String.valueOf(lobbyId));
//            Intent intent = new Intent(this, MainActivity.class);
//            startActivity(intent);
            Intent intent = new Intent(this, MenuActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        } else {
            MethodUtil.showCustomToast(this, "Login failed", R.drawable.ic_warning_red_24dp);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (String.valueOf(parent).contains("spinnerLobby")) {
            lobbyId = Integer.valueOf(GLobbyList.get(position).id);
        }

//        if (String.valueOf(parent).contains("spinnerTransaksi")) {
//            PreferenceManager.setJenisTransaksiLogin(JenisTransaksi.get(position));
//        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
