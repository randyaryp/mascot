package com.rtech.rparkout.modul.posmasuk;

import android.util.Log;

import com.rtech.rparkout.component.network.NetworkService;
import com.rtech.rparkout.component.network.ResponeError;
import com.rtech.rparkout.component.network.response.SaveDataPosMasukRequest;
import com.rtech.rparkout.component.network.response.SaveDataPosMasukResponse;
import com.rtech.rparkout.component.network.response.UploadImageRequest;
import com.rtech.rparkout.component.network.response.UploadImageResponse;
import com.rtech.rparkout.modul.CommonInterface;

import rx.Subscriber;

public class PosMasukPresenterImpl implements PosMasukPresenter{
    private PosMasukView mView;
    private CommonInterface cInterface;
    private NetworkService mService;
    private PosMasukInteractor mInteractor;

    public PosMasukPresenterImpl(CommonInterface commonInterface, PosMasukView view) {
        mView = view;
        cInterface = commonInterface;
        mService = cInterface.getService();
        mInteractor = new PosMasukInteractorImpl(mService);
    }

    @Override
    public void uploadImage(UploadImageRequest dev_id) {
        cInterface.showProgressLoading();
        mInteractor.uploadImage(dev_id).subscribe(new Subscriber<UploadImageResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.i("kunam", e.getMessage());
                cInterface.hideProgresLoading();
                cInterface.onFailureRequest(ResponeError.getErrorMessage(e));
            }

            @Override
            public void onNext(UploadImageResponse gCreditCard) {
                cInterface.hideProgresLoading();
                mView.onSuccessUploadImage(gCreditCard);
            }
        });
    }

    @Override
    public void saveDataPosMasuk(SaveDataPosMasukRequest dev_id) {
        cInterface.showProgressLoading();
        mInteractor.saveDataPosMasuk(dev_id).subscribe(new Subscriber<SaveDataPosMasukResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.i("kunam err", e.getMessage());
                cInterface.hideProgresLoading();
                cInterface.onFailureRequest(ResponeError.getErrorMessage(e));
            }

            @Override
            public void onNext(SaveDataPosMasukResponse gCreditCard) {
                cInterface.hideProgresLoading();
                mView.onSuccessSaveDataPosMasuk(gCreditCard);
            }
        });
    }
}
