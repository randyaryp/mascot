package com.rtech.rparkout.modul.transaction;

//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;
import com.rtech.mascot.R;
//import com.rtech.onstreet.R;

import java.io.File;

public class PdfReadActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_pdf);

        File a = new File(getIntent().getStringExtra("paths"));
        PDFView pdfView = (PDFView) findViewById(R.id.pdfView);
        pdfView.fromFile(a).load();
//        pdfView.fromAsset(getIntent().getStringExtra("paths")).load();
//        pdfView.fromUri(getIntent().getStringExtra("paths"));

    }
}