package com.rtech.rparkout.modul.transaction;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.pdf.PdfDocument;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
//import android.support.annotation.Nullable;
//import android.support.annotation.RequiresApi;
//import android.support.v4.content.res.ResourcesCompat;
//import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
//import com.squareup.picasso.Picasso;
//import com.google.firebase.analytics.FirebaseAnalytics;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.res.ResourcesCompat;

//import com.rtech.onstreet.R;
import com.rtech.mascot.R;
import com.rtech.rparkout.component.adapter.CustomAdapter;
import com.rtech.rparkout.component.adapter.CustomNoIconAdapter;
import com.rtech.rparkout.component.network.NetworkManager;
import com.rtech.rparkout.component.network.NetworkService;
import com.rtech.rparkout.component.network.gson.GParkingRateMessage;
import com.rtech.rparkout.component.network.response.SaveTransactionRequest;
import com.rtech.rparkout.component.network.response.SaveTransactionResponse;
import com.rtech.rparkout.component.util.MethodUtil;
import com.rtech.rparkout.component.util.PreferenceManager;
import com.rtech.rparkout.modul.CommonInterface;
import com.rtech.rparkout.modul.capture.CaptureActivity;
import com.rtech.rparkout.modul.captureDepan.CaptureDepan;
import com.rtech.rparkout.modul.login.LoginActivity;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Created by Dhimas on 10/5/17.
 */

public class TransactionActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, CommonInterface, TransactionView {
    private TextView user;
    private TextView location, tarif;
    private Button loginBtn;
    private RelativeLayout setting_btn;
    private ImageView signout_btn;
    private Spinner dropdownPrepaid;
    private Spinner dropdownDuration;
    private Spinner dropdown, dropdownTransactions, dropdownJenisKendaraanMasuk;
    private LinearLayout prepaid;
    private TextView capt_nopol, edit_nopol_masuk;
    private ImageView capture, captureKendaraanDepanMasuk;
    private LinearLayout captureNopolMasuk, captureDepan;
    private GParkingRateMessage[] parkingRate = new GParkingRateMessage[6];
    private String[] parkingRates = new String[6];

    private LinearLayout transactionsInLayout, transactionsOutLayout;

    String[] vehicles;
    int ics[];
    String[] itemsPrepaid;
    String[] itemsDuration;
    String[] itemsTransactions;
    private EditText edit_nopol;
    Date currentTime;
    SimpleDateFormat sdf;

    String tar;
    public TransactionPresenter mPresenter;
    String vehicle;
    String rated;
    String payments;
    SaveTransactionRequest cek;
    String vehiclePosition;
    String msBerlaku;
    int duration;
    int TAKE_PHOTO_CODE = 0;

    // will enable user to enter any text to be printed
    EditText myTextbox;

    // android built in classes for bluetooth operations
    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;

    // needed for communication to bluetooth device / network
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;

    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;

    private static final int RC_OCR_CAPTURE = 9003;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private static final int CAMERA_REQUEST = 1888;

    public static int count = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transaction_black_layout);
        initComponent();
        initEvent();
//        initEvents();
    }

    private void initComponent() {
        mPresenter = new TransactionPresenterImpl(this, this);
        parkingRates[0] = "Mobil";
        parkingRates[1] = "Motor";
        parkingRates[2] = "Box";
        parkingRates[3] = "Bus";
        parkingRates[4] = "Pickup";
        parkingRates[5] = "Truck";

        tarif = findViewById(R.id.tarif);

        user = findViewById(R.id.user);
        user.setText(PreferenceManager.getUser());

        location = findViewById(R.id.location);
        location.setText(PreferenceManager.getSite());

        loginBtn = findViewById(R.id.cnt_btn);
//        setting_btn = findViewById(R.id.setting_btn);
        signout_btn = findViewById(R.id.logout_btn);
//        setting_btn.setVisibility(View.GONE);
        signout_btn.setVisibility(View.VISIBLE);
        prepaid = findViewById(R.id.prepaid);

        transactionsInLayout = findViewById(R.id.linear_in);
        transactionsOutLayout = findViewById(R.id.linear_out);

//        KENDARAAN
        dropdown = findViewById(R.id.spinner1);
        ics = new int[]{
                R.drawable.ic_directions_car_black_24dp,
                R.drawable.ic_motorcycle_black_24dp,
                R.drawable.ic_box_black,
                R.drawable.ic_bus_black,
                R.drawable.ic_pickup_truck,
                R.drawable.ic_truck_black
        };
        dropdown.setOnItemSelectedListener(this);
//        CustomAdapter customAdapter=new CustomAdapter(getApplicationContext(),ics,parkingRate);
        CustomNoIconAdapter customAdapter = new CustomNoIconAdapter(getApplicationContext(), parkingRates);
        dropdown.setAdapter(customAdapter);

//        DROPDOWN TRANSAKSI
        dropdownTransactions = findViewById(R.id.spinnerJenisTransaksi);
        itemsTransactions = new String[]{"Gate Masuk", "Gate Keluar"};
        dropdownTransactions.setOnItemSelectedListener(this);
        CustomNoIconAdapter transactionsAdapterNoIcon = new CustomNoIconAdapter(getApplicationContext(), itemsTransactions);
        dropdownTransactions.setAdapter(transactionsAdapterNoIcon);

//        DROPDOWN JENIS KENDARAAN MASUK
        dropdownJenisKendaraanMasuk = findViewById(R.id.spinnerJenisKendaraanMasuk);
        dropdownJenisKendaraanMasuk.setOnItemSelectedListener(this);
        CustomNoIconAdapter jenisKendaraanMasukAdapter = new CustomNoIconAdapter(getApplicationContext(), parkingRates);
        dropdownJenisKendaraanMasuk.setAdapter(jenisKendaraanMasukAdapter);

//        PREPAID
        dropdownPrepaid = findViewById(R.id.spinnerPrepaid);
        itemsPrepaid = new String[]{"EMoney", "Flazz", "Brizzi"};
        dropdownPrepaid.setOnItemSelectedListener(this);
        CustomNoIconAdapter customAdapterNoIcon = new CustomNoIconAdapter(getApplicationContext(), itemsPrepaid);
        dropdownPrepaid.setAdapter(customAdapterNoIcon);

        capture = findViewById(R.id.capture);
        capt_nopol = findViewById(R.id.capt_nopol);
        edit_nopol_masuk = findViewById(R.id.edit_nopol_masuk);

        captureNopolMasuk = findViewById(R.id.linear_capture_nopol);

        captureDepan = findViewById(R.id.captureDepan);

        if (getIntent().getStringExtra("capture") != null && getIntent().getStringExtra("capture") != "") {
            capt_nopol.setText(getIntent().getStringExtra("capture"));
            edit_nopol_masuk.setText(getIntent().getStringExtra("capture"));
        }

        if (getIntent().getStringExtra("vehicleType") != null && getIntent().getStringExtra("vehicleType") != "") {
            dropdown.setSelection(Integer.parseInt(getIntent().getStringExtra("vehicleType")));
            dropdownJenisKendaraanMasuk.setSelection(Integer.parseInt(getIntent().getStringExtra("vehicleType")));
        }

        edit_nopol = findViewById(R.id.edit_nopol);
        edit_nopol.setText(getIntent().getStringExtra("capture"));

//        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.payment);
//        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
//        {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                // checkedId is the RadioButton selected
//                Log.i("kunam", String.valueOf(checkedId));
//            }
//        });

        captureKendaraanDepanMasuk = findViewById(R.id.capture_kendaraan_depan_masuk);
    }

    private void initEvent() {
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit_nopol.getText().toString().equalsIgnoreCase("") || edit_nopol.getText().toString() == null) {
                    MethodUtil.showCustomToast(TransactionActivity.this, "Harap Menginputkan No Polisi", R.drawable.ic_warning_red_24dp);
                } else {
                    String idTrx = UUID.randomUUID().toString().replace("-", "").substring(0, 12).toUpperCase();
                    Log.i("kunam", idTrx);
                    boolean connected = false;
                    ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                            connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
                        //we are connected to a network
                        Log.i("kunam", "connect");
                        connected = true;
                    } else {
                        Log.i("kunam", "tidak connect");
                        connected = false;
                    }


                    long yourmilliseconds = System.currentTimeMillis();
                    sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    currentTime = new Date(yourmilliseconds);

                    String currentDateandTime = sdf.format(currentTime);

                    Date date = null;
                    try {
                        date = sdf.parse(currentDateandTime);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);

                    calendar.add(Calendar.HOUR, duration);
                    msBerlaku = sdf.format(calendar.getTime());

//                    Log.i("kunamsdf", msBerlaku);

                    cek = new SaveTransactionRequest(
                            PreferenceManager.getImei(),
                            PreferenceManager.getSite(),
                            "test",
                            idTrx,
                            capt_nopol.getText().toString(),
                            edit_nopol.getText().toString(),
                            vehicle,
                            rated,
                            payments,
                            sdf.format(currentTime));

                    if (connected) {
                        mPresenter.saveTransaction(cek);
                    } else {
                        List<SaveTransactionRequest> requests;
                        if (PreferenceManager.getSavedTransaction() == null) {
                            requests = new ArrayList<SaveTransactionRequest>();
//                            requests.add(cek);
//                            PreferenceManager.setSavedTransaction(requests);
                        } else {
                            requests = PreferenceManager.getSavedTransaction();
                        }
                        Log.i("kunam", String.valueOf(requests.size()));
                        requests.add(cek);
                        PreferenceManager.setSavedTransaction(requests);
                        generatePDF();
                    }


                }
//                generatePDF();
            }
        });
        signout_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceManager.logOut();
                gotoLoginPage();
            }
        });
        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(getApplicationContext(), OcrCaptureActivity.class);
//                intent.putExtra(OcrCaptureActivity.AutoFocus, true);
//                intent.putExtra(OcrCaptureActivity.UseFlash, false);
//
//                startActivityForResult(intent, RC_OCR_CAPTURE);
                Intent intent = new Intent(getApplicationContext(), CaptureActivity.class);
                intent.putExtra("vehicleType", vehiclePosition);
                startActivity(intent);
            }
        });
        captureNopolMasuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CaptureActivity.class);
                intent.putExtra("vehicleType", vehiclePosition);
                startActivity(intent);
            }
        });

        final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/picFolder/";
        File newdir = new File(dir);
        newdir.mkdirs();
        captureDepan.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                } else {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                }
            }
        });
    }

    private void initEvents() {
        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    findBT();
                    openBT();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });

        signout_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    closeBT();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    sendData("test");
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    // this will find a bluetooth printer device
    void findBT() {

        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if (mBluetoothAdapter == null) {
//                tarif.setText("No bluetooth adapter available");
            }

            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivityForResult(enableBluetooth, 0);
            }

            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {

                    // RPP300 is the name of the bluetooth printer device
                    // we got this name from the list of paired devices
                    Log.i("kunamdevice", device.getName());
                    if (device.getName().equals("Virtual Bluetooth Printer")) {
                        mmDevice = device;
                        break;
                    }
                }
            }

//            tarif.setText("Bluetooth device found.");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // tries to open a connection to the bluetooth printer device
    void openBT() throws IOException {
        try {

            // Standard SerialPortService ID
            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
            mmSocket.connect();
            mmOutputStream = mmSocket.getOutputStream();
            mmInputStream = mmSocket.getInputStream();

            beginListenForData();

//            tarif.setText("Bluetooth Opened");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * after opening a connection to bluetooth printer device,
     * we have to listen and check if a data were sent to be printed.
     */
    void beginListenForData() {
        try {
            final Handler handler = new Handler();

            // this is the ASCII code for a newline character
            final byte delimiter = 10;

            stopWorker = false;
            readBufferPosition = 0;
            readBuffer = new byte[1024];

            workerThread = new Thread(new Runnable() {
                public void run() {

                    while (!Thread.currentThread().isInterrupted() && !stopWorker) {

                        try {

                            int bytesAvailable = mmInputStream.available();

                            if (bytesAvailable > 0) {

                                byte[] packetBytes = new byte[bytesAvailable];
                                mmInputStream.read(packetBytes);

                                for (int i = 0; i < bytesAvailable; i++) {

                                    byte b = packetBytes[i];
                                    if (b == delimiter) {

                                        byte[] encodedBytes = new byte[readBufferPosition];
                                        System.arraycopy(
                                                readBuffer, 0,
                                                encodedBytes, 0,
                                                encodedBytes.length
                                        );

                                        // specify US-ASCII encoding
                                        final String data = new String(encodedBytes, "US-ASCII");
                                        readBufferPosition = 0;

                                        // tell the user data were sent to bluetooth printer device
                                        handler.post(new Runnable() {
                                            public void run() {
                                                tarif.setText(data);
                                            }
                                        });

                                    } else {
                                        readBuffer[readBufferPosition++] = b;
                                    }
                                }
                            }

                        } catch (IOException ex) {
                            stopWorker = true;
                        }

                    }
                }
            });

            workerThread.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // this will send text data to be printed by the bluetooth printer
    void sendData(String pdf) throws IOException {
        try {

            // the text typed by the user
            String msg =
                    "        RPARK ON STREET";
            msg += "\n";
            msg +=
                    "          Nota Parkir";
            msg += "\n";
            msg += "\n";
            msg += "\n";
            msg +=
                    "Lokasi      :" + PreferenceManager.getSite();
            msg += "\n";
            msg +=
                    "Petugas     :" + PreferenceManager.getUser();
            msg += "\n";
            msg +=
                    "No Polisi   :" + edit_nopol.getText().toString();
            msg += "\n";
            msg +=
                    "Masuk       :" + sdf.format(currentTime);
            msg += "\n";
            msg +=
                    "Ms. Berlaku :" + msBerlaku;
            msg += "\n";
            msg +=
                    "Durasi      :" + duration + " Jam";
            msg += "\n";
            msg +=
                    "Biaya Parkir:" + tar;
            msg += "\n";
            msg += "\n";
            msg += "\n";
            msg +=
                    "Terima kasih atas Kunjungan anda";
            msg += "\n";
            msg +=
                    "            *****";
            msg += "\n";
            msg += "\n";
            msg += "\n";
            msg += "\n";
            msg += "\n";
            msg += "\n";
            msg += "\n";
            msg += "\n";

            mmOutputStream.write(msg.getBytes());

//            InputStream is = this.openFileInput(pdf); // Where this is Activity
//            ByteArrayOutputStream bos = new ByteArrayOutputStream();
//            byte[] b = new byte[1024];
//            int bytesRead = is.read(b);
//            while (bytesRead != -1) {
//                bos.write(b, 0, bytesRead);
//                bytesRead = is.read(b);
//            }
//            byte[] bytes = bos.toByteArray();
//
//            byte[] printformat = { 27, 33, 0 }; //try adding this print format
//
//            mmOutputStream.write(printformat);
//            mmOutputStream.write(bytes);

            // tell the user data were sent
//            myLabel.setText("Data Sent");

//            closeBT();

            // tell the user data were sent
            Toast.makeText(getApplicationContext(), "Data Sent", Toast.LENGTH_LONG).show();
//            tarif.setText("Data sent.");

//            Log.i("kunam", msg.getBytes().toString());
//
//            mmOutputStream.write(msg.getBytes());
//
//            // tell the user data were sent
//            tarif.setText("Data sent.");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // close the connection to bluetooth printer.
    void closeBT() throws IOException {
        try {
            stopWorker = true;
            mmOutputStream.close();
            mmInputStream.close();
            mmSocket.close();
//            tarif.setText("Bluetooth Closed");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.radio_cash:
                if (checked) {
                    // Pirates are the best
                    Log.i("kunam", "CASH");
                    prepaid.setVisibility(View.GONE);
                    payments = "CASH";
                }
                break;
            case R.id.radio_prepaid:
                if (checked) {
                    Log.i("kunam", "Prepaid");
                    prepaid.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void gotoLoginPage() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (String.valueOf(parent).contains("spinner1") || String.valueOf(parent).contains("spinnerJenisKendaraanMasuk")) {
            vehiclePosition = String.valueOf(position);
            vehicle = parkingRates[position];

        }
        if (String.valueOf(parent).contains("spinnerClock")) {
            int sum = 0;
            duration = 0;
            msBerlaku = "";

            if (position == 0) {
                sum = Integer.valueOf(parkingRate[Integer.valueOf(vehiclePosition)].rate1);
                duration = 1;
            } else if (position == 1) {
                sum = Integer.valueOf(parkingRate[Integer.valueOf(vehiclePosition)].rate1) +
                        Integer.valueOf(parkingRate[Integer.valueOf(vehiclePosition)].rate2);
                duration = 2;
            } else if (position == 2) {
//                sum = Integer.valueOf(parkingRate.get(Integer.valueOf(vehiclePosition)).rate1) +
//                        Integer.valueOf(parkingRate.get(Integer.valueOf(vehiclePosition)).rate2) +
//                        Integer.valueOf(parkingRate.get(Integer.valueOf(vehiclePosition)).rate3);
                duration = 3;
            } else if (position == 3) {
//                sum = Integer.valueOf(parkingRate.get(Integer.valueOf(vehiclePosition)).rate1) +
//                        Integer.valueOf(parkingRate.get(Integer.valueOf(vehiclePosition)).rate2) +
//                        Integer.valueOf(parkingRate.get(Integer.valueOf(vehiclePosition)).rate3) +
//                        Integer.valueOf(parkingRate.get(Integer.valueOf(vehiclePosition)).rate4);
                duration = 4;
            } else if (position == 4) {
//                sum = Integer.valueOf(parkingRate.get(Integer.valueOf(vehiclePosition)).rate1) +
//                        Integer.valueOf(parkingRate.get(Integer.valueOf(vehiclePosition)).rate2) +
//                        Integer.valueOf(parkingRate.get(Integer.valueOf(vehiclePosition)).rate3) +
//                        Integer.valueOf(parkingRate.get(Integer.valueOf(vehiclePosition)).rate4) +
//                        Integer.valueOf(parkingRate.get(Integer.valueOf(vehiclePosition)).rate5);
                duration = 5;
            }

            rated = String.valueOf(sum);
            tarif.setText("Rp " + MethodUtil.toCurrencyFormat(String.valueOf(sum)));
            tar = "Rp " + MethodUtil.toCurrencyFormat(String.valueOf(sum));

        }
        if (String.valueOf(parent).contains("spinnerPrepaid")) {
//            Toast.makeText(getApplicationContext(), itemsPrepaid[position] + " Sprinner Prepaid", Toast.LENGTH_LONG).show();
            payments = itemsPrepaid[position];
        }

        if (String.valueOf(parent).contains("spinnerJenisTransaksi")) {
            if (position == 0) {
                transactionsInLayout.setVisibility(View.VISIBLE);
                transactionsOutLayout.setVisibility(View.GONE);
            } else {
                transactionsInLayout.setVisibility(View.GONE);
                transactionsOutLayout.setVisibility(View.VISIBLE);
            }
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void generatePDF() {
        // create a new document
        PdfDocument document = new PdfDocument();

        // crate a page description
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(300, 300, 1).create();

        // start a page
        PdfDocument.Page page = document.startPage(pageInfo);
        Canvas canvas = page.getCanvas();
        Typeface tf = ResourcesCompat.getFont(TransactionActivity.this, R.font.nunitoregular);
        Paint paint = new Paint();
        paint.setTypeface(tf);
        paint.setColor(Color.BLACK);
        Rect r = new Rect();

        String text = "";

//        HEADER
        text = "RPARK ON STREET";
        canvas.getClipBounds(r);
        int cHeight = r.height();
        int cWidth = r.width();
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        float x = cWidth / 2f - r.width() / 2f - r.left;
        canvas.drawText(text, x, 10, paint);

        text = "Nota Parkir";
        canvas.getClipBounds(r);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        x = cWidth / 2f - r.width() / 2f - r.left;
        canvas.drawText(text, x, 25, paint);

//        CONTENT
//        LOCATION
        text = "Lokasi";
        canvas.getClipBounds(r);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        x = 15;
        canvas.drawText(text, x, 60, paint);

        text = ":";
        canvas.getClipBounds(r);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        x = 100;
        canvas.drawText(text, x, 60, paint);

        text = PreferenceManager.getSite();
        canvas.getClipBounds(r);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        x = 105;
        canvas.drawText(text, x, 60, paint);

//        USER
        text = "Petugas";
        canvas.getClipBounds(r);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        x = 15;
        canvas.drawText(text, x, 75, paint);

        text = ":";
        canvas.getClipBounds(r);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        x = 100;
        canvas.drawText(text, x, 75, paint);

        text = PreferenceManager.getUser();
        canvas.getClipBounds(r);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        x = 105;
        canvas.drawText(text, x, 75, paint);

//        PLAT NUMBER
        text = "No Polisi";
        canvas.getClipBounds(r);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        x = 15;
        canvas.drawText(text, x, 90, paint);

        text = ":";
        canvas.getClipBounds(r);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        x = 100;
        canvas.drawText(text, x, 90, paint);

        text = edit_nopol.getText().toString();
        canvas.getClipBounds(r);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        x = 105;
        canvas.drawText(text, x, 90, paint);

//        TIME
        text = "Masuk";
        canvas.getClipBounds(r);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        x = 15;
        canvas.drawText(text, x, 105, paint);

        text = ":";
        canvas.getClipBounds(r);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        x = 100;
        canvas.drawText(text, x, 105, paint);

        text = sdf.format(currentTime);
        canvas.getClipBounds(r);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        x = 105;
        canvas.drawText(text, x, 105, paint);

//        AMOUNT
        text = "Biaya Parkir";
        canvas.getClipBounds(r);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        x = 15;
        canvas.drawText(text, x, 120, paint);

        text = ":";
        canvas.getClipBounds(r);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        x = 100;
        canvas.drawText(text, x, 120, paint);

        text = tar;
        canvas.getClipBounds(r);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        x = 105;
        canvas.drawText(text, x, 120, paint);

//        FOOTER
        text = "Terima Kasih Atas Kunjungan Anda";
        canvas.getClipBounds(r);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        x = cWidth / 2f - r.width() / 2f - r.left;
        canvas.drawText(text, x, 160, paint);

        text = "*****";
        canvas.getClipBounds(r);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        x = cWidth / 2f - r.width() / 2f - r.left;
        canvas.drawText(text, x, 175, paint);


        // finish the page
        document.finishPage(page);

        // write the document content
        String directory_path = getFilesDir() + "/mypdf/";
        File file = new File(directory_path);
        if (!file.exists()) {
            file.mkdirs();
        }
        String targetPdf = directory_path + "test-2.pdf";
        File filePath = new File(targetPdf);
        try {
            document.writeTo(new FileOutputStream(filePath));
//            Intent intent = new Intent(TransactionActivity.this, PdfReadActivity.class);
//            intent.putExtra("paths", targetPdf);
//            startActivity(intent);
        } catch (IOException e) {
            Log.e("main", "error " + e.toString());
            Toast.makeText(TransactionActivity.this, "Something wrong: " + e.toString(), Toast.LENGTH_LONG).show();
        }

        // close the document
        document.close();

        try {
            findBT();
            openBT();
//            mmOutputStream.write(canvas.)
            sendData(targetPdf);
//            closeBT();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void showProgressLoading() {
        progressBar.show(this, "", false, null);
    }

    @Override
    public void hideProgresLoading() {
        progressBar.getDialog().dismiss();
    }

    @Override
    public NetworkService getService() {
        return NetworkManager.getInstance();
    }

    @Override
    public void onFailureRequest(String msg) {
        MethodUtil.showCustomToast(this, msg, R.drawable.ic_warning_red_24dp);
    }

    @Override
    public void onSuccessSaveTransaction(SaveTransactionResponse dev_response) {
        Log.i("kunam", dev_response.message);
        if (dev_response.code != 0) {
            List<SaveTransactionRequest> requests = PreferenceManager.getSavedTransaction();
            requests.add(cek);
            PreferenceManager.setSavedTransaction(requests);
        }
        generatePDF();
    }

    @Override
    public void onFailedSaveTransaction(SaveTransactionRequest req) {
        List<SaveTransactionRequest> requests = PreferenceManager.getSavedTransaction();
        requests.add(req);
        PreferenceManager.setSavedTransaction(requests);
        generatePDF();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            } else {
                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("kunam", "kunaman");

        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            captureKendaraanDepanMasuk.setImageBitmap(photo);
            String imageBase64 = getBase64String(photo);
            Log.i("kunam", imageBase64);
        }
    }

    private String getBase64String(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        byte[] imageBytes = baos.toByteArray();

        String base64String = Base64.encodeToString(imageBytes, Base64.NO_WRAP);

        return base64String;
    }
}
