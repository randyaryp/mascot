package com.rtech.rparkout.modul.kendaraan;

import android.util.Log;

import com.rtech.rparkout.component.network.NetworkService;
import com.rtech.rparkout.component.network.ResponeError;
import com.rtech.rparkout.component.network.request.EditKendaraanRequest;
import com.rtech.rparkout.component.network.request.KendaraanListRequest;
import com.rtech.rparkout.component.network.request.SearchKendaraanRequest;
import com.rtech.rparkout.component.network.response.EditKendaraanResponse;
import com.rtech.rparkout.component.network.response.KendaraanListResponse;
import com.rtech.rparkout.component.network.response.ParkingRateResponse;
import com.rtech.rparkout.component.network.response.UploadImageRequest;
import com.rtech.rparkout.modul.CommonInterface;

import rx.Subscriber;

public class KendaraanListPresenterImpl implements KendaraanListPresenter{
    private KendaraanListView mView;
    private CommonInterface cInterface;
    private NetworkService mService;
    private KendaraanListInteractor mInteractor;

    public KendaraanListPresenterImpl(CommonInterface commonInterface, KendaraanListView view) {
        mView = view;
        cInterface = commonInterface;
        mService = cInterface.getService();
        mInteractor = new KendaraanListInteractorImpl(mService);
    }
    @Override
    public void kendaraanList(KendaraanListRequest dev_id) {
        cInterface.showProgressLoading();
        mInteractor.kendaraanlist(dev_id).subscribe(new Subscriber<KendaraanListResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.i("kunam", e.getMessage());
                cInterface.hideProgresLoading();
                cInterface.onFailureRequest(ResponeError.getErrorMessage(e));
            }

            @Override
            public void onNext(KendaraanListResponse gCreditCard) {
                cInterface.hideProgresLoading();
                mView.onSuccessGetKendaraanList(gCreditCard);
            }
        });
    }

    @Override
    public void searchKendaraanList(SearchKendaraanRequest dev_id) {
//        cInterface.showProgressLoading();
        mInteractor.searchKendaraan(dev_id).subscribe(new Subscriber<KendaraanListResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.i("kunam", e.getMessage());
                cInterface.hideProgresLoading();
                cInterface.onFailureRequest(ResponeError.getErrorMessage(e));
            }

            @Override
            public void onNext(KendaraanListResponse gCreditCard) {
                cInterface.hideProgresLoading();
                mView.onSuccessGetKendaraanList(gCreditCard);
            }
        });
    }

    @Override
    public void EditKendaraan(EditKendaraanRequest dev_id) {
        cInterface.showProgressLoading();
        mInteractor.EditKendaraan(dev_id).subscribe(new Subscriber<EditKendaraanResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.i("kunam", e.getMessage());
                cInterface.hideProgresLoading();
                cInterface.onFailureRequest(ResponeError.getErrorMessage(e));
            }

            @Override
            public void onNext(EditKendaraanResponse gCreditCard) {
                cInterface.hideProgresLoading();
                mView.onSuccessEditKendaraan(gCreditCard);
            }
        });
    }

    @Override
    public void uploadImage(UploadImageRequest dev_id) {

    }
}
