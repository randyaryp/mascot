package com.rtech.rparkout.modul.carcall;

import com.rtech.rparkout.component.network.request.CarCallRequest;
import com.rtech.rparkout.component.network.response.CarCallResponse;

import rx.Observable;

public interface CarCallInteractor {
    Observable<CarCallResponse> carcall(CarCallRequest carcall);
}
