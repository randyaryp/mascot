package com.rtech.rparkout.modul.kendaraan;

import com.rtech.rparkout.component.network.response.EditKendaraanResponse;
import com.rtech.rparkout.component.network.response.KendaraanListResponse;
import com.rtech.rparkout.component.network.response.UploadImageResponse;

public interface KendaraanListView {
    void onSuccessGetKendaraanList(KendaraanListResponse dev_response);
    void onSuccessEditKendaraan(EditKendaraanResponse dev_response);
    void onSuccessUploadImage(UploadImageResponse dev_response);
}
