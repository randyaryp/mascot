package com.rtech.rparkout.modul.poskeluar;

import android.util.Log;

import com.rtech.rparkout.component.network.NetworkService;
import com.rtech.rparkout.component.network.ResponeError;
import com.rtech.rparkout.component.network.response.ParkingRateRequest;
import com.rtech.rparkout.component.network.response.ParkingRateResponse;
import com.rtech.rparkout.component.network.response.SaveDataPosKeluarRequest;
import com.rtech.rparkout.component.network.response.SaveDataPosKeluarResponse;
import com.rtech.rparkout.component.network.response.UploadImageResponse;
import com.rtech.rparkout.modul.CommonInterface;

import rx.Subscriber;

public class ParkingRatePresenterImpl implements ParkingRatePresenter{
    private ParkingRateView mView;
    private CommonInterface cInterface;
    private NetworkService mService;
    private ParkingRateInteractor mInteractor;

    public ParkingRatePresenterImpl(CommonInterface commonInterface, ParkingRateView view) {
        mView = view;
        cInterface = commonInterface;
        mService = cInterface.getService();
        mInteractor = new ParkingRateInteractorImpl(mService);
    }

    @Override
    public void parkingRate(ParkingRateRequest dev_id) {
        cInterface.showProgressLoading();
        mInteractor.parkingrate(dev_id).subscribe(new Subscriber<ParkingRateResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.i("kunam", e.getMessage());
                cInterface.hideProgresLoading();
                cInterface.onFailureRequest(ResponeError.getErrorMessage(e));
            }

            @Override
            public void onNext(ParkingRateResponse gCreditCard) {
                cInterface.hideProgresLoading();
                mView.onSuccessGetParkingrate(gCreditCard);
            }
        });
    }

    @Override
    public void saveDataPosKeluar(SaveDataPosKeluarRequest dev_id) {
        cInterface.showProgressLoading();
        mInteractor.saveDataPosKeluar(dev_id).subscribe(new Subscriber<SaveDataPosKeluarResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.i("kunam", e.getMessage());
                cInterface.hideProgresLoading();
                cInterface.onFailureRequest(ResponeError.getErrorMessage(e));
            }

            @Override
            public void onNext(SaveDataPosKeluarResponse gCreditCard) {
                cInterface.hideProgresLoading();
                mView.onSuccessSaveDataPosKeluar(gCreditCard);
            }
        });
    }
}
