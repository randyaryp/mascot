package com.rtech.rparkout.modul.poskeluar;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.google.zxing.Result;
import com.rtech.mascot.R;
import com.rtech.rparkout.component.network.NetworkManager;
import com.rtech.rparkout.component.network.NetworkService;
import com.rtech.rparkout.component.network.response.ParkingRateRequest;
import com.rtech.rparkout.component.network.response.ParkingRateResponse;
import com.rtech.rparkout.component.util.MethodUtil;
import com.rtech.rparkout.component.util.PreferenceManager;
import com.rtech.rparkout.modul.CommonInterface;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QRScanner extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mScannerView = new ZXingScannerView(this);
        setContentView(mScannerView);
    }
    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        Log.i("kunam", rawResult.getText());
        String[] arrRaw = rawResult.getText().split("/");
        Log.i("kunam", String.valueOf(arrRaw.length));
        Log.i("kunam", arrRaw[arrRaw.length - 1]);
        Log.i("kunam", arrRaw[arrRaw.length - 1].substring(3));
        PreferenceManager.setParkingTicketNo(arrRaw[arrRaw.length - 1].substring(3));
        super.onBackPressed();
    }
}
