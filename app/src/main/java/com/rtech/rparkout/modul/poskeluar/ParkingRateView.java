package com.rtech.rparkout.modul.poskeluar;

import com.rtech.rparkout.component.network.response.ParkingRateResponse;
import com.rtech.rparkout.component.network.response.SaveDataPosKeluarResponse;

public interface ParkingRateView {
    void onSuccessGetParkingrate(ParkingRateResponse dev_response);
    void onSuccessSaveDataPosKeluar(SaveDataPosKeluarResponse dev_response);
}
