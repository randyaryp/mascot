package com.rtech.rparkout.modul.menu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.rtech.mascot.R;
import com.rtech.rparkout.MainActivity;

public class MenuActivity extends AppCompatActivity {
    private LinearLayout posMasuk, posKeluar, kendaraanList, printerSetting;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_layout);
        initComponent();
        initEvent();
    }

    private void initComponent() {
        posMasuk = findViewById(R.id.posMasuk);
        posKeluar = findViewById(R.id.posKeluar);
        kendaraanList = findViewById(R.id.kendaraanList);
        printerSetting = findViewById(R.id.printerSetting);
    }

    private void initEvent() {
        posMasuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("page", "posmasuk");
                startActivity(intent);
            }
        });
        posKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("page", "poskeluar");
                startActivity(intent);
            }
        });
        kendaraanList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("page", "kendaraanlist");
                startActivity(intent);
            }
        });
        printerSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("page", "printersetting");
                startActivity(intent);
            }
        });
    }
}
