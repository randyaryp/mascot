package com.rtech.rparkout.modul.kendaraan;

import com.rtech.rparkout.component.network.request.EditKendaraanRequest;
import com.rtech.rparkout.component.network.request.KendaraanListRequest;
import com.rtech.rparkout.component.network.request.SearchKendaraanRequest;
import com.rtech.rparkout.component.network.response.EditKendaraanResponse;
import com.rtech.rparkout.component.network.response.KendaraanListResponse;
import com.rtech.rparkout.component.network.response.UploadImageRequest;
import com.rtech.rparkout.component.network.response.UploadImageResponse;

import rx.Observable;

public interface KendaraanListInteractor {
    Observable<KendaraanListResponse> kendaraanlist(KendaraanListRequest device_id);
    Observable<KendaraanListResponse> searchKendaraan(SearchKendaraanRequest device_id);
    Observable<EditKendaraanResponse> EditKendaraan(EditKendaraanRequest device_id);
    Observable<UploadImageResponse> uploadImage(UploadImageRequest device_id);
}
