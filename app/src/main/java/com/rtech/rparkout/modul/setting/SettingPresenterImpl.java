package com.rtech.rparkout.modul.setting;

import android.util.Log;

import com.rtech.rparkout.component.network.NetworkService;
import com.rtech.rparkout.component.network.ResponeError;
import com.rtech.rparkout.component.network.request.TicketNoteRequest;
import com.rtech.rparkout.component.network.response.CekDeviceRequest;
import com.rtech.rparkout.component.network.response.CekDeviceResponse;
import com.rtech.rparkout.component.network.response.LobbyListRequest;
import com.rtech.rparkout.component.network.response.LobbyListResponse;
import com.rtech.rparkout.component.network.response.PaymentTypeResponse;
import com.rtech.rparkout.component.network.response.TicketNoteResponse;
import com.rtech.rparkout.component.network.response.VehicleColorResponse;
import com.rtech.rparkout.component.network.response.VehicleListRequest;
import com.rtech.rparkout.component.network.response.VehicleListResponse;
import com.rtech.rparkout.component.network.response.VehicleTypeResponse;
import com.rtech.rparkout.modul.CommonInterface;

import rx.Subscriber;

/**
 * Created by Dhimas on 12/22/17.
 */

public class SettingPresenterImpl implements SettingPresenter {
    private SettingView mView;
    private CommonInterface cInterface;
    private NetworkService mService;
    private SettingInteractor mInteractor;

    public SettingPresenterImpl(CommonInterface commonInterface, SettingView view) {
        mView = view;
        cInterface = commonInterface;
        mService = cInterface.getService();
        mInteractor = new SettingInteractorImpl(mService);
    }

    @Override
    public void cekDevice(CekDeviceRequest dev_id) {
        cInterface.showProgressLoading();
        mInteractor.cekDevice(dev_id).subscribe(new Subscriber<CekDeviceResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
//                Log.i("kunam", e.getMessage());
                cInterface.hideProgresLoading();
                cInterface.onFailureRequest(ResponeError.getErrorMessage(e));
            }

            @Override
            public void onNext(CekDeviceResponse gCreditCard) {
//                cInterface.hideProgresLoading();
                mView.onSuccessCekDevice(gCreditCard);
            }
        });
    }

    @Override
    public void lobbyList(LobbyListRequest dev_id) {
//        cInterface.showProgressLoading();
        mInteractor.lobbyList(dev_id).subscribe(new Subscriber<LobbyListResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
//                Log.i("kunam", e.getMessage());
                cInterface.hideProgresLoading();
                cInterface.onFailureRequest(ResponeError.getErrorMessage(e));
            }

            @Override
            public void onNext(LobbyListResponse gCreditCard) {
//                cInterface.hideProgresLoading();
                mView.onSuccessLobbyList(gCreditCard);
            }
        });
    }

    @Override
    public void vehicleList(VehicleListRequest dev_id) {
//        cInterface.showProgressLoading();
        mInteractor.vehicleList(dev_id).subscribe(new Subscriber<VehicleListResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
//                Log.i("kunam", e.getMessage());
                cInterface.hideProgresLoading();
                cInterface.onFailureRequest(ResponeError.getErrorMessage(e));
            }

            @Override
            public void onNext(VehicleListResponse gCreditCard) {
                cInterface.hideProgresLoading();
                mView.onSuccessVehicleList(gCreditCard);
            }
        });
    }

    @Override
    public void ticketNote(TicketNoteRequest dev_id) {
        mInteractor.ticketNote(dev_id).subscribe(new Subscriber<TicketNoteResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.i("kunam", e.getMessage());
                cInterface.hideProgresLoading();
                cInterface.onFailureRequest(ResponeError.getErrorMessage(e));
            }

            @Override
            public void onNext(TicketNoteResponse gCreditCard) {
//                cInterface.hideProgresLoading();
                mView.onSuccessTicketNote(gCreditCard);
            }
        });
    }

    @Override
    public void vehicleType() {
        mInteractor.vehicleType().subscribe(new Subscriber<VehicleTypeResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.i("kunam", e.getMessage());
                cInterface.hideProgresLoading();
                cInterface.onFailureRequest(ResponeError.getErrorMessage(e));
            }

            @Override
            public void onNext(VehicleTypeResponse gCreditCard) {
                mView.onSuccessVehicleType(gCreditCard);
            }
        });
    }

    @Override
    public void vehicleColor() {
        mInteractor.vehicleColor().subscribe(new Subscriber<VehicleColorResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.i("kunam", e.getMessage());
                cInterface.hideProgresLoading();
                cInterface.onFailureRequest(ResponeError.getErrorMessage(e));
            }

            @Override
            public void onNext(VehicleColorResponse gCreditCard) {
                mView.onSuccessVehicleColor(gCreditCard);
            }
        });
    }

    @Override
    public void paymentType() {
        mInteractor.paymentType().subscribe(new Subscriber<PaymentTypeResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.i("kunam", e.getMessage());
                cInterface.hideProgresLoading();
                cInterface.onFailureRequest(ResponeError.getErrorMessage(e));
            }

            @Override
            public void onNext(PaymentTypeResponse gCreditCard) {
                mView.onSuccessPaymentType(gCreditCard);
            }
        });
    }
}
