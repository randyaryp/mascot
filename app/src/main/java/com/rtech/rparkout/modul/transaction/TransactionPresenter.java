package com.rtech.rparkout.modul.transaction;

import com.rtech.rparkout.component.network.response.SaveTransactionRequest;

/**
 * Created by Dhimas on 12/22/17.
 */

public interface TransactionPresenter {
    void saveTransaction(SaveTransactionRequest dev_id);
}
