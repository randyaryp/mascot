package com.rtech.rparkout.modul.setting;

import com.rtech.rparkout.component.network.request.TicketNoteRequest;
import com.rtech.rparkout.component.network.response.CekDeviceRequest;
import com.rtech.rparkout.component.network.response.CekDeviceResponse;
import com.rtech.rparkout.component.network.response.LobbyListRequest;
import com.rtech.rparkout.component.network.response.LobbyListResponse;
import com.rtech.rparkout.component.network.response.PaymentTypeResponse;
import com.rtech.rparkout.component.network.response.TicketNoteResponse;
import com.rtech.rparkout.component.network.response.VehicleColorResponse;
import com.rtech.rparkout.component.network.response.VehicleListRequest;
import com.rtech.rparkout.component.network.response.VehicleListResponse;
import com.rtech.rparkout.component.network.response.VehicleTypeResponse;

import rx.Observable;

/**
 * Created by Dhimas on 1/3/18.
 */

public interface SettingInteractor {
    Observable<CekDeviceResponse> cekDevice(CekDeviceRequest device_id);
    Observable<LobbyListResponse> lobbyList(LobbyListRequest device_id);
    Observable<VehicleListResponse> vehicleList(VehicleListRequest device_id);
    Observable<TicketNoteResponse> ticketNote(TicketNoteRequest device_id);
    Observable<VehicleTypeResponse> vehicleType();
    Observable<VehicleColorResponse> vehicleColor();
    Observable<PaymentTypeResponse> paymentType();
}
