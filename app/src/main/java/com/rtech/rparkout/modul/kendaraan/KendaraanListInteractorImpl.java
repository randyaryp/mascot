package com.rtech.rparkout.modul.kendaraan;

import com.rtech.rparkout.component.network.NetworkService;
import com.rtech.rparkout.component.network.request.EditKendaraanRequest;
import com.rtech.rparkout.component.network.request.KendaraanListRequest;
import com.rtech.rparkout.component.network.request.SearchKendaraanRequest;
import com.rtech.rparkout.component.network.response.EditKendaraanResponse;
import com.rtech.rparkout.component.network.response.KendaraanListResponse;
import com.rtech.rparkout.component.network.response.UploadImageRequest;
import com.rtech.rparkout.component.network.response.UploadImageResponse;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class KendaraanListInteractorImpl implements KendaraanListInteractor{
    private NetworkService mService;

    public KendaraanListInteractorImpl(NetworkService service) {
        mService = service;
    }

    @Override
    public Observable<KendaraanListResponse> kendaraanlist(KendaraanListRequest device_id) {
        return mService.parkingInList(device_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io());
    }

    @Override
    public Observable<KendaraanListResponse> searchKendaraan(SearchKendaraanRequest device_id) {
        return mService.search(device_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io());
    }

    @Override
    public Observable<EditKendaraanResponse> EditKendaraan(EditKendaraanRequest device_id) {
        return mService.EditKendaraan(device_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io());
    }

    @Override
    public Observable<UploadImageResponse> uploadImage(UploadImageRequest device_id) {
        return null;
    }
}
