package com.rtech.rparkout.modul.transaction;

import com.rtech.rparkout.component.network.NetworkService;
import com.rtech.rparkout.component.network.response.SaveTransactionRequest;
import com.rtech.rparkout.component.network.response.SaveTransactionResponse;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Dhimas on 1/3/18.
 */

public class TransactionInteractorImpl implements TransactionInteractor {
    private NetworkService mService;

    public TransactionInteractorImpl(NetworkService service) {
        mService = service;
    }

    @Override
    public Observable<SaveTransactionResponse> saveTransaction(SaveTransactionRequest device_id) {
        return mService.saveTransaction(device_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io());
    }
}
