package com.rtech.rparkout.modul.login;

import com.rtech.rparkout.component.network.NetworkService;
import com.rtech.rparkout.component.network.response.AuthRequest;
import com.rtech.rparkout.component.network.response.AuthResponse;
import com.rtech.rparkout.component.network.response.ParkingRateRequest;
import com.rtech.rparkout.component.network.response.ParkingRateResponse;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Dhimas on 1/3/18.
 */

public class LoginInteractorImpl implements LoginInteractor {
    private NetworkService mService;

    public LoginInteractorImpl(NetworkService service) {
        mService = service;
    }

    @Override
    public Observable<ParkingRateResponse> parkingRate(ParkingRateRequest device_id) {
        return mService.parkingRate(device_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io());
    }

    @Override
    public Observable<AuthResponse> login(AuthRequest log_in) {
        return mService.login(log_in)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io());
    }
}
