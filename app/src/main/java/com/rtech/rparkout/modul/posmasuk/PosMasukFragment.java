package com.rtech.rparkout.modul.posmasuk;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dantsu.escposprinter.EscPosPrinter;
import com.dantsu.escposprinter.connection.bluetooth.BluetoothPrintersConnections;
import com.dantsu.escposprinter.exceptions.EscPosBarcodeException;
import com.dantsu.escposprinter.exceptions.EscPosConnectionException;
import com.dantsu.escposprinter.exceptions.EscPosEncodingException;
import com.dantsu.escposprinter.exceptions.EscPosParserException;
import com.dantsu.escposprinter.textparser.PrinterTextParserImg;
import com.google.zxing.WriterException;
import com.mazenrashed.printooth.Printooth;
import com.mazenrashed.printooth.data.printable.ImagePrintable;
import com.mazenrashed.printooth.data.printable.Printable;
import com.mazenrashed.printooth.data.printable.TextPrintable;
import com.mazenrashed.printooth.data.printer.DefaultPrinter;
//import com.rtech.mascot.BuildConfig;
import com.mazenrashed.printooth.utilities.PrintingCallback;
import com.rtech.mascot.R;
import com.rtech.rparkout.component.adapter.CustomNoIconAdapter;
import com.rtech.rparkout.component.adapter.VehicleColorAdapter;
import com.rtech.rparkout.component.adapter.VehicleTypeAdapter;
import com.rtech.rparkout.component.network.NetworkManager;
import com.rtech.rparkout.component.network.NetworkService;
import com.rtech.rparkout.component.network.gson.GLobby;
import com.rtech.rparkout.component.network.gson.GVehicle;
import com.rtech.rparkout.component.network.gson.GVehicleColor;
import com.rtech.rparkout.component.network.gson.GVehicleType;
import com.rtech.rparkout.component.network.response.SaveDataPosMasukRequest;
import com.rtech.rparkout.component.network.response.SaveDataPosMasukResponse;
import com.rtech.rparkout.component.network.response.TicketNoteResponse;
import com.rtech.rparkout.component.network.response.UploadImageRequest;
import com.rtech.rparkout.component.network.response.UploadImageResponse;
import com.rtech.rparkout.component.network.response.VehicleColorResponse;
import com.rtech.rparkout.component.network.response.VehicleTypeResponse;
import com.rtech.rparkout.component.util.MethodUtil;
import com.rtech.rparkout.component.util.PreferenceManager;
import com.rtech.rparkout.modul.CommonInterface;
import com.rtech.rparkout.modul.capture.CaptureActivity;
import com.rtech.rparkout.modul.kendaraan.DetailKendaraan;
import com.rtech.rparkout.modul.navigation.MyRecyclerViewAdapter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;


public class PosMasukFragment extends Fragment implements CommonInterface, PosMasukView, AdapterView.OnItemSelectedListener {
    public PosMasukPresenter mPresenter;
    private TextView user, location, lobby;
    private Button buttonPosMasuk, buttonReset;
    private LinearLayout captureNopolMasuk, captureDepan, captureKanan, captureKiri, captureBelakang;
    private EditText editNopolMasuk;
    private View viewFragment;
    private RadioGroup groupRadio;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private static final int CAMERA_REQUEST = 1888;
    public static final int CAMERA_PERM_CODE = 101;
    public static final int CAMERA_REQUEST_CODE = 102;
    private RecyclerView scrollImageFront, scrollImageBack, scrollImageRight, scrollImageLeft;
    private TicketNoteResponse TicketNote;
    private Switch CarCall;
    MyRecyclerViewAdapter adapter, adapterFront, adapterRight, adapterLeft, adapterBack;

    List<GVehicleType> VehicleTypeList;
    List<GVehicleColor> VehicleColorList;

    ArrayList<Bitmap> frontPhotoBase64 = new ArrayList<>();
    ArrayList<Bitmap> rightPhotoBase64 = new ArrayList<>();
    ArrayList<Bitmap> leftPhotoBase64 = new ArrayList<>();
    ArrayList<Bitmap> backPhotoBase64 = new ArrayList<>();
    Bitmap photoImage, bitmap;
    private String photoImageBase64, noPOl, QRCod, imageID, capturePosition;
//    private String printerConnection;

    QRGEncoder qrgEncoder;
    Date currentTime;
    SimpleDateFormat sdf;
    private Dialog dialog;

    TextView spinnerKendaraan, spinnerWarna;
    ArrayList<String> arrayListKendaraan, arrayListWarna;
    Dialog dialogKendaraan, dialogWarna;

    String currentPhotoPath;
//    ImageView kunam;

    private String mCameraFileName;

    public PosMasukFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Printooth.INSTANCE.init(getContext());
//        connectionPrinter();
        imageID = "";
        capturePosition = "";
        viewFragment = inflater.inflate(R.layout.fragment_pos_masuk, container, false);
        initComponent();
        initEvent();
        return viewFragment;
    }

    @Override
    public void onResume() {
        if (PreferenceManager.getCaptureNoPol() != null) {
            editNopolMasuk.setText(PreferenceManager.getCaptureNoPol());
        }
        ArrayList<String> animalNames = new ArrayList<>();
        animalNames.add("Horse");
        // set up the RecyclerView
        MyRecyclerViewAdapter.ItemClickListener listener = new MyRecyclerViewAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Toast.makeText(getActivity(), "Item clicked: " + position, Toast.LENGTH_SHORT).show();
            }
        };
        LinearLayoutManager layoutManagerFront
                = new LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager layoutManagerBack
                = new LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager layoutManagerRight
                = new LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager layoutManagerLeft
                = new LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false);
        scrollImageFront.setLayoutManager(layoutManagerFront);
        scrollImageBack.setLayoutManager(layoutManagerBack);
        scrollImageRight.setLayoutManager(layoutManagerRight);
        scrollImageLeft.setLayoutManager(layoutManagerLeft);

        adapter = new MyRecyclerViewAdapter(getContext(), animalNames, "a");
        adapter.setClickListener(listener);

        switch (capturePosition) {
            case "front":
                if (photoImage != null) {
                    frontPhotoBase64.add(photoImage);
                }
                break;
            case "right":
                if (photoImage != null) {
                    rightPhotoBase64.add(photoImage);
                }
                break;
            case "left":
                if (photoImage != null) {
                    leftPhotoBase64.add(photoImage);
                }
                break;
            case "back":
                if (photoImage != null) {
                    backPhotoBase64.add(photoImage);
                }
                break;
            default:
//                break;
        }
//        FRONT
        if (frontPhotoBase64 == null || frontPhotoBase64.size() == 0) {
            scrollImageFront.setAdapter(adapter);
        } else {
            adapterFront = new MyRecyclerViewAdapter(getContext(), frontPhotoBase64);
            adapterFront.setClickListener(listener);
            scrollImageFront.setAdapter(adapterFront);
        }
//        RIGHT
        if (rightPhotoBase64 == null || rightPhotoBase64.size() == 0) {
            scrollImageRight.setAdapter(adapter);
        } else {
            adapterRight = new MyRecyclerViewAdapter(getContext(), rightPhotoBase64);
            adapterRight.setClickListener(listener);
            scrollImageRight.setAdapter(adapterRight);
        }
//        LEFT
        if (leftPhotoBase64 == null || leftPhotoBase64.size() == 0) {
            scrollImageLeft.setAdapter(adapter);
        } else {
            adapterLeft = new MyRecyclerViewAdapter(getContext(), leftPhotoBase64);
            adapterLeft.setClickListener(listener);
            scrollImageLeft.setAdapter(adapterLeft);
        }
//        BACK
        if (backPhotoBase64 == null || backPhotoBase64.size() == 0) {
            scrollImageBack.setAdapter(adapter);
        } else {
            adapterBack = new MyRecyclerViewAdapter(getContext(), backPhotoBase64);
            adapterBack.setClickListener(listener);
            scrollImageBack.setAdapter(adapterBack);
        }
        super.onResume();
    }

    private void initComponent() {
        mPresenter = new PosMasukPresenterImpl(this, this);
        groupRadio = viewFragment.findViewById(R.id.radioGroup1);
        RadioButton button;

        List<GVehicle> listVehicle = PreferenceManager.getVehicleList();
        for (int i = 0; i < listVehicle.size(); i++) {
            button = new RadioButton(getContext());
            button.setText(listVehicle.get(i).vehicle_name);
            button.setId(i);
            groupRadio.addView(button);
        }

        user = viewFragment.findViewById(R.id.user);
        user.setText(PreferenceManager.getUser());

//        kunam = viewFragment.findViewById(R.id.kunam);

        location = viewFragment.findViewById(R.id.location);
        location.setText(PreferenceManager.getSiteName());

        lobby = viewFragment.findViewById(R.id.lobby);
        String lobbyName = "";
        List<GLobby> lobbyList = PreferenceManager.getLobbyList();
        for (int i = 0; i < lobbyList.size(); i++) {
            if (lobbyList.get(i).id.equalsIgnoreCase(PreferenceManager.getLoggedInLobbyId())) {
                lobbyName = lobbyList.get(i).lobby_name;
            }
        }
        lobby.setText(lobbyName);

        buttonPosMasuk = viewFragment.findViewById(R.id.buttonPosMasuk);
        captureNopolMasuk = viewFragment.findViewById(R.id.linear_capture_nopol);
        editNopolMasuk = viewFragment.findViewById(R.id.edit_nopol_masuk);
        captureDepan = viewFragment.findViewById(R.id.captureDepan);
        captureBelakang = viewFragment.findViewById(R.id.captureBelakang);
        captureKanan = viewFragment.findViewById(R.id.captureKanan);
        captureKiri = viewFragment.findViewById(R.id.captureKiri);
        scrollImageFront = viewFragment.findViewById(R.id.scrollImageFront);
        scrollImageBack = viewFragment.findViewById(R.id.scrollImageBack);
        scrollImageRight = viewFragment.findViewById(R.id.scrollImageRight);
        scrollImageLeft = viewFragment.findViewById(R.id.scrollImageLeft);

        VehicleTypeResponse vecType = PreferenceManager.getVehicleType();
        VehicleColorResponse vecColor = PreferenceManager.getVehicleColor();

        TicketNote = PreferenceManager.getTicketNote();
        CarCall = viewFragment.findViewById(R.id.switchOnOff);
        buttonReset = viewFragment.findViewById(R.id.buttonReset);

        spinnerKendaraan=viewFragment.findViewById(R.id.spinnerKendaraan);

        // initialize array list
        arrayListKendaraan=new ArrayList<>();
        // set value in array list
        for (int i = 0; i<vecType.data.size(); i++){
            arrayListKendaraan.add(vecType.data.get(i).vehicletype);
        }

        spinnerKendaraan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Initialize dialog
                dialogKendaraan=new Dialog(getContext());

                // set custom dialog
                dialogKendaraan.setContentView(R.layout.dialog_searchable_spinner);

                // set custom height and width
                dialogKendaraan.getWindow().setLayout(650,800);

                // set transparent background
                dialogKendaraan.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                // show dialog
                dialogKendaraan.show();

                // Initialize and assign variable
                EditText editText=dialogKendaraan.findViewById(R.id.edit_text);
                ListView listView=dialogKendaraan.findViewById(R.id.list_view);

                // Initialize array adapter
                ArrayAdapter<String> adapter=new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1,arrayListKendaraan);

                // set adapter
                listView.setAdapter(adapter);
                editText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        adapter.getFilter().filter(s);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        // when item selected from list
                        // set selected item on textView
                        spinnerKendaraan.setText(adapter.getItem(position));

                        // Dismiss dialog
                        dialogKendaraan.dismiss();
                    }
                });
            }
        });

        spinnerWarna=viewFragment.findViewById(R.id.spinnerWarna);

        // initialize array list
        arrayListWarna=new ArrayList<>();
        // set value in array list
        for (int i = 0; i<vecColor.data.size(); i++){
            arrayListWarna.add(vecColor.data.get(i).color);
        }

        spinnerWarna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Initialize dialog
                dialogWarna=new Dialog(getContext());

                // set custom dialog
                dialogWarna.setContentView(R.layout.dialog_searchable_spinner);

                // set custom height and width
                dialogWarna.getWindow().setLayout(650,800);

                // set transparent background
                dialogWarna.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                // show dialog
                dialogWarna.show();

                // Initialize and assign variable
                EditText editText=dialogWarna.findViewById(R.id.edit_text);
                ListView listView=dialogWarna.findViewById(R.id.list_view);

                // Initialize array adapter
                ArrayAdapter<String> adapter=new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1,arrayListWarna);

                // set adapter
                listView.setAdapter(adapter);
                editText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        adapter.getFilter().filter(s);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        // when item selected from list
                        // set selected item on textView
                        spinnerWarna.setText(adapter.getItem(position));

                        // Dismiss dialog
                        dialogWarna.dismiss();
                    }
                });
            }
        });
    }

    private void initEvent() {
        buttonPosMasuk.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if (PreferenceManager.getNamaPrinter() == null ||
                        PreferenceManager.getNamaPrinter() == "" ||
                        PreferenceManager.getAlamatPrinter() == null ||
                        PreferenceManager.getAlamatPrinter() == "") {
                    Toast.makeText(getActivity(), "Mohon atur printer dahulu", Toast.LENGTH_LONG).show();
                }
                else{
                    int selectedId = groupRadio.getCheckedRadioButtonId();
                    List<GVehicle> listVehicle = PreferenceManager.getVehicleList();
                    String vehicle = "";
                    if (selectedId != -1){
                        vehicle = listVehicle.get(selectedId).vehicle_name;
                    }
                    Printooth.INSTANCE.setPrinter(PreferenceManager.getNamaPrinter(), PreferenceManager.getAlamatPrinter());

                    try {
                        if (!Printooth.INSTANCE.hasPairedPrinter()){
                            Toast.makeText(getActivity(), "Mohon hubungkan printer dahulu", Toast.LENGTH_LONG).show();
                        }else{
                            long yourmilliseconds = System.currentTimeMillis();
                            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            currentTime = new Date(yourmilliseconds);

                            String car_call = "N";
                            if (CarCall.isChecked()) {
                                car_call= "Y";
                            }

                            SaveDataPosMasukRequest posMasukRequest = new SaveDataPosMasukRequest(
                                    PreferenceManager.getSite(),
                                    PreferenceManager.getImei(),
                                    editNopolMasuk.getText().toString(),
                                    vehicle,
                                    sdf.format(currentTime),
                                    imageID,
                                    PreferenceManager.getLoggedInLobbyId(),
                                    spinnerKendaraan.getText().toString(),
                                    spinnerWarna.getText().toString(),
                                    car_call
                            );
                            mPresenter.saveDataPosMasuk(posMasukRequest);
//                            cetakTestPrint();
                        }
                    }
                    catch (Exception e){
                        e.printStackTrace();
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        captureNopolMasuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), CaptureActivity.class);
                startActivity(intent);
            }
        });
//        captureDepan.setOnClickListener(new View.OnClickListener() {
//            @RequiresApi(api = Build.VERSION_CODES.M)
//            @Override
//            public void onClick(View v) {
//                if (frontPhotoBase64 != null && frontPhotoBase64.size() > 4) {
//                    Toast.makeText(getActivity(), "Foto depan sudah maksimum", Toast.LENGTH_SHORT).show();
//                } else {
//                    if (getContext().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
//                    } else {
//                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                        capturePosition = "front";
//                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
//                    }
//                }
//            }
//        });
        captureDepan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                capturePosition = "front";
                verifyPermissions();
            }
        });
//        captureKanan.setOnClickListener(new View.OnClickListener() {
//            @RequiresApi(api = Build.VERSION_CODES.M)
//            @Override
//            public void onClick(View v) {
//                if (rightPhotoBase64 != null && rightPhotoBase64.size() > 4) {
//                    Toast.makeText(getActivity(), "Foto kanan sudah maksimum", Toast.LENGTH_SHORT).show();
//                } else {
//                    if (getContext().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
//                    } else {
//                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                        capturePosition = "right";
//                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
//                    }
//                }
//            }
//        });
        captureKanan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                capturePosition = "right";
                verifyPermissions();
            }
        });
//        captureKiri.setOnClickListener(new View.OnClickListener() {
//            @RequiresApi(api = Build.VERSION_CODES.M)
//            @Override
//            public void onClick(View v) {
//                if (leftPhotoBase64 != null && leftPhotoBase64.size() > 4) {
//                    Toast.makeText(getActivity(), "Foto kiri sudah maksimum", Toast.LENGTH_SHORT).show();
//                } else {
//                    if (getContext().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
//                    } else {
//                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                        capturePosition = "left";
//                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
//                    }
//                }
//            }
//        });
        captureKiri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                capturePosition = "left";
                verifyPermissions();
            }
        });
//        captureBelakang.setOnClickListener(new View.OnClickListener() {
//            @RequiresApi(api = Build.VERSION_CODES.M)
//            @Override
//            public void onClick(View v) {
//                if (backPhotoBase64 != null && backPhotoBase64.size() > 4) {
//                    Toast.makeText(getActivity(), "Foto belakang sudah maksimum", Toast.LENGTH_SHORT).show();
//                } else {
//                    if (getContext().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
//                    } else {
//                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                        capturePosition = "back";
//                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
//                    }
//                }
//            }
//        });
        captureBelakang.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                capturePosition = "back";
                verifyPermissions();
            }
        });
        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetSelectedForm();
            }
        });
    }

    private void verifyPermissions(){
        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA};

        if(ContextCompat.checkSelfPermission(getContext(),
                permissions[0]) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getContext(),
                permissions[1]) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getContext(),
                permissions[2]) == PackageManager.PERMISSION_GRANTED){
            dispatchTakePictureIntent();
        }else{
            ActivityCompat.requestPermissions(getActivity(),
                    permissions,
                    CAMERA_PERM_CODE);
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getContext(),
                        "com.rtech.mascot.provider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
//        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }



//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
//            photoImage = (Bitmap) data.getExtras().get("data");
//            Log.i("kunam width", String.valueOf(photoImage.getWidth()));
//            Log.i("kunam height", String.valueOf(photoImage.getHeight()));
//            Bitmap resized = Bitmap.createScaledBitmap(photoImage,(int)(photoImage.getWidth()*2), (int)(photoImage.getHeight()*2), true);
//            photoImageBase64 = getBase64String(resized);
//            String upl = "data:image/jpeg;base64," + photoImageBase64;
//
//            UploadImageRequest req = new UploadImageRequest(upl);
//            mPresenter.uploadImage(req);
//        }
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CAMERA_REQUEST_CODE){
            if(resultCode == Activity.RESULT_OK){
                File f = new File(currentPhotoPath);
                try {
                    photoImage = (Bitmap) getThumbnail(Uri.fromFile(f));
                    photoImageBase64 = getBase64String(photoImage);
                    String upl = "data:image/jpeg;base64," + photoImageBase64;
                    UploadImageRequest req = new UploadImageRequest(upl);
                    mPresenter.uploadImage(req);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.d("tag", "ABsolute Url of Image is " + Uri.fromFile(f));

                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri contentUri = Uri.fromFile(f);
                mediaScanIntent.setData(contentUri);
//                this.sendBroadcast(mediaScanIntent);
                getContext().sendBroadcast(mediaScanIntent);
            }

        }
    }

    public Bitmap getThumbnail(Uri uri) throws FileNotFoundException, IOException{
        InputStream input = getContext().getContentResolver().openInputStream(uri);

        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither=true;//optional
        onlyBoundsOptions.inPreferredConfig=Bitmap.Config.ARGB_8888;//optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();

        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1)) {
            return null;
        }

        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

        double ratio = (originalSize > 100) ? (originalSize / 100) : 1.0;

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
        bitmapOptions.inDither = true; //optional
        bitmapOptions.inPreferredConfig=Bitmap.Config.ARGB_8888;//
        input = getContext().getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();
        return bitmap;
    }

    private static int getPowerOfTwoForSampleRatio(double ratio){
        int k = Integer.highestOneBit((int)Math.floor(ratio));
        if(k==0) return 1;
        else return k;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == CAMERA_PERM_CODE){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                dispatchTakePictureIntent();
            }else {
                Toast.makeText(getContext(), "Camera Permission is Required to Use camera.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String getBase64String(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        byte[] imageBytes = baos.toByteArray();

        String base64String = Base64.encodeToString(imageBytes, Base64.NO_WRAP);

        return base64String;
    }

    @Override
    public void onSuccessUploadImage(UploadImageResponse dev_response) {
        if (dev_response.success) {
            if (imageID.equalsIgnoreCase("")) {
                imageID = String.valueOf(dev_response.img_id);
            } else {
                imageID = imageID + "|" + String.valueOf(dev_response.img_id);
            }
        } else {
            MethodUtil.showCustomToast(getActivity(), "Upload failed", R.drawable.ic_warning_red_24dp);
        }
        photoImage = null;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onSuccessSaveDataPosMasuk(SaveDataPosMasukResponse dev_response) {

        if (dev_response.success){
            QRCod = dev_response.qrcode;
            noPOl = editNopolMasuk.getText().toString();
            PreferenceManager.setCaptureNoPol(null);
            editNopolMasuk.setText("");
            frontPhotoBase64 = new ArrayList<>();
            rightPhotoBase64 = new ArrayList<>();
            leftPhotoBase64 = new ArrayList<>();
            backPhotoBase64 = new ArrayList<>();
            scrollImageFront.setAdapter(adapter);
            scrollImageRight.setAdapter(adapter);
            scrollImageLeft.setAdapter(adapter);
            scrollImageBack.setAdapter(adapter);
            capturePosition = "";
            spinnerWarna.setText("");
            spinnerKendaraan.setText("");
            cetakStrukMasuk(PreferenceManager.getSite(), QRCod);
//            cetakTestPrint();
            initCetak2();
        }else{
            MethodUtil.showCustomToast(getActivity(), dev_response.message, R.drawable.ic_warning_red_24dp);
        }
    }

    @Override
    public void showProgressLoading() {
        Log.i("kunam", "masuk loading");
        progressBar.show(getContext(), "loading", false, null);
    }

    @Override
    public void hideProgresLoading() {
        progressBar.getDialog().dismiss();
    }

    @Override
    public NetworkService getService() {
        return NetworkManager.getInstance();
    }

    @Override
    public void onFailureRequest(String msg) {
        MethodUtil.showCustomToast(getActivity(), msg, R.drawable.ic_warning_red_24dp);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void cetakStrukMasuk(String site, String noTiket){
        Printooth.INSTANCE.setPrinter(PreferenceManager.getNamaPrinter(), PreferenceManager.getAlamatPrinter());
        int selectedId = groupRadio.getCheckedRadioButtonId();
        List<GVehicle> listVehicle = PreferenceManager.getVehicleList();
        String vehicle = "";
        if (selectedId != -1){
            vehicle = listVehicle.get(selectedId).vehicle_name;
        }
        if (!Printooth.INSTANCE.hasPairedPrinter()){
        }

        if (getContext().checkSelfPermission(Manifest.permission.BLUETOOTH_SCAN) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.BLUETOOTH_SCAN}, MY_CAMERA_PERMISSION_CODE);
        }
        if (getContext().checkSelfPermission(Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.BLUETOOTH_CONNECT}, MY_CAMERA_PERMISSION_CODE);
        }

        ArrayList<Printable> printables = new ArrayList<Printable>();
        TextPrintable printableTitle = (TextPrintable) new TextPrintable.Builder()
                .setText("Mascot Valet System")
                .setFontSize((byte) 1)
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .setNewLinesAfter(2)
                .build();
        TextPrintable printableSubTitle = (TextPrintable) new TextPrintable.Builder()
                .setText("Nota Parkir")
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .setNewLinesAfter(3)
                .build();
        TextPrintable printableSubLokasi = (TextPrintable) new TextPrintable.Builder()
                .setText("   Lokasi      :" + PreferenceManager.getSiteName())
                .setNewLinesAfter(1)
                .build();

        String lobbyName = "";
        List<GLobby> lobbyList = PreferenceManager.getLobbyList();
        for (int i = 0; i < lobbyList.size(); i++) {
            if (lobbyList.get(i).id.equalsIgnoreCase(PreferenceManager.getLoggedInLobbyId())) {
                lobbyName = lobbyList.get(i).lobby_name;
            }
        }
        TextPrintable printableLobby = (TextPrintable) new TextPrintable.Builder()
                .setText("   Lobby       :" + lobbyName)
                .setNewLinesAfter(1)
                .build();
        TextPrintable printableUser = (TextPrintable) new TextPrintable.Builder()
                .setText("   Petugas     :" + PreferenceManager.getUser())
                .setNewLinesAfter(1)
                .build();
        TextPrintable printableNoPol = (TextPrintable) new TextPrintable.Builder()
                .setText("   No Polisi   :" + noPOl)
                .setNewLinesAfter(1)
                .build();
        TextPrintable printablePaket = (TextPrintable) new TextPrintable.Builder()
                .setText("   Transaksi   :" + vehicle)
                .setNewLinesAfter(1)
                .build();
        TextPrintable printableFooter1 = (TextPrintable) new TextPrintable.Builder()
                .setText(TicketNote.data.in1)
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .setNewLinesAfter(1)
                .build();
        TextPrintable printableFooter2 = (TextPrintable) new TextPrintable.Builder()
                .setText(TicketNote.data.in2)
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .setNewLinesAfter(1)
                .build();
        TextPrintable printableFooter3 = (TextPrintable) new TextPrintable.Builder()
                .setText(TicketNote.data.in3)
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .setNewLinesAfter(1)
                .build();
        TextPrintable printableFooter4 = (TextPrintable) new TextPrintable.Builder()
                .setText(TicketNote.data.in4)
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .setNewLinesAfter(1)
                .build();
        TextPrintable printableFooter5 = (TextPrintable) new TextPrintable.Builder()
                .setText(TicketNote.data.in5)
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .setNewLinesAfter(3)
                .build();
        TextPrintable printableenter = (TextPrintable) new TextPrintable.Builder()
                .setText("\n")
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .build();

        String urlCarCall = "http://valetdemo.rdev.co.id/public/info/" + site + noTiket;
        qrgEncoder = new QRGEncoder(urlCarCall, null, QRGContents.Type.TEXT, 300);
        try {
            bitmap = qrgEncoder.encodeAsBitmap();
        } catch (WriterException e) {
            Log.e("Tag", e.toString());
        }

        ImagePrintable QRCodes = new ImagePrintable.
                Builder(bitmap).
                setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER()).
                setNewLinesAfter(1).
                build();

        TextPrintable printableNoTicket = (TextPrintable) new TextPrintable.Builder()
                .setText(noTiket)
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .setNewLinesAfter(3)
                .build();

        printables.add(printableTitle);
        printables.add(printableSubTitle);
        printables.add(printableSubLokasi);
        printables.add(printableLobby);
        printables.add(printableUser);
        printables.add(printableNoPol);
        printables.add(printablePaket);
        printables.add(QRCodes);
        printables.add(printableNoTicket);
        printables.add(printableFooter1);
        printables.add(printableFooter2);
        printables.add(printableFooter3);
        printables.add(printableFooter4);
        if (CarCall.isChecked()){
            printables.add(printableFooter5);
        }
        printables.add(printableenter);
        printables.add(printableenter);
        printables.add(printableenter);
        Printooth.INSTANCE.printer().print(printables);
    }

    private void cetakTestPrint(){
        ArrayList<Printable> printables = new ArrayList<Printable>();
        TextPrintable printable3 = (TextPrintable) new TextPrintable.Builder()
                .setText("Test Print OK")
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .setNewLinesAfter(3)
                .build();
        TextPrintable printableenter = (TextPrintable) new TextPrintable.Builder()
                .setText("\n")
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .build();
        printables.add(printable3);
        printables.add(printableenter);
        Printooth.INSTANCE.setPrinter(PreferenceManager.getNamaPrinter(), PreferenceManager.getAlamatPrinter());
        Printooth.INSTANCE.printer().print(printables);
    }

    private void initCetak2(){
        LayoutInflater inflator = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflator.inflate(R.layout.dialog, null);

        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        dialog = new Dialog(getContext(), R.style.CustomDialog);
        dialog.setContentView(view);
        dialog.setCancelable(false);
        dialog.setOnCancelListener(null);
        dialog.show();

        TextView alertTitle = view.findViewById(R.id.alertTitle);
        alertTitle.setText("Print Copy 2!");

        Button button = view.findViewById(R.id.buttonProses);
        button.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                cetakStrukMasuk(PreferenceManager.getSite(), QRCod);
                initCetak3();
            }
        });
        Button buttonCancel = view.findViewById(R.id.buttonCancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    private void initCetak3(){
        LayoutInflater inflator = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflator.inflate(R.layout.dialog, null);

        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        dialog = new Dialog(getContext(), R.style.CustomDialog);
        dialog.setContentView(view);
        dialog.setCancelable(false);
        dialog.setOnCancelListener(null);
        dialog.show();

        TextView alertTitle = view.findViewById(R.id.alertTitle);
        alertTitle.setText("Print Copy 3!");

        Button button = view.findViewById(R.id.buttonProses);
        button.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                cetakStrukMasuk(PreferenceManager.getSite(), QRCod);
            }
        });
        Button buttonCancel = view.findViewById(R.id.buttonCancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public void resetSelectedForm(){
        CarCall.setChecked(false);
        groupRadio.setSelected(false);
        PreferenceManager.setCaptureNoPol(null);
        editNopolMasuk.setText("");
        frontPhotoBase64 = new ArrayList<>();
        rightPhotoBase64 = new ArrayList<>();
        leftPhotoBase64 = new ArrayList<>();
        backPhotoBase64 = new ArrayList<>();
        scrollImageFront.setAdapter(adapter);
        scrollImageRight.setAdapter(adapter);
        scrollImageLeft.setAdapter(adapter);
        scrollImageBack.setAdapter(adapter);
        capturePosition = "";
        spinnerWarna.setText("");
        spinnerKendaraan.setText("");
    }
}
