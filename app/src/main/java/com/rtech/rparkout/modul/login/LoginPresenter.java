package com.rtech.rparkout.modul.login;

import com.rtech.rparkout.component.network.response.AuthRequest;
import com.rtech.rparkout.component.network.response.ParkingRateRequest;

/**
 * Created by Dhimas on 12/22/17.
 */

public interface LoginPresenter {
    void parkingRate(ParkingRateRequest dev_id);
    void login(AuthRequest log_in);
}
