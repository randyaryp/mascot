package com.rtech.rparkout.modul.posmasuk;

import com.rtech.rparkout.component.network.NetworkService;
import com.rtech.rparkout.component.network.response.SaveDataPosMasukRequest;
import com.rtech.rparkout.component.network.response.SaveDataPosMasukResponse;
import com.rtech.rparkout.component.network.response.UploadImageRequest;
import com.rtech.rparkout.component.network.response.UploadImageResponse;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class PosMasukInteractorImpl implements PosMasukInteractor {
    private NetworkService mService;

    public PosMasukInteractorImpl(NetworkService service) {
        mService = service;
    }

    @Override
    public Observable<UploadImageResponse> uploadImage(UploadImageRequest device_id) {
        return mService.uploadImage(device_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io());
    }

    @Override
    public Observable<SaveDataPosMasukResponse> saveDataPosMasuk(SaveDataPosMasukRequest device_id) {
        return mService.saveDataPosMasuk(device_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io());
    }
}
