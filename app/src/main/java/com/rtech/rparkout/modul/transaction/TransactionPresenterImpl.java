package com.rtech.rparkout.modul.transaction;

import android.util.Log;

import com.rtech.rparkout.component.network.NetworkService;
import com.rtech.rparkout.component.network.response.SaveTransactionRequest;
import com.rtech.rparkout.component.network.response.SaveTransactionResponse;
import com.rtech.rparkout.modul.CommonInterface;

import rx.Subscriber;

/**
 * Created by Dhimas on 12/22/17.
 */

public class TransactionPresenterImpl implements TransactionPresenter {
    private TransactionView mView;
    private CommonInterface cInterface;
    private NetworkService mService;
    private TransactionInteractor mInteractor;

    public TransactionPresenterImpl(CommonInterface commonInterface, TransactionView view) {
        mView = view;
        cInterface = commonInterface;
        mService = cInterface.getService();
        mInteractor = new TransactionInteractorImpl(mService);
    }


    @Override
    public void saveTransaction(final SaveTransactionRequest dev_id) {
        cInterface.showProgressLoading();
        mInteractor.saveTransaction(dev_id).subscribe(new Subscriber<SaveTransactionResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.i("kunamer", e.getMessage());
                cInterface.hideProgresLoading();
                mView.onFailedSaveTransaction(dev_id);
            }

            @Override
            public void onNext(SaveTransactionResponse gCreditCard) {
                cInterface.hideProgresLoading();
                mView.onSuccessSaveTransaction(gCreditCard);
            }
        });
    }
}
