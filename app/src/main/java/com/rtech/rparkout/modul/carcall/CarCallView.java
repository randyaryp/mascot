package com.rtech.rparkout.modul.carcall;

import com.rtech.rparkout.component.network.response.CarCallResponse;

public interface CarCallView {
    void onSuccessCarCall(CarCallResponse dev_response);
}
