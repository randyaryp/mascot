package com.rtech.rparkout.modul.poskeluar;

import android.Manifest;
//import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.mazenrashed.printooth.Printooth;
import com.mazenrashed.printooth.data.printable.Printable;
import com.mazenrashed.printooth.data.printable.TextPrintable;
import com.mazenrashed.printooth.data.printer.DefaultPrinter;
import com.mazenrashed.printooth.utilities.PrintingCallback;
import com.rtech.mascot.R;
import com.rtech.rparkout.component.network.NetworkManager;
import com.rtech.rparkout.component.network.NetworkService;
import com.rtech.rparkout.component.network.gson.GLobby;
import com.rtech.rparkout.component.network.gson.GPaymentType;
import com.rtech.rparkout.component.network.gson.GVehicle;
import com.rtech.rparkout.component.network.response.AuthResponse;
import com.rtech.rparkout.component.network.response.ParkingRateRequest;
import com.rtech.rparkout.component.network.response.ParkingRateResponse;
import com.rtech.rparkout.component.network.response.SaveDataPosKeluarRequest;
import com.rtech.rparkout.component.network.response.SaveDataPosKeluarResponse;
import com.rtech.rparkout.component.network.response.TicketNoteResponse;
import com.rtech.rparkout.component.util.MethodUtil;
import com.rtech.rparkout.component.util.PreferenceManager;
import com.rtech.rparkout.modul.CommonInterface;
import com.rtech.rparkout.modul.capture.CaptureActivity;
import com.rtech.rparkout.modul.kendaraan.DetailKendaraan;
import com.rtech.rparkout.modul.posmasuk.PosMasukView;

import java.util.ArrayList;
import java.util.List;

public class PosKeluarFragment extends Fragment implements CommonInterface, ParkingRateView {
    public ParkingRatePresenter mPresenter;
    private View viewFragment;
    private TextView user, location, lobby, tarif, duration, policeno;
    private LinearLayout linearCaptureQR, detail_kendaraan, pembayaran;
    private EditText edit_no_ticket;
    private ParkingRateResponse transaction;
    private TicketNoteResponse TicketNote;
    private RadioGroup payment;

    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private static final int CAMERA_REQUEST = 1888;

    private Button buttonPosKeluar, buttonReset;
//    private String printerConnection;

    public PosKeluarFragment() {
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Printooth.INSTANCE.init(getContext());
//        connectionPrinter();
        viewFragment = inflater.inflate(R.layout.fragment_pos_keluar, container, false);
        initComponent();
        initEvents();
        return viewFragment;
    }

    private void connectionPrinter(){
//        printerConnection="";
        ArrayList<Printable> printables = new ArrayList<Printable>();
        TextPrintable printableSubTitle = (TextPrintable) new TextPrintable.Builder()
                .setText("")
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .setNewLinesAfter(1)
                .build();
        printables.add(printableSubTitle);
        try {
            Printooth.INSTANCE.printer().print(printables);

            Printooth.INSTANCE.printer().setPrintingCallback(new PrintingCallback() {
                @Override
                public void connectingWithPrinter() {
                }

                @Override
                public void printingOrderSentSuccessfully() {
                }

                @Override
                public void connectionFailed(String s) {
//                    printerConnection = "FAILED BLOG";
                }

                @Override
                public void onError(String s) {
//                    printerConnection = "FAILED BLOG";
                }

                @Override
                public void onMessage(String s) {
                }

                @Override
                public void disconnected() {
                }
            });


        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void initComponent() {
        mPresenter = new ParkingRatePresenterImpl(this, this);
        linearCaptureQR = viewFragment.findViewById(R.id.linear_capture_qr);
        user = viewFragment.findViewById(R.id.user);
        user.setText(PreferenceManager.getUser());

        location = viewFragment.findViewById(R.id.location);
        location.setText(PreferenceManager.getSiteName());

        lobby = viewFragment.findViewById(R.id.lobby);
        String lobbyName = "";
        List<GLobby> lobbyList = PreferenceManager.getLobbyList();
        for (int i=0; i< lobbyList.size(); i++){
            if (lobbyList.get(i).id.equalsIgnoreCase(PreferenceManager.getLoggedInLobbyId())){
                lobbyName = lobbyList.get(i).lobby_name;
            }
        }
        lobby.setText(lobbyName);

        detail_kendaraan = viewFragment.findViewById(R.id.detail_kendaraan);
        tarif = viewFragment.findViewById(R.id.tarif);
        pembayaran = viewFragment.findViewById(R.id.pembayaran);
        duration = viewFragment.findViewById(R.id.duration);
        policeno = viewFragment.findViewById(R.id.policeno);
        edit_no_ticket = viewFragment.findViewById(R.id.edit_no_ticket);
        buttonPosKeluar = viewFragment.findViewById(R.id.buttonPosKeluar);
        TicketNote = PreferenceManager.getTicketNote();

        payment = viewFragment.findViewById(R.id.payment);
        RadioButton button;
        List<GPaymentType> listPaymentType = PreferenceManager.getPaymenttype().data;
        for (int i = 0; i < listPaymentType.size(); i++) {
            button = new RadioButton(getContext());
            button.setText(listPaymentType.get(i).payment_name);
            button.setId(Integer.valueOf(listPaymentType.get(i).payment_code));
            payment.addView(button);
            if (i==0){
                button.setChecked(true);
            }
        }

        buttonReset = viewFragment.findViewById(R.id.buttonReset);
    }

    private void initEvents() {
        linearCaptureQR.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), QRScanner.class);
                startActivity(intent);
            }
        });

        edit_no_ticket.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() == 6){
                    ParkingRateRequest parkingRateRequest = new ParkingRateRequest(edit_no_ticket.getText().toString(), PreferenceManager.getSite());
                    mPresenter.parkingRate(parkingRateRequest);
                }
            }
        });

        buttonPosKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (transaction == null){
                    MethodUtil.showCustomToast(getActivity(), "Silahkan scan tiket dahulu", R.drawable.ic_warning_red_24dp);
                }
                else if (PreferenceManager.getNamaPrinter() == null || PreferenceManager.getAlamatPrinter() == null) {
                    Toast.makeText(getActivity(), "Mohon atur printer dahulu", Toast.LENGTH_LONG).show();
                }
                else{
                    int selectedId = payment.getCheckedRadioButtonId();
                    Printooth.INSTANCE.setPrinter(PreferenceManager.getNamaPrinter(), PreferenceManager.getAlamatPrinter());
                    try {
                        AuthResponse auth = PreferenceManager.getLoginAuth();
                        SaveDataPosKeluarRequest req = new SaveDataPosKeluarRequest(
                                edit_no_ticket.getText().toString(),
                                PreferenceManager.getUser(),
                                auth.data.device_id,
                                PreferenceManager.getSite(),
                                auth.data.shift_id,
                                PreferenceManager.getLoggedInLobbyId(),
                                transaction.datetimeout,
                                transaction.durasi1,
                                transaction.parking_fee.biayaparkir,
                                "0",
                                "0",
                                String.valueOf(selectedId),
                                "0"
                        );
                        mPresenter.saveDataPosKeluar(req);
                    }catch (Exception e){
                        e.printStackTrace();
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }
        });
        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetSelectedForm();
            }
        });
    }

    @Override
    public void onResume() {
        if (PreferenceManager.getParkingticketno() != null){
            edit_no_ticket.setText(PreferenceManager.getParkingticketno());
            detail_kendaraan.setVisibility(View.VISIBLE);
            pembayaran.setVisibility(View.VISIBLE);
            ParkingRateRequest parkingRateRequest = new ParkingRateRequest(PreferenceManager.getParkingticketno(), PreferenceManager.getSite());
            mPresenter.parkingRate(parkingRateRequest);
        }else{
            detail_kendaraan.setVisibility(View.GONE);
            pembayaran.setVisibility(View.GONE);
        }
        super.onResume();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void showProgressLoading() {
        progressBar.show(getContext(), "", false, null);
    }

    @Override
    public void hideProgresLoading() {
        progressBar.getDialog().dismiss();
    }

    @Override
    public NetworkService getService() {
        return NetworkManager.getInstance();
    }

    @Override
    public void onFailureRequest(String msg) {
        MethodUtil.showCustomToast(getActivity(), msg, R.drawable.ic_warning_red_24dp);
    }

    @Override
    public void onSuccessGetParkingrate(ParkingRateResponse dev_response) {
        PreferenceManager.setParkingTicketNo(null);
        if (dev_response.success){
            edit_no_ticket.setText(dev_response.message);
            detail_kendaraan.setVisibility(View.VISIBLE);
            pembayaran.setVisibility(View.VISIBLE);
            transaction = dev_response;
            tarif.setText("Rp " + MethodUtil.toCurrencyFormat(dev_response.parking_fee.biayaparkir));
            policeno.setText(dev_response.policeno);
            duration.setText(dev_response.durasi2);
            PreferenceManager.setParkingTicketNo(null);
        }else{
            MethodUtil.showCustomToast(getActivity(), dev_response.message, R.drawable.ic_warning_red_24dp);
            detail_kendaraan.setVisibility(View.GONE);
            pembayaran.setVisibility(View.GONE);
            edit_no_ticket.setText("");
            PreferenceManager.setParkingTicketNo(null);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onSuccessSaveDataPosKeluar(SaveDataPosKeluarResponse dev_response) {
        if (dev_response.success){
            cetakStrukKeluar(dev_response);
            PreferenceManager.setCaptureNoPol(null);
            detail_kendaraan.setVisibility(View.GONE);
            pembayaran.setVisibility(View.GONE);
            edit_no_ticket.setText("");
        }else{
            MethodUtil.showCustomToast(getActivity(), "Transactions failed", R.drawable.ic_warning_red_24dp);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void cetakStrukKeluar(SaveDataPosKeluarResponse transactions){
        Printooth.INSTANCE.setPrinter(PreferenceManager.getNamaPrinter(), PreferenceManager.getAlamatPrinter());
        if (!Printooth.INSTANCE.hasPairedPrinter()){

        }

        if (getContext().checkSelfPermission(Manifest.permission.BLUETOOTH_SCAN) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.BLUETOOTH_SCAN}, MY_CAMERA_PERMISSION_CODE);
        }
        if (getContext().checkSelfPermission(Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.BLUETOOTH_CONNECT}, MY_CAMERA_PERMISSION_CODE);
        }

        ArrayList<Printable> printables = new ArrayList<Printable>();
        TextPrintable printableTitle = (TextPrintable) new TextPrintable.Builder()
                .setText("Mascot Valet System")
                .setFontSize((byte) 1)
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .setNewLinesAfter(2)
                .build();
        TextPrintable printableSubTitle = (TextPrintable) new TextPrintable.Builder()
                .setText("Nota Parkir")
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .setNewLinesAfter(3)
                .build();
        TextPrintable printableSubLokasi = (TextPrintable) new TextPrintable.Builder()
                .setText("   Lokasi       : " + PreferenceManager.getSiteName())
                .setNewLinesAfter(1)
                .build();

        String lobbyName = "";
        List<GLobby> lobbyList = PreferenceManager.getLobbyList();
        for (int i = 0; i < lobbyList.size(); i++) {
            if (lobbyList.get(i).id.equalsIgnoreCase(PreferenceManager.getLoggedInLobbyId())) {
                lobbyName = lobbyList.get(i).lobby_name;
            }
        }
        TextPrintable printableLobby = (TextPrintable) new TextPrintable.Builder()
                .setText("   Lobby        : " + lobbyName)
                .setNewLinesAfter(1)
                .build();
        TextPrintable printableUser = (TextPrintable) new TextPrintable.Builder()
                .setText("   Petugas      : " + PreferenceManager.getUser())
                .setNewLinesAfter(1)
                .build();
        TextPrintable printableNoPol = (TextPrintable) new TextPrintable.Builder()
                .setText("   No Polisi    : " + transaction.policeno)
                .setNewLinesAfter(1)
                .build();
        TextPrintable printableJamMasuk = (TextPrintable) new TextPrintable.Builder()
                .setText("   Jam Masuk    : " + transaction.datetimein)
                .setNewLinesAfter(1)
                .build();
        TextPrintable printableJamKeluar = (TextPrintable) new TextPrintable.Builder()
                .setText("   Jam Keluar   : " + transaction.datetimeout)
                .setNewLinesAfter(1)
                .build();
        TextPrintable printableDurasi = (TextPrintable) new TextPrintable.Builder()
                .setText("   Durasi       : " + transaction.durasi2)
                .setNewLinesAfter(1)
                .build();
        TextPrintable printableBiaya = (TextPrintable) new TextPrintable.Builder()
                .setText("   Biaya Valet  : Rp " + MethodUtil.toCurrencyFormat(transaction.parking_fee.biayaparkir))
                .setNewLinesAfter(1)
                .build();
        TextPrintable printableFooter1 = (TextPrintable) new TextPrintable.Builder()
                .setText(TicketNote.data.out1)
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .setNewLinesAfter(1)
                .build();
        TextPrintable printableFooter2 = (TextPrintable) new TextPrintable.Builder()
                .setText(TicketNote.data.out2)
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .setNewLinesAfter(1)
                .build();
        TextPrintable printableFooter3 = (TextPrintable) new TextPrintable.Builder()
                .setText(TicketNote.data.out3)
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .setNewLinesAfter(1)
                .build();
        TextPrintable printableFooter4 = (TextPrintable) new TextPrintable.Builder()
                .setText(TicketNote.data.out4)
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .setNewLinesAfter(1)
                .build();
        TextPrintable printableFooter5 = (TextPrintable) new TextPrintable.Builder()
                .setText(TicketNote.data.out5)
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .setNewLinesAfter(3)
                .build();
        TextPrintable printableenter = (TextPrintable) new TextPrintable.Builder()
                .setText("\n")
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .build();

//        String urlCarCall = "http://valetdemo.rdev.co.id/public/info/" + site + noTiket;
//        qrgEncoder = new QRGEncoder(urlCarCall, null, QRGContents.Type.TEXT, 300);
//        try {
//            bitmap = qrgEncoder.encodeAsBitmap();
//        } catch (WriterException e) {
//            Log.e("Tag", e.toString());
//        }
//
//        ImagePrintable QRCodes = new ImagePrintable.
//                Builder(bitmap).
//                setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER()).
//                setNewLinesAfter(2).
//                build();

        printables.add(printableTitle);
        printables.add(printableSubTitle);
        printables.add(printableSubLokasi);
        printables.add(printableLobby);
        printables.add(printableUser);
        printables.add(printableNoPol);
        printables.add(printableJamMasuk);
        printables.add(printableJamKeluar);
        printables.add(printableDurasi);
        printables.add(printableBiaya);
        printables.add(printableenter);
        printables.add(printableenter);
        printables.add(printableFooter1);
        printables.add(printableFooter2);
        printables.add(printableFooter3);
        printables.add(printableFooter4);
        printables.add(printableFooter5);
        printables.add(printableenter);
        printables.add(printableenter);
        printables.add(printableenter);

        Printooth.INSTANCE.printer().print(printables);

//        MethodUtil.showCustomToast(getActivity(), "Transactions Success", null);
        Toast.makeText(getActivity(), "Transactions Success",
                Toast.LENGTH_LONG).show();
    }

    public void resetSelectedForm(){
        PreferenceManager.setCaptureNoPol(null);
        detail_kendaraan.setVisibility(View.GONE);
        pembayaran.setVisibility(View.GONE);
        edit_no_ticket.setText("");
    }
}
