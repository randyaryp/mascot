package com.rtech.rparkout.modul.login;

import android.util.Log;

import com.rtech.rparkout.component.network.NetworkService;
import com.rtech.rparkout.component.network.ResponeError;
import com.rtech.rparkout.component.network.response.AuthRequest;
import com.rtech.rparkout.component.network.response.AuthResponse;
import com.rtech.rparkout.component.network.response.ParkingRateRequest;
import com.rtech.rparkout.component.network.response.ParkingRateResponse;
import com.rtech.rparkout.modul.CommonInterface;

import rx.Subscriber;

/**
 * Created by Dhimas on 12/22/17.
 */

public class LoginPresenterImpl implements LoginPresenter {
    private LoginView mView;
    private CommonInterface cInterface;
    private NetworkService mService;
    private LoginInteractor mInteractor;

    public LoginPresenterImpl(CommonInterface commonInterface, LoginView view) {
        mView = view;
        cInterface = commonInterface;
        mService = cInterface.getService();
        mInteractor = new LoginInteractorImpl(mService);
    }


    @Override
    public void parkingRate(ParkingRateRequest dev_id) {
        cInterface.showProgressLoading();
        mInteractor.parkingRate(dev_id).subscribe(new Subscriber<ParkingRateResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.i("kunam", e.getMessage());
                cInterface.hideProgresLoading();
                cInterface.onFailureRequest(ResponeError.getErrorMessage(e));
            }

            @Override
            public void onNext(ParkingRateResponse gCreditCard) {
                cInterface.hideProgresLoading();
                mView.onSuccessParkingRate(gCreditCard);
            }
        });
    }

    @Override
    public void login(AuthRequest log_in) {
        cInterface.showProgressLoading();
        mInteractor.login(log_in).subscribe(new Subscriber<AuthResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.i("kunam", e.getMessage());
                cInterface.hideProgresLoading();
                cInterface.onFailureRequest(ResponeError.getErrorMessage(e));
            }

            @Override
            public void onNext(AuthResponse gCreditCard) {
                cInterface.hideProgresLoading();
                mView.onSuccessLogin(gCreditCard);
            }
        });
    }
}
