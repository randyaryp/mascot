package com.rtech.rparkout.modul.setting;

import com.rtech.rparkout.component.network.response.CekDeviceResponse;
import com.rtech.rparkout.component.network.response.LobbyListResponse;
import com.rtech.rparkout.component.network.response.PaymentTypeResponse;
import com.rtech.rparkout.component.network.response.TicketNoteResponse;
import com.rtech.rparkout.component.network.response.VehicleColorResponse;
import com.rtech.rparkout.component.network.response.VehicleListResponse;
import com.rtech.rparkout.component.network.response.VehicleTypeResponse;

/**
 * Created by Dhimas on 12/22/17.
 */

public interface SettingView {
    void onSuccessCekDevice(CekDeviceResponse dev_response);
    void onSuccessLobbyList(LobbyListResponse dev_response);
    void onSuccessVehicleList(VehicleListResponse dev_response);
    void onSuccessTicketNote(TicketNoteResponse dev_response);
    void onSuccessVehicleType(VehicleTypeResponse dev_response);
    void onSuccessVehicleColor(VehicleColorResponse dev_response);
    void onSuccessPaymentType(PaymentTypeResponse dev_response);
}
