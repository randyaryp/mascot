package com.rtech.rparkout.modul.splash;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
//import android.support.annotation.Nullable;
//import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.ImageView;

//import com.google.firebase.messaging.FirebaseMessaging;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

//import com.rtech.onstreet.R;
import com.rtech.mascot.R;
import com.rtech.rparkout.MainActivity;
import com.rtech.rparkout.component.dialog.CustomProgressBar;
//import NetworkManager;
//import NetworkService;
//import GAgent;
import com.rtech.rparkout.component.network.NetworkManager;
import com.rtech.rparkout.component.network.NetworkService;
import com.rtech.rparkout.component.util.PreferenceManager;
import com.rtech.rparkout.modul.CommonInterface;
import com.rtech.rparkout.modul.login.LoginActivity;
import com.rtech.rparkout.modul.menu.MenuActivity;
import com.rtech.rparkout.modul.transaction.TransactionActivity;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

//import com.google.firebase.iid.FirebaseInstanceId;
//import com.google.firebase.iid.InstanceIdResult;

/**
 * Created by Dhimas on 11/23/17.
 */

public class SplashActivity extends AppCompatActivity implements CommonInterface{
    private String currentVersion;
    private ImageView splashOne;
    private static CustomProgressBar progressBar = new CustomProgressBar();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_layout);
        splashOne = findViewById(R.id.img_splash_1);

        String uniqueID = UUID.randomUUID().toString();
        uniqueID = uniqueID.substring(uniqueID.length()-6).toUpperCase();
//        String uniqueID = "F9B2D3";
        if (TextUtils.isEmpty(PreferenceManager.getImei())) {
            PreferenceManager.setImei(uniqueID);
        }

        try {
            currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        downTimer();
    }

    @Override
    public void showProgressLoading() {
        progressBar.show(this, "", false, null);
    }

    @Override
    public void hideProgresLoading() {
        progressBar.getDialog().dismiss();
    }

    @Override
    public NetworkService getService() {
        return NetworkManager.getInstance();
    }

    @Override
    public void onFailureRequest(String msg) {
//        MethodUtil.showCustomToast(this, msg, R.drawable.ic_error_login);
    }

    private void downTimer() {
        long futureMillis = TimeUnit.SECONDS.toMillis(2);
        new CountDownTimer(futureMillis, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000) % 60;
                if (seconds == 0) {
                    cancel();
                    onFinish();
                }
            }

            @Override
            public void onFinish() {
                if (PreferenceManager.isLogin()) {
                    gotoMainPage();
                } else {
                    gotoLoginPage();
                }
            }
        }.start();
    }
    private void gotoLoginPage() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
    private void gotoTransactionsPage() {
        Intent intent = new Intent(this, TransactionActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void gotoMainPage() {
        Intent intent = new Intent(this, MenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}
