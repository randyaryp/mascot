package com.rtech.rparkout.modul.setting;

import com.rtech.rparkout.component.network.NetworkService;
import com.rtech.rparkout.component.network.request.TicketNoteRequest;
import com.rtech.rparkout.component.network.response.CekDeviceRequest;
import com.rtech.rparkout.component.network.response.CekDeviceResponse;
import com.rtech.rparkout.component.network.response.LobbyListRequest;
import com.rtech.rparkout.component.network.response.LobbyListResponse;
import com.rtech.rparkout.component.network.response.PaymentTypeResponse;
import com.rtech.rparkout.component.network.response.TicketNoteResponse;
import com.rtech.rparkout.component.network.response.VehicleColorResponse;
import com.rtech.rparkout.component.network.response.VehicleListRequest;
import com.rtech.rparkout.component.network.response.VehicleListResponse;
import com.rtech.rparkout.component.network.response.VehicleTypeResponse;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Dhimas on 1/3/18.
 */

public class SettingInteractorImpl implements SettingInteractor {
    private NetworkService mService;

    public SettingInteractorImpl(NetworkService service) {
        mService = service;
    }

    @Override
    public Observable<CekDeviceResponse> cekDevice(CekDeviceRequest device_id) {
        return mService.verifyDevice(device_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io());
    }

    @Override
    public Observable<LobbyListResponse> lobbyList(LobbyListRequest device_id) {
        return mService.lobbyList(device_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io());
    }

    @Override
    public Observable<VehicleListResponse> vehicleList(VehicleListRequest device_id) {
        return mService.vehicleList(device_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io());
    }

    @Override
    public Observable<TicketNoteResponse> ticketNote(TicketNoteRequest device_id) {
        return mService.getTicketNote(device_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io());
    }

    @Override
    public Observable<VehicleTypeResponse> vehicleType() {
        return mService.getVehicleType()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io());
    }

    @Override
    public Observable<VehicleColorResponse> vehicleColor() {
        return mService.getVehicleColor()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io());
    }

    @Override
    public Observable<PaymentTypeResponse> paymentType() {
        return mService.getPaymentType()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io());
    }
}
