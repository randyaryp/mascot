package com.rtech.rparkout.modul.setting;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

//import com.google.firebase.analytics.FirebaseAnalytics;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
//import com.rtech.onstreet.R;
import com.rtech.mascot.R;
import com.rtech.rparkout.component.network.NetworkManager;
import com.rtech.rparkout.component.network.NetworkService;
import com.rtech.rparkout.component.network.request.TicketNoteRequest;
import com.rtech.rparkout.component.network.response.CekDeviceRequest;
import com.rtech.rparkout.component.network.response.CekDeviceResponse;
import com.rtech.rparkout.component.network.response.LobbyListRequest;
import com.rtech.rparkout.component.network.response.LobbyListResponse;
import com.rtech.rparkout.component.network.response.PaymentTypeResponse;
import com.rtech.rparkout.component.network.response.TicketNoteResponse;
import com.rtech.rparkout.component.network.response.VehicleColorResponse;
import com.rtech.rparkout.component.network.response.VehicleListRequest;
import com.rtech.rparkout.component.network.response.VehicleListResponse;
import com.rtech.rparkout.component.network.response.VehicleTypeResponse;
import com.rtech.rparkout.component.util.MethodUtil;
import com.rtech.rparkout.component.util.PreferenceManager;
import com.rtech.rparkout.modul.CommonInterface;
import com.rtech.rparkout.modul.login.LoginActivity;

/**
 * Created by Dhimas on 10/5/17.
 */

public class SettingActivity extends AppCompatActivity implements CommonInterface, SettingView {
    private TextView registerText;
    private EditText mobile;
    private Button loginBtn;
    private ImageView qrss;
//    private FirebaseAnalytics mFirebaseAnalytics;
    private TextView dev_id;

    private EditText location;

    public SettingPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_layout);
        initComponent();
        initEvent();
    }

    private void initComponent() {
        loginBtn = findViewById(R.id.cnt_btn);
        dev_id = findViewById(R.id.dev_id);
        dev_id.setText(PreferenceManager.getImei());
        qrss = findViewById(R.id.qr_asu);
        location = findViewById(R.id.input_location);

        mPresenter = new SettingPresenterImpl(this, this);

    }

    private void initEvent() {
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onBackPressed();

                if(location.getText() == null || location.getText().toString().equalsIgnoreCase("")){
                    MethodUtil.showCustomToast(SettingActivity.this, "Harap Masukan Kode Lokasi", R.drawable.ic_warning_red_24dp);
                }else{
                    CekDeviceRequest cek = new CekDeviceRequest(PreferenceManager.getImei(), location.getText().toString());
//                    LobbyListRequest lobbyList = new LobbyListRequest(location.getText().toString());
                    mPresenter.cekDevice(cek);
//                    mPresenter.lobbyList(lobbyList);
                }
            }
        });

        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode(PreferenceManager.getImei(), BarcodeFormat.QR_CODE, 512, 512);
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
            qrss.setImageBitmap(bmp);

        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void showProgressLoading() {
        progressBar.show(this, "", false, null);
    }

    @Override
    public void hideProgresLoading() {
        progressBar.getDialog().dismiss();
    }

    @Override
    public NetworkService getService() {
        return NetworkManager.getInstance();
    }

    @Override
    public void onFailureRequest(String msg) {
        MethodUtil.showCustomToast(this, msg, R.drawable.ic_warning_red_24dp);
    }

    @Override
    public void onSuccessCekDevice(CekDeviceResponse dev_response) {
//        Log.i("kunam cuk", String.valueOf(dev_response.code));
//        Log.i("kunam", String.valueOf(dev_response.message.size()));
        if(dev_response.success){
//            Intent intent = new Intent(this, LoginActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PreferenceManager.setSite(location.getText().toString(), dev_response.data.site_name);
            LobbyListRequest lobbyList = new LobbyListRequest(location.getText().toString());
            mPresenter.lobbyList(lobbyList);
//            intent.putExtra("site_id", location.getText().toString());
//            startActivity(intent);
//            finish();
        }else{
            MethodUtil.showCustomToast(this, "Device ID Belum Terdaftar", R.drawable.ic_warning_red_24dp);
        }
    }

    @Override
    public void onSuccessLobbyList(LobbyListResponse dev_response) {
        if(dev_response.data.size() > 0){
            PreferenceManager.setLobbyList(dev_response.data);
//            VehicleListRequest vehicleList = new VehicleListRequest(location.getText().toString());
//            mPresenter.vehicleList(vehicleList);
            TicketNoteRequest ticketNote = new TicketNoteRequest(location.getText().toString());
            mPresenter.ticketNote(ticketNote);
        }else{
            MethodUtil.showCustomToast(this, "Kode Lokasi Belum Terdaftar", R.drawable.ic_warning_red_24dp);
        }
    }

    @Override
    public void onSuccessVehicleList(VehicleListResponse dev_response) {
        if(dev_response.data.size() > 0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            PreferenceManager.setSite(location.getText().toString());
            PreferenceManager.setVehicleList(dev_response.data);
            startActivity(intent);
            finish();
        }else{
            MethodUtil.showCustomToast(this, "Kode Lokasi Belum Terdaftar", R.drawable.ic_warning_red_24dp);
        }
    }

    @Override
    public void onSuccessTicketNote(TicketNoteResponse dev_response) {
        if(dev_response.success){
            PreferenceManager.setTicketnote(dev_response);
//            VehicleListRequest vehicleList = new VehicleListRequest(location.getText().toString());
//            mPresenter.vehicleList(vehicleList);
            mPresenter.vehicleType();
        }else{
            MethodUtil.showCustomToast(this, "Kode Lokasi Belum Terdaftar", R.drawable.ic_warning_red_24dp);
        }
    }

    @Override
    public void onSuccessVehicleType(VehicleTypeResponse dev_response) {
        if(dev_response.success){
            PreferenceManager.setVehicletype(dev_response);
//            VehicleListRequest vehicleList = new VehicleListRequest(location.getText().toString());
//            mPresenter.vehicleList(vehicleList);
            mPresenter.paymentType();
        }else{
            MethodUtil.showCustomToast(this, "Kode Lokasi Belum Terdaftar", R.drawable.ic_warning_red_24dp);
        }
    }

    @Override
    public void onSuccessVehicleColor(VehicleColorResponse dev_response) {
        if(dev_response.success){
            PreferenceManager.setVehiclecolor(dev_response);
            VehicleListRequest vehicleList = new VehicleListRequest(location.getText().toString());
            mPresenter.vehicleList(vehicleList);
        }else{
            MethodUtil.showCustomToast(this, "Kode Lokasi Belum Terdaftar", R.drawable.ic_warning_red_24dp);
        }
    }

    @Override
    public void onSuccessPaymentType(PaymentTypeResponse dev_response) {
        if (dev_response.success){
            PreferenceManager.setPaymenttype(dev_response);
            mPresenter.vehicleColor();
        }else{
            MethodUtil.showCustomToast(this, "Kode Lokasi Belum Terdaftar", R.drawable.ic_warning_red_24dp);
        }
    }
}
