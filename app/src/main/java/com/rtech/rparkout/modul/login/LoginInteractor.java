package com.rtech.rparkout.modul.login;

import com.rtech.rparkout.component.network.response.AuthRequest;
import com.rtech.rparkout.component.network.response.AuthResponse;
import com.rtech.rparkout.component.network.response.ParkingRateRequest;
import com.rtech.rparkout.component.network.response.ParkingRateResponse;

import rx.Observable;

/**
 * Created by Dhimas on 1/3/18.
 */

public interface LoginInteractor {
    Observable<ParkingRateResponse> parkingRate(ParkingRateRequest device_id);

    Observable<AuthResponse> login(AuthRequest log_in);
}
