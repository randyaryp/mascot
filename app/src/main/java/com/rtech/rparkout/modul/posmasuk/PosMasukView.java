package com.rtech.rparkout.modul.posmasuk;

import com.rtech.rparkout.component.network.response.SaveDataPosMasukResponse;
import com.rtech.rparkout.component.network.response.UploadImageResponse;

public interface PosMasukView {
    void onSuccessUploadImage(UploadImageResponse dev_response);
    void onSuccessSaveDataPosMasuk(SaveDataPosMasukResponse dev_response);
}
