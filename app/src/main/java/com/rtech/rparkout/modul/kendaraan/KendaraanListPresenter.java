package com.rtech.rparkout.modul.kendaraan;

import com.rtech.rparkout.component.network.request.EditKendaraanRequest;
import com.rtech.rparkout.component.network.request.KendaraanListRequest;
import com.rtech.rparkout.component.network.request.SearchKendaraanRequest;
import com.rtech.rparkout.component.network.response.UploadImageRequest;

public interface KendaraanListPresenter {
    void kendaraanList(KendaraanListRequest dev_id);
    void searchKendaraanList(SearchKendaraanRequest dev_id);
    void EditKendaraan(EditKendaraanRequest dev_id);
    void uploadImage(UploadImageRequest dev_id);
}
