package com.rtech.rparkout.modul.kendaraan;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rtech.mascot.R;
import com.rtech.rparkout.MainActivity;
import com.rtech.rparkout.component.adapter.VehicleColorAdapter;
import com.rtech.rparkout.component.adapter.VehicleTypeAdapter;
import com.rtech.rparkout.component.network.NetworkManager;
import com.rtech.rparkout.component.network.NetworkService;
import com.rtech.rparkout.component.network.gson.GKendaraanDetail;
import com.rtech.rparkout.component.network.gson.GLobby;
import com.rtech.rparkout.component.network.gson.GVehicle;
import com.rtech.rparkout.component.network.gson.GVehicleColor;
import com.rtech.rparkout.component.network.gson.GVehicleType;
import com.rtech.rparkout.component.network.request.EditKendaraanRequest;
import com.rtech.rparkout.component.network.response.EditKendaraanResponse;
import com.rtech.rparkout.component.network.response.KendaraanListResponse;
import com.rtech.rparkout.component.network.response.UploadImageRequest;
import com.rtech.rparkout.component.network.response.UploadImageResponse;
import com.rtech.rparkout.component.network.response.VehicleColorResponse;
import com.rtech.rparkout.component.network.response.VehicleTypeResponse;
import com.rtech.rparkout.component.util.MethodUtil;
import com.rtech.rparkout.component.util.PreferenceManager;
import com.rtech.rparkout.modul.CommonInterface;
import com.rtech.rparkout.modul.navigation.MyRecyclerViewAdapter;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class EditKendaraan extends AppCompatActivity implements CommonInterface, KendaraanListView, AdapterView.OnItemSelectedListener {
    private Dialog dialog;

    private GKendaraanDetail detail;
    private TextView user, location, lobby, noTicket;
    private Button buttonEdit, buttonCancel;
    private ImageView BackButton;
    private RadioGroup groupRadio;
    private EditText editNoPol;
    List<GVehicleType> VehicleTypeList;
    List<GVehicleColor> VehicleColorList;
    public KendaraanListPresenter mPresenter;
    private RecyclerView scrollImageFront, scrollImageBack, scrollImageRight, scrollImageLeft;
    private LinearLayout captureNopolMasuk, captureDepan, captureKanan, captureKiri, captureBelakang;
    private Switch CarCall;
    private String photoImageBase64, noPOl, QRCod, imageID, capturePosition;
    Bitmap photoImage, bitmap;
    ArrayList<Bitmap> frontPhotoBase64 = new ArrayList<>();
    ArrayList<Bitmap> rightPhotoBase64 = new ArrayList<>();
    ArrayList<Bitmap> leftPhotoBase64 = new ArrayList<>();
    ArrayList<Bitmap> backPhotoBase64 = new ArrayList<>();

    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private static final int CAMERA_REQUEST = 1888;

    TextView spinnerKendaraan, spinnerWarna;
    ArrayList<String> arrayListKendaraan, arrayListWarna;
    Dialog dialogKendaraan, dialogWarna;
    MyRecyclerViewAdapter adapter, adapterFront, adapterRight, adapterLeft, adapterBack;

    @Override
    public void onResume() {
        ArrayList<String> animalNames = new ArrayList<>();
        animalNames.add("Horse");
        // set up the RecyclerView
        MyRecyclerViewAdapter.ItemClickListener listener = new MyRecyclerViewAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Toast.makeText(getApplicationContext(), "Item clicked: " + position, Toast.LENGTH_SHORT).show();
            }
        };
        LinearLayoutManager layoutManagerFront
                = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager layoutManagerBack
                = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager layoutManagerRight
                = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager layoutManagerLeft
                = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        scrollImageFront.setLayoutManager(layoutManagerFront);
        scrollImageBack.setLayoutManager(layoutManagerBack);
        scrollImageRight.setLayoutManager(layoutManagerRight);
        scrollImageLeft.setLayoutManager(layoutManagerLeft);

        adapter = new MyRecyclerViewAdapter(getApplicationContext(), animalNames, "a");
        adapter.setClickListener(listener);

        switch (capturePosition) {
            case "front":
                if (photoImage != null) {
                    frontPhotoBase64.add(photoImage);
                }
                break;
            case "right":
                if (photoImage != null) {
                    rightPhotoBase64.add(photoImage);
                }
                break;
            case "left":
                if (photoImage != null) {
                    leftPhotoBase64.add(photoImage);
                }
                break;
            case "back":
                if (photoImage != null) {
                    backPhotoBase64.add(photoImage);
                }
                break;
            default:
//                break;
        }
//        FRONT
        if (frontPhotoBase64 == null || frontPhotoBase64.size() == 0) {
            scrollImageFront.setAdapter(adapter);
        } else {
            adapterFront = new MyRecyclerViewAdapter(getApplicationContext(), frontPhotoBase64);
            adapterFront.setClickListener(listener);
            scrollImageFront.setAdapter(adapterFront);
        }
//        RIGHT
        if (rightPhotoBase64 == null || rightPhotoBase64.size() == 0) {
            scrollImageRight.setAdapter(adapter);
        } else {
            adapterRight = new MyRecyclerViewAdapter(getApplicationContext(), rightPhotoBase64);
            adapterRight.setClickListener(listener);
            scrollImageRight.setAdapter(adapterRight);
        }
//        LEFT
        if (leftPhotoBase64 == null || leftPhotoBase64.size() == 0) {
            scrollImageLeft.setAdapter(adapter);
        } else {
            adapterLeft = new MyRecyclerViewAdapter(getApplicationContext(), leftPhotoBase64);
            adapterLeft.setClickListener(listener);
            scrollImageLeft.setAdapter(adapterLeft);
        }
//        BACK
        if (backPhotoBase64 == null || backPhotoBase64.size() == 0) {
            scrollImageBack.setAdapter(adapter);
        } else {
            adapterBack = new MyRecyclerViewAdapter(getApplicationContext(), backPhotoBase64);
            adapterBack.setClickListener(listener);
            scrollImageBack.setAdapter(adapterBack);
        }
        super.onResume();
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        imageID = "";
        capturePosition = "";

        setContentView(R.layout.edit_kendaraan_layout);
        String position = getIntent().getStringExtra("positions");
        int pos = Integer.valueOf(position);
        KendaraanListResponse res = PreferenceManager.getListKendaraanInside();
        detail = res.data.get(pos);
        initComponent();
        initValue();
        initEvent();
    }

    private void initComponent() {
        mPresenter = new KendaraanListPresenterImpl(this, this);

        scrollImageFront = findViewById(R.id.scrollImageFront);
        scrollImageBack = findViewById(R.id.scrollImageBack);
        scrollImageRight = findViewById(R.id.scrollImageRight);
        scrollImageLeft = findViewById(R.id.scrollImageLeft);

        captureDepan = findViewById(R.id.captureDepan);
        captureBelakang = findViewById(R.id.captureBelakang);
        captureKanan = findViewById(R.id.captureKanan);
        captureKiri = findViewById(R.id.captureKiri);

        user = findViewById(R.id.user);
        location = findViewById(R.id.location);
        lobby = findViewById(R.id.lobby);

        BackButton = findViewById(R.id.back_button);
        noTicket = findViewById(R.id.noTicket);
        buttonEdit = findViewById(R.id.buttonEditKendaraan);
        buttonCancel = findViewById(R.id.buttonReset);

        groupRadio = findViewById(R.id.radioGroup1);
        RadioButton button;
        List<GVehicle> listVehicle = PreferenceManager.getVehicleList();
        Log.i("kunam", detail.car_type);
        for (int i = 0; i < listVehicle.size(); i++) {
            button = new RadioButton(getApplicationContext());
            button.setText(listVehicle.get(i).vehicle_name);
            if (listVehicle.get(i).vehicle_name.equalsIgnoreCase(detail.ti_package)){
                button.setChecked(true);
            }
            button.setId(i);
            groupRadio.addView(button);
        }

        editNoPol = findViewById(R.id.edit_nopol_masuk);

//        dropdownVehicleType = findViewById(R.id.spinner1);
//        dropdownVehicleType.setOnItemSelectedListener(this);
        VehicleTypeResponse vecType = PreferenceManager.getVehicleType();
//        VehicleTypeList = vecType.data;
//        VehicleTypeAdapter customAdapter=new VehicleTypeAdapter(getApplicationContext(), VehicleTypeList);
//        dropdownVehicleType.setAdapter(customAdapter);
//        for (int i=0; i < vecType.data.size(); i++){
//            if (vecType.data.get(i).vehicletype.equalsIgnoreCase(detail.car_type)){
//                dropdownVehicleType.setSelection(i);
//            }
//        }

        spinnerKendaraan=findViewById(R.id.spinnerKendaraan);
        spinnerKendaraan.setText(detail.car_type);

        // initialize array list
        arrayListKendaraan=new ArrayList<>();
        // set value in array list
        for (int i = 0; i<vecType.data.size(); i++){
            arrayListKendaraan.add(vecType.data.get(i).vehicletype);
        }

        spinnerKendaraan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Initialize dialog
                dialogKendaraan=new Dialog(EditKendaraan.this);

                // set custom dialog
                dialogKendaraan.setContentView(R.layout.dialog_searchable_spinner);

                // set custom height and width
                dialogKendaraan.getWindow().setLayout(650,800);

                // set transparent background
                dialogKendaraan.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                // show dialog
                dialogKendaraan.show();

                // Initialize and assign variable
                EditText editText=dialogKendaraan.findViewById(R.id.edit_text);
                ListView listView=dialogKendaraan.findViewById(R.id.list_view);

                // Initialize array adapter
                ArrayAdapter<String> adapter=new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1,arrayListKendaraan);

                // set adapter
                listView.setAdapter(adapter);
                editText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        adapter.getFilter().filter(s);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        // when item selected from list
                        // set selected item on textView
                        spinnerKendaraan.setText(adapter.getItem(position));

                        // Dismiss dialog
                        dialogKendaraan.dismiss();
                    }
                });
            }
        });

//        dropdownVehicleColor = findViewById(R.id.spinner2);
//        dropdownVehicleColor.setOnItemSelectedListener(this);
        VehicleColorResponse vecColor = PreferenceManager.getVehicleColor();
//        VehicleColorList = vecColor.data;
//        VehicleColorAdapter vecColorAdapter=new VehicleColorAdapter(getApplicationContext(), VehicleColorList);
//        dropdownVehicleColor.setAdapter(vecColorAdapter);
//        for (int i = 0; i < vecColor.data.size(); i++){
//            if (vecColor.data.get(i).color.equalsIgnoreCase(detail.car_color)){
//                dropdownVehicleColor.setSelection(i);
//            }
//        }

        spinnerWarna=findViewById(R.id.spinnerWarna);
        spinnerWarna.setText(detail.car_color);

        // initialize array list
        arrayListWarna=new ArrayList<>();
        // set value in array list
        for (int i = 0; i<vecColor.data.size(); i++){
            arrayListWarna.add(vecColor.data.get(i).color);
        }

        spinnerWarna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Initialize dialog
                dialogWarna=new Dialog(EditKendaraan.this);

                // set custom dialog
                dialogWarna.setContentView(R.layout.dialog_searchable_spinner);

                // set custom height and width
                dialogWarna.getWindow().setLayout(650,800);

                // set transparent background
                dialogWarna.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                // show dialog
                dialogWarna.show();

                // Initialize and assign variable
                EditText editText=dialogWarna.findViewById(R.id.edit_text);
                ListView listView=dialogWarna.findViewById(R.id.list_view);

                // Initialize array adapter
                ArrayAdapter<String> adapter=new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1,arrayListWarna);

                // set adapter
                listView.setAdapter(adapter);
                editText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        adapter.getFilter().filter(s);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        // when item selected from list
                        // set selected item on textView
                        spinnerWarna.setText(adapter.getItem(position));

                        // Dismiss dialog
                        dialogWarna.dismiss();
                    }
                });
            }
        });

        CarCall = findViewById(R.id.switchOnOff);
    }

    private void initValue(){
        noTicket.setText(detail.ti_qrcode);
        editNoPol.setText(detail.ti_policeno);

        user.setText(PreferenceManager.getUser());
        location.setText(PreferenceManager.getSiteName());
        String lobbyName = "";
        List<GLobby> lobbyList = PreferenceManager.getLobbyList();
        for (int i=0; i< lobbyList.size(); i++){
            if (lobbyList.get(i).id.equalsIgnoreCase(PreferenceManager.getLoggedInLobbyId())){
                lobbyName = lobbyList.get(i).lobby_name;
            }
        }
        lobby.setText(lobbyName);
        if (detail.car_call.equalsIgnoreCase("Y")){
            CarCall.setChecked(true);
        }else{
            CarCall.setChecked(false);
        }
    }

    private void initEvent() {
        BackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selectedId = groupRadio.getCheckedRadioButtonId();
                List<GVehicle> listVehicle = PreferenceManager.getVehicleList();
                String vehicle = "";
                if (selectedId != -1){
                    vehicle = listVehicle.get(selectedId).vehicle_name;
                }

                String car_call = "N";
                if (CarCall.isChecked()) {
                    car_call= "Y";
                }

                EditKendaraanRequest editKendaraanRequest = new EditKendaraanRequest(
                        PreferenceManager.getSite(),
                        PreferenceManager.getLoggedInLobbyId(),
                        detail.ti_qrcode,
                        editNoPol.getText().toString(),
                        vehicle,
                        spinnerWarna.getText().toString(),
                        car_call,
                        spinnerKendaraan.getText().toString()
                );
                mPresenter.EditKendaraan(editKendaraanRequest);
            }
        });
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        captureDepan.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if (frontPhotoBase64 != null && frontPhotoBase64.size() > 4) {
                    Toast.makeText(getApplicationContext(), "Foto depan sudah maksimum", Toast.LENGTH_SHORT).show();
                } else {
                    if (getApplicationContext().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                    } else {
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        capturePosition = "front";
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }
                }
            }
        });
        captureKanan.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if (rightPhotoBase64 != null && rightPhotoBase64.size() > 4) {
                    Toast.makeText(getApplicationContext(), "Foto kanan sudah maksimum", Toast.LENGTH_SHORT).show();
                } else {
                    if (getApplicationContext().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                    } else {
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        capturePosition = "right";
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }
                }
            }
        });
        captureKiri.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if (leftPhotoBase64 != null && leftPhotoBase64.size() > 4) {
                    Toast.makeText(getApplicationContext(), "Foto kiri sudah maksimum", Toast.LENGTH_SHORT).show();
                } else {
                    if (getApplicationContext().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                    } else {
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        capturePosition = "left";
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }
                }
            }
        });
        captureBelakang.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if (backPhotoBase64 != null && backPhotoBase64.size() > 4) {
                    Toast.makeText(getApplicationContext(), "Foto belakang sudah maksimum", Toast.LENGTH_SHORT).show();
                } else {
                    if (getApplicationContext().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                    } else {
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        capturePosition = "back";
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }
                }
            }
        });
    }

    @Override
    public void showProgressLoading() {
//        progressBar.show(getApplicationContext(), "", false, null);
//        LayoutInflater inflator = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        final View view = inflator.inflate(R.layout.progress_bar, null);
//        if (dialog != null && dialog.isShowing()) {
//            dialog.dismiss();
//        }
//        dialog = new Dialog(getApplicationContext(), R.style.CustomDialog);
//        dialog.setContentView(view);
//        dialog.setCancelable(false);
//        dialog.setOnCancelListener(null);
//        dialog.show();
    }

    @Override
    public void hideProgresLoading() {
//        dialog.dismiss();
    }

    @Override
    public NetworkService getService() {
        return NetworkManager.getInstance();
    }

    @Override
    public void onFailureRequest(String msg) {
        MethodUtil.showCustomToast(this, msg, R.drawable.ic_warning_red_24dp);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onSuccessGetKendaraanList(KendaraanListResponse dev_response) {

    }

    @Override
    public void onSuccessEditKendaraan(EditKendaraanResponse dev_response) {
        if (dev_response.success){
            Toast.makeText(this, "Success edit", Toast.LENGTH_SHORT).show();
            PreferenceManager.setDefaultFragment("3");
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }else{
            MethodUtil.showCustomToast(this, "Failed edit Kendaraan", R.drawable.ic_warning_red_24dp);
        }
    }

    @Override
    public void onSuccessUploadImage(UploadImageResponse dev_response) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            photoImage = (Bitmap) data.getExtras().get("data");
            Log.i("kunam width", String.valueOf(photoImage.getWidth()));
            Log.i("kunam height", String.valueOf(photoImage.getHeight()));
            Bitmap resized = Bitmap.createScaledBitmap(photoImage,(int)(photoImage.getWidth()*2), (int)(photoImage.getHeight()*2), true);
            photoImageBase64 = getBase64String(resized);
            String upl = "data:image/jpeg;base64," + photoImageBase64;

            UploadImageRequest req = new UploadImageRequest(upl);
            mPresenter.uploadImage(req);
        }
    }

    private String getBase64String(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        byte[] imageBytes = baos.toByteArray();

        String base64String = Base64.encodeToString(imageBytes, Base64.NO_WRAP);

        return base64String;
    }
}
