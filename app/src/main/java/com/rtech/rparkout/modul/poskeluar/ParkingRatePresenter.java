package com.rtech.rparkout.modul.poskeluar;

import com.rtech.rparkout.component.network.response.ParkingRateRequest;
import com.rtech.rparkout.component.network.response.SaveDataPosKeluarRequest;

public interface ParkingRatePresenter {
    void parkingRate(ParkingRateRequest dev_id);
    void saveDataPosKeluar(SaveDataPosKeluarRequest dev_id);
}
