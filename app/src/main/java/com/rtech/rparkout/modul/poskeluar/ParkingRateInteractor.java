package com.rtech.rparkout.modul.poskeluar;

import com.rtech.rparkout.component.network.response.ParkingRateRequest;
import com.rtech.rparkout.component.network.response.ParkingRateResponse;
import com.rtech.rparkout.component.network.response.SaveDataPosKeluarRequest;
import com.rtech.rparkout.component.network.response.SaveDataPosKeluarResponse;
import com.rtech.rparkout.component.network.response.SaveDataPosMasukRequest;
import com.rtech.rparkout.component.network.response.SaveDataPosMasukResponse;

import rx.Observable;

public interface ParkingRateInteractor {
    Observable<ParkingRateResponse> parkingrate(ParkingRateRequest device_id);
    Observable<SaveDataPosKeluarResponse> saveDataPosKeluar(SaveDataPosKeluarRequest device_id);
}
