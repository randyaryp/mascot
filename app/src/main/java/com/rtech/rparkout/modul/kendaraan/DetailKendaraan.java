package com.rtech.rparkout.modul.kendaraan;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.google.zxing.WriterException;
import com.mazenrashed.printooth.Printooth;
import com.mazenrashed.printooth.data.printable.ImagePrintable;
import com.mazenrashed.printooth.data.printable.Printable;
import com.mazenrashed.printooth.data.printable.TextPrintable;
import com.mazenrashed.printooth.data.printer.DefaultPrinter;
import com.mazenrashed.printooth.ui.ScanningActivity;
import com.mazenrashed.printooth.utilities.PrintingCallback;
import com.rtech.mascot.R;
import com.rtech.rparkout.component.network.NetworkService;
import com.rtech.rparkout.component.network.gson.GKendaraanDetail;
import com.rtech.rparkout.component.network.gson.GLobby;
import com.rtech.rparkout.component.network.gson.GVehicle;
import com.rtech.rparkout.component.network.response.KendaraanListResponse;
import com.rtech.rparkout.component.network.response.SaveDataPosMasukRequest;
import com.rtech.rparkout.component.network.response.TicketNoteResponse;
import com.rtech.rparkout.component.util.PreferenceManager;
import com.rtech.rparkout.modul.CommonInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;

public class DetailKendaraan extends AppCompatActivity implements CommonInterface {
    private static final int MY_CAMERA_PERMISSION_CODE = 100;

    private ImageView BackButton;
    private TextView user, location, lobby, NoTicket, Jenis, NoPolice, JenisKendaraan, WarnaKendaraan, CarCall;
    private Button EditKendaraan, ButtonPrint;
    GKendaraanDetail detail;
    private TicketNoteResponse TicketNote;
    QRGEncoder qrgEncoder;
    Bitmap bitmap;

//    String printerConnection;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_kendaraan_layout);
//        connectionPrinter();
        initComponent();
        initValue();
        initEvent();
    }

    private void connectionPrinter(){
//        printerConnection="";
        ArrayList<Printable> printables = new ArrayList<Printable>();
        TextPrintable printableSubTitle = (TextPrintable) new TextPrintable.Builder()
                .setText("")
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .setNewLinesAfter(1)
                .build();
        printables.add(printableSubTitle);
        try {
            Printooth.INSTANCE.printer().print(printables);

            Printooth.INSTANCE.printer().setPrintingCallback(new PrintingCallback() {
                @Override
                public void connectingWithPrinter() {
                }

                @Override
                public void printingOrderSentSuccessfully() {
                }

                @Override
                public void connectionFailed(String s) {
//                    printerConnection = "FAILED BLOG";
                }

                @Override
                public void onError(String s) {
//                    printerConnection = "FAILED BLOG";
                }

                @Override
                public void onMessage(String s) {
                }

                @Override
                public void disconnected() {
                }
            });


        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(DetailKendaraan.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void initComponent() {
        BackButton = findViewById(R.id.back_button);

        user = findViewById(R.id.user);
        location = findViewById(R.id.location);
        lobby = findViewById(R.id.lobby);

        NoTicket = findViewById(R.id.noTicket);

        Jenis = findViewById(R.id.transaksi);

        NoPolice = findViewById(R.id.no_polisi);
        JenisKendaraan = findViewById(R.id.jenis_kendaraan);
        WarnaKendaraan = findViewById(R.id.warna_kendaraan);

        EditKendaraan = findViewById(R.id.buttonEditKendaraan);
        ButtonPrint = findViewById(R.id.buttonPrint);
        CarCall = findViewById(R.id.car_call);
    }

    private void initValue(){
        String position = getIntent().getStringExtra("positions");
        int pos = Integer.valueOf(position);
        KendaraanListResponse res = PreferenceManager.getListKendaraanInside();
        detail = res.data.get(pos);

        user.setText(PreferenceManager.getUser());
        location.setText(PreferenceManager.getSiteName());
        String lobbyName = "";
        List<GLobby> lobbyList = PreferenceManager.getLobbyList();
        for (int i=0; i< lobbyList.size(); i++){
            if (lobbyList.get(i).id.equalsIgnoreCase(PreferenceManager.getLoggedInLobbyId())){
                lobbyName = lobbyList.get(i).lobby_name;
            }
        }
        lobby.setText(lobbyName);

        NoTicket.setText(detail.ti_qrcode);
        Jenis.setText(detail.ti_package);
        NoPolice.setText(detail.ti_policeno);
        JenisKendaraan.setText(detail.car_type);
        WarnaKendaraan.setText(detail.car_color);

        if (!detail.device_id.equalsIgnoreCase(PreferenceManager.getImei())){
            EditKendaraan.setVisibility(View.GONE);
        }

        TicketNote = PreferenceManager.getTicketNote();
        String car_call = "NO";
        if (detail.car_call.equalsIgnoreCase("Y")){
            car_call = "YES";
        }
        CarCall.setText(car_call);
    }

    private void initEvent() {
        BackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        EditKendaraan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), EditKendaraan.class);
                intent.putExtra("positions", String.valueOf(getIntent().getStringExtra("positions")));
                startActivity(intent);
            }
        });

        ButtonPrint.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                Printooth.INSTANCE.setPrinter(PreferenceManager.getNamaPrinter(), PreferenceManager.getAlamatPrinter());
                if (!Printooth.INSTANCE.hasPairedPrinter()){
                    Toast.makeText(getApplicationContext(), "Mohon hubungkan printer dahulu", Toast.LENGTH_LONG).show();
                }else{
//                    if (printerConnection == ""){
                        cetakStrukMasuk(PreferenceManager.getSite(), detail.ti_qrcode);
//                    }else{
//                        Toast.makeText(DetailKendaraan.this, "PRINTER NOT CONNECTED", Toast.LENGTH_LONG).show();
//                    }
                }
            }
        });
    }

    @Override
    public void showProgressLoading() {

    }

    @Override
    public void hideProgresLoading() {

    }

    @Override
    public NetworkService getService() {
        return null;
    }

    @Override
    public void onFailureRequest(String msg) {

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void cetakStrukMasuk(String site, String noTiket){
        String vehicle = detail.ti_package;

        if (!Printooth.INSTANCE.hasPairedPrinter()){

        }

        if (getApplicationContext().checkSelfPermission(Manifest.permission.BLUETOOTH_SCAN) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.BLUETOOTH_SCAN}, MY_CAMERA_PERMISSION_CODE);
        }
        if (getApplicationContext().checkSelfPermission(Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.BLUETOOTH_CONNECT}, MY_CAMERA_PERMISSION_CODE);
        }

        ArrayList<Printable> printables = new ArrayList<Printable>();
        TextPrintable printableTitle = (TextPrintable) new TextPrintable.Builder()
                .setText("Mascot Valet System")
                .setFontSize((byte) 1)
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .setNewLinesAfter(2)
                .build();
        TextPrintable printableSubTitle = (TextPrintable) new TextPrintable.Builder()
                .setText("Nota Parkir")
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .setNewLinesAfter(3)
                .build();
        TextPrintable printableSubLokasi = (TextPrintable) new TextPrintable.Builder()
                .setText("   Lokasi      :" + PreferenceManager.getSiteName())
                .setNewLinesAfter(1)
                .build();

        String lobbyName = "";
        List<GLobby> lobbyList = PreferenceManager.getLobbyList();
        for (int i = 0; i < lobbyList.size(); i++) {
            if (lobbyList.get(i).id.equalsIgnoreCase(PreferenceManager.getLoggedInLobbyId())) {
                lobbyName = lobbyList.get(i).lobby_name;
            }
        }
        TextPrintable printableLobby = (TextPrintable) new TextPrintable.Builder()
                .setText("   Lobby       :" + lobbyName)
                .setNewLinesAfter(1)
                .build();
        TextPrintable printableUser = (TextPrintable) new TextPrintable.Builder()
                .setText("   Petugas     :" + PreferenceManager.getUser())
                .setNewLinesAfter(1)
                .build();
        TextPrintable printableNoPol = (TextPrintable) new TextPrintable.Builder()
                .setText("   No Polisi   :" + detail.ti_policeno)
                .setNewLinesAfter(1)
                .build();
        TextPrintable printablePaket = (TextPrintable) new TextPrintable.Builder()
                .setText("   Transaksi   :" + vehicle)
                .setNewLinesAfter(1)
                .build();
        TextPrintable printableFooter1 = (TextPrintable) new TextPrintable.Builder()
                .setText(TicketNote.data.in1)
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .setNewLinesAfter(1)
                .build();
        TextPrintable printableFooter2 = (TextPrintable) new TextPrintable.Builder()
                .setText(TicketNote.data.in2)
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .setNewLinesAfter(1)
                .build();
        TextPrintable printableFooter3 = (TextPrintable) new TextPrintable.Builder()
                .setText(TicketNote.data.in3)
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .setNewLinesAfter(1)
                .build();
        TextPrintable printableFooter4 = (TextPrintable) new TextPrintable.Builder()
                .setText(TicketNote.data.in4)
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .setNewLinesAfter(1)
                .build();
        TextPrintable printableFooter5 = (TextPrintable) new TextPrintable.Builder()
                .setText(TicketNote.data.in5)
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .setNewLinesAfter(3)
                .build();
        TextPrintable printableenter = (TextPrintable) new TextPrintable.Builder()
                .setText("\n")
                .setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER())
                .build();

        String urlCarCall = "http://valetdemo.rdev.co.id/public/info/" + site + noTiket;
        qrgEncoder = new QRGEncoder(urlCarCall, null, QRGContents.Type.TEXT, 300);
        try {
            bitmap = qrgEncoder.encodeAsBitmap();
        } catch (WriterException e) {
            Log.e("Tag", e.toString());
        }

        ImagePrintable QRCodes = new ImagePrintable.
                Builder(bitmap).
                setAlignment(DefaultPrinter.Companion.getALIGNMENT_CENTER()).
                setNewLinesAfter(2).
                build();

        printables.add(printableTitle);
        printables.add(printableSubTitle);
        printables.add(printableSubLokasi);
        printables.add(printableLobby);
        printables.add(printableUser);
        printables.add(printableNoPol);
        printables.add(printablePaket);
        printables.add(QRCodes);
        printables.add(printableFooter1);
        printables.add(printableFooter2);
        printables.add(printableFooter3);
        printables.add(printableenter);
        printables.add(printableenter);
        printables.add(printableenter);

        Printooth.INSTANCE.printer().print(printables);
    }
}
