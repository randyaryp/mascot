package com.rtech.rparkout.component.network.response;

import com.rtech.rparkout.component.network.gson.GVehicleType;

import java.util.List;

public class VehicleTypeResponse {
    public boolean success;
    public String message;
    public List<GVehicleType> data;
}
