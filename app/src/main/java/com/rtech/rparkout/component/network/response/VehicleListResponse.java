package com.rtech.rparkout.component.network.response;

import com.rtech.rparkout.component.network.gson.GVehicle;

import java.util.List;

public class VehicleListResponse {
    public boolean success;
    public String message;
    public List<GVehicle> data;
}
