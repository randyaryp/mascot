package com.rtech.rparkout.component.network.request;

public class SearchKendaraanRequest {
    String site_id;
    String lobby_id;
    String search_key;

    public SearchKendaraanRequest(String site_id, String lobby_id, String search_key) {
        this.site_id = site_id;
        this.lobby_id = lobby_id;
        this.search_key = search_key;
    }
}
