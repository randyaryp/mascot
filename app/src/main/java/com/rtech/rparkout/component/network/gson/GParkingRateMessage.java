package com.rtech.rparkout.component.network.gson;

/**
 * Created by Dhimas on 11/22/17.
 */

public class GParkingRateMessage {
    public String site_id;
    public String vehicle_id;
    public String rate1;
    public String rate2;
    public String rate3;
    public String rate4;
    public String rate5;
    public String created_at;
    public String updated_at;
}
