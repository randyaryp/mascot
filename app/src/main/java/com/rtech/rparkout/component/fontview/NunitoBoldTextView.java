package com.rtech.rparkout.component.fontview;

import android.content.Context;
//import android.support.annotation.Nullable;
//import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

/**
 * Created by Dhimas on 9/25/17.
 */

public class NunitoBoldTextView extends AppCompatTextView {

    public NunitoBoldTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            setTypeface(FontHelper.getNunitoBold(context));
        }
    }

    public NunitoBoldTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode()) {
            setTypeface(FontHelper.getNunitoBold(context));
        }
    }

}
