package com.rtech.rparkout.component.network.response;

/**
 * Created by Dhimas on 11/22/17.
 */

public class CekDeviceRequest {
    String device_id;
    String site_id;

    public CekDeviceRequest(String foo, String site_id) {
        this.device_id = foo;
        this.site_id = site_id;
    }
}
