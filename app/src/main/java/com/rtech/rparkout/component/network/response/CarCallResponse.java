package com.rtech.rparkout.component.network.response;

import com.rtech.rparkout.component.network.gson.GCarCall;

import java.util.List;

public class CarCallResponse {
    public boolean success;
    public int total;
    public List<GCarCall> message;
}
