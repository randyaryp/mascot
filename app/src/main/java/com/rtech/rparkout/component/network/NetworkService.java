package com.rtech.rparkout.component.network;

import com.rtech.rparkout.component.network.request.CarCallRequest;
import com.rtech.rparkout.component.network.request.EditKendaraanRequest;
import com.rtech.rparkout.component.network.request.KendaraanListRequest;
import com.rtech.rparkout.component.network.request.SearchKendaraanRequest;
import com.rtech.rparkout.component.network.request.TicketNoteRequest;
import com.rtech.rparkout.component.network.response.AuthRequest;
import com.rtech.rparkout.component.network.response.AuthResponse;
import com.rtech.rparkout.component.network.response.CarCallResponse;
import com.rtech.rparkout.component.network.response.CekDeviceRequest;
import com.rtech.rparkout.component.network.response.CekDeviceResponse;
import com.rtech.rparkout.component.network.response.EditKendaraanResponse;
import com.rtech.rparkout.component.network.response.KendaraanListResponse;
import com.rtech.rparkout.component.network.response.LobbyListRequest;
import com.rtech.rparkout.component.network.response.LobbyListResponse;
import com.rtech.rparkout.component.network.response.ParkingRateRequest;
import com.rtech.rparkout.component.network.response.ParkingRateResponse;
import com.rtech.rparkout.component.network.response.PaymentTypeResponse;
import com.rtech.rparkout.component.network.response.SaveDataPosKeluarRequest;
import com.rtech.rparkout.component.network.response.SaveDataPosKeluarResponse;
import com.rtech.rparkout.component.network.response.SaveDataPosMasukRequest;
import com.rtech.rparkout.component.network.response.SaveDataPosMasukResponse;
import com.rtech.rparkout.component.network.response.SaveTransactionRequest;
import com.rtech.rparkout.component.network.response.SaveTransactionResponse;
import com.rtech.rparkout.component.network.response.TicketNoteResponse;
import com.rtech.rparkout.component.network.response.UploadImageRequest;
import com.rtech.rparkout.component.network.response.UploadImageResponse;
import com.rtech.rparkout.component.network.response.VehicleColorResponse;
import com.rtech.rparkout.component.network.response.VehicleListRequest;
import com.rtech.rparkout.component.network.response.VehicleListResponse;
import com.rtech.rparkout.component.network.response.VehicleTypeResponse;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by Dhimas on 10/6/17.
 */

public interface NetworkService {

    @POST("request_cekdevice")
    Observable<CekDeviceResponse> verifyDevice(@Body CekDeviceRequest body);

    @POST("request_parkingrate")
    Observable<ParkingRateResponse> parkingRate(@Body ParkingRateRequest body);

    @POST("parking/transSave")
    Observable<SaveTransactionResponse> saveTransaction(@Body SaveTransactionRequest body);

    @POST("login")
    Observable<AuthResponse> login(@Body AuthRequest body);

    @POST("lobby_list")
    Observable<LobbyListResponse> lobbyList(@Body LobbyListRequest body);

    @POST("vehicle_list")
    Observable<VehicleListResponse> vehicleList(@Body VehicleListRequest body);

    @POST("upload_img")
    Observable<UploadImageResponse> uploadImage(@Body UploadImageRequest body);

    @POST("save_data_in")
    Observable<SaveDataPosMasukResponse> saveDataPosMasuk(@Body SaveDataPosMasukRequest body);

    @POST("save_transaction")
    Observable<SaveDataPosKeluarResponse> saveDataPosKeluar(@Body SaveDataPosKeluarRequest body);

    @POST("ticketnote")
    Observable<TicketNoteResponse> getTicketNote(@Body TicketNoteRequest body);

    @GET("vehicletype")
    Observable<VehicleTypeResponse> getVehicleType();

    @GET("vehiclecolor")
    Observable<VehicleColorResponse> getVehicleColor();

    @POST("parking_in_list")
    Observable<KendaraanListResponse> parkingInList(@Body KendaraanListRequest body);

    @POST("parking_in_update")
    Observable<EditKendaraanResponse> EditKendaraan(@Body EditKendaraanRequest body);

    @GET("paymenttype")
    Observable<PaymentTypeResponse> getPaymentType();

    @POST("search")
    Observable<KendaraanListResponse> search(@Body SearchKendaraanRequest body);

    @POST("car_call_list")
    Observable<CarCallResponse> carcalllist(@Body CarCallRequest body);
}
