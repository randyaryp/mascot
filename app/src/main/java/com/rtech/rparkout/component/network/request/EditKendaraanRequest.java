package com.rtech.rparkout.component.network.request;

public class EditKendaraanRequest {
    String site_id;
    String lobby_id;
    String ti_qrcode;
    String ti_policeno;
    String ti_package;
    String car_color;
    String car_call;
    String car_type;

    public EditKendaraanRequest(
            String site_id,
            String lobby_id,
            String ti_qrcode,
            String ti_policeno,
            String ti_package,
            String car_color,
            String car_call,
            String car_type) {
        this.site_id = site_id;
        this.lobby_id = lobby_id;
        this.ti_qrcode = ti_qrcode;
        this.ti_policeno = ti_policeno;
        this.ti_package = ti_package;
        this.car_color = car_color;
        this.car_call = car_call;
        this.car_type = car_type;
    }
}
