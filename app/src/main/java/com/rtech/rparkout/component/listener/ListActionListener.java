package com.rtech.rparkout.component.listener;

public interface ListActionListener {
    void itemClicked(int position);
    void itemDeleted(int position);
    void acceptClicked(int position);
    void rejectClicked(int position);
    void searchClicked(int position);
}
