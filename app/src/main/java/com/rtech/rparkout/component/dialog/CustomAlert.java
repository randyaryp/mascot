package com.rtech.rparkout.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.rtech.mascot.R;
import com.rtech.rparkout.MainActivity;

public class CustomAlert {
    private Dialog dialog;

    public Dialog show(Context context, CharSequence title, boolean cancelable,
                       DialogInterface.OnCancelListener cancelListener) {
        LayoutInflater inflator = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflator.inflate(R.layout.dialog, null);
//        if (title != null) {
//            final TextView tv = view.findViewById(R.id.id_title);
//            tv.setText(title);
//        }

        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        dialog = new Dialog(context, R.style.CustomDialog);
        dialog.setContentView(view);
        dialog.setCancelable(cancelable);
        dialog.setOnCancelListener(cancelListener);
        dialog.show();

        Button button = view.findViewById(R.id.buttonProses);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckYes();
            }
        });

        return dialog;
    }

    public Dialog getDialog() {
        return dialog;
    }

    public Boolean CheckYes() {
        return true;
    }

    public void CloseDialog() {
        dialog.dismiss();
    }
}
