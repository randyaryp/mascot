package com.rtech.rparkout.component.network.response;

import com.rtech.rparkout.component.network.gson.GTicketNote;

public class TicketNoteResponse {
    public boolean success;
    public String message;
    public GTicketNote data;
}
