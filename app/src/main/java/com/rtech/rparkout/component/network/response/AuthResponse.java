package com.rtech.rparkout.component.network.response;

import com.rtech.rparkout.component.network.gson.GAuthMessage;

import java.util.List;

/**
 * Created by Dhimas on 11/22/17.
 */

public class AuthResponse {
    public boolean success;
    public String message;
    public GAuthMessage data;
}
