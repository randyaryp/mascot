package com.rtech.rparkout.component.adapter;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rtech.mascot.R;

public class RecyclerViewHolder extends RecyclerView.ViewHolder {

    public TextView view, noPolisi, kendaraan, timeDateIn, dateIn, packagePark;
    public FrameLayout frame_card_kendaraan;
    public RecyclerViewHolder(@NonNull View itemView) {
        super(itemView);
        view = itemView.findViewById(R.id.randomText);
        frame_card_kendaraan = itemView.findViewById(R.id.frame_card_kendaraan);
        noPolisi = itemView.findViewById(R.id.noPolisi);
        kendaraan = itemView.findViewById(R.id.kendaraan);
        timeDateIn = itemView.findViewById(R.id.timeDateIn);
        dateIn = itemView.findViewById(R.id.dateIn);
        packagePark = itemView.findViewById(R.id.packagePark);

    }

    public TextView getView(){
        return view;
    }
}