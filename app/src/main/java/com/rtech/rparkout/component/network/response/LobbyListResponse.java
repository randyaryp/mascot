package com.rtech.rparkout.component.network.response;

import com.rtech.rparkout.component.network.gson.GLobby;
import com.rtech.rparkout.component.network.gson.GParkingRateMessage;

import java.util.List;

public class LobbyListResponse {
    public boolean success;
    public String message;
    public List<GLobby> data;
}
