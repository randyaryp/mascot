package com.rtech.rparkout.component.network.response;

import com.rtech.rparkout.component.network.gson.GKendaraanDetail;

import java.util.List;

public class KendaraanListResponse {
    public boolean success;
    public String message;
    public List<GKendaraanDetail> data;
}
