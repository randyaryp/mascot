package com.rtech.rparkout.component.network.response;

public class SaveDataPosMasukResponse {
    public boolean success;
    public String message;
    public String qrcode;
    public String data;
}
