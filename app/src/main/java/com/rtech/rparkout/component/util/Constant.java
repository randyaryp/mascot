package com.rtech.rparkout.component.util;

/**
 * Created by Dhimas on 11/10/17.
 */

public class Constant {
    public static final int INTERNET_DATA = 0;

    public static final int PULSA_HANDPHONE = 1;

    public static final int PASCABAYAR = 2;

    public static final int PDAM = 3;

    public static final int LISTRIK = 4;

    public static final int BPJS_KESEHATAN = 5;

    public static final int BPJS_KETENAGAKERJAAN = 6;

    public static final int TV_INTERNET = 7;

    public static final int CICILAN = 8;

    public static final int ASURANSI = 9;

    public static final int TRAIN = 10;

    public static final int INTERNET = 11;

    public static final int PLANE = 19;

    public static final int PASCA_BAYAR = 20;

    public static final int GAMES = 69;

    public static final int TARIK_DANA = 13;

    public static final String EXPIRED_SESSION = "User tidak dikenali.";

    public static final String EXPIRED_ACCESS_TOKEN = "AccessToken is already expired.";

    public static final String POSITION = "position";

    public static final String TRANSACTION = "transaction";

    public static final String ERROR_ALREADY_REGISTERED = "Mobile phone number is already registered";

    public static final String TOPUP_STATUS_WAITING = "3";

    public static final String TOPUP_STATUS_ON_PROCESS = "4";

    public static final String TOPUP_STATUS_SUCCESS = "5";

    public static final String TOPUP_STATUS_REJECT = "6";

    public static final String TOPUP_STATUS_REFUND = "7";

    public static final String GAME_TOP_UP = "2";
    public static final String GAME_VOUCHERS = "3";

    public static final String SERVICE_PULSA = "0";
    public static final String SERVICE_PAKET_DATA = "1";
    public static final String SERVICE_PDAM = "2";
    public static final String SERVICE_PLN = "3";
    public static final String SERVICE_TELKOM = "4";
    public static final String SERVICE_MULTIFINANCE = "5";
    public static final String SERVICE_TV = "6";
    public static final String SERVICE_ASURANSI = "7";
    public static final String SERVICE_KARTU_KREDIT = "8";
    public static final String SERVICE_ISP = "9";
    public static final String SERVICE_BPJS = "11";
    public static final String SERVICE_TRAIN = "10";
    public static final String SERVICE_PASCABAYAR = "20";
    public static final String SERVICE_PLANE = "19";
    public static final String SERVICE_TOPUP = "12";
    public static final String SERVICE_TRANSFER = "14";
    public static final String SERVICE_REQUEST = "15";
    public static final String SERVICE_INVITE = "16";
    public static final String SERVICE_FEEDS = "17";
    public static final String SERVICE_GROUP = "18";

    public static final String SERVICE_GAMES = "69";
    public static final String SERVICE_TARIK_DANA =  "13";
    public static final String SERVICE_MEMBERSHIP = "21";

    public static final String SERVICE_PURCHASE = "0";
    public static final String SERVICE_JIWASRAYA = "161";
    public static final String SERVICE_PAYMENT = "1";

    public static final int TYPE_REFERRAL = 1;
    public static final int TYPE_TRANSACTION = 2;
    public static final int TYPE_TOPUP = 3;
    public static final int TYPE_REDEEM = 4;

    //TOPUP
    public static final String NOTIFICATION_TOP_UP  = "1";
    public static final String  NOTIFICATION_REQUEST = "31";
    public static final String  NOTIFICATION_TRANSFER = "32";

    //PPOB
    public static final String NOTIFICATION_TRANSACTION_PPOB = "2";
    public static final String NOTIFICATION_TRANSACTION_PPOB_PULSA= "19";
    public static final String NOTIFICATION_TRANSACTION_PPOB_DATA= "29";
    public static final String NOTIFICATION_TRANSACTION_PPOB_PLNPRE= "20";
    public static final String NOTIFICATION_TRANSACTION_PPOB_PLNPOST= "30";
    public static final String NOTIFICATION_TRANSACTION_PPOB_BPJS= "21";
    public static final String NOTIFICATION_TRANSACTION_PPOB_PDAM= "22";
    public static final String NOTIFICATION_TRANSACTION_PPOB_INTERNET= "23";
    public static final String NOTIFICATION_TRANSACTION_PPOB_TV_KABEL= "24";
    public static final String NOTIFICATION_TRANSACTION_PPOB_CICILAN= "25";
    public static final String NOTIFICATION_TRANSACTION_PPOB_ASURANSI = "26";
    public static final String NOTIFICATION_TRANSACTION_PPOB_JIWASRAYA = "27";
    public static final String NOTIFICATION_TRANSACTION_PPOB_GAME = "28";

    //MERCHANT
    public static final String NOTIFICATION_TRANSACTION_MERCHANT = "3";

    //CASHOUT
    public static final String NOTIFICATION_CASHOUT = "4";

    //FEEDS
    public static final String NOTIFICATION_FEEDS = "5";
    public static final String NOTIFICATION_FEEDS_COMMENT = "7";
    public static final String NOTIFICATION_FEEDS_LIKE= "16";
    public static final String NOTIFICATION_FEEDS_MENTION= "17";

    //GROUP
    public static final String NOTIFICATION_GROUP_REJECT= "6";
    public static final String NOTIFICATION_GROUP_REQUEST= "8";
    public static final String NOTIFICATION_GROUP_ACCEPT= "9";
    public static final String NOTIFICATION_GROUP_INVITE= "10";

    //SYSTEM
    public static final String NOTIFICATION_SYSTEM_UPDATE= "11";
    public static final String NOTIFICATION_SYSTEM_PROMO= "12";
    public static final String NOTIFICATION_SYSTEM_REFERRAL= "13";
    public static final String NOTIFICATION_SYSTEM_EVENT= "14";
    public static final String NOTIFICATION_SYSTEM_DOOFUN= "15";
    public static final String NOTIFICATION_SYSTEM_UPGRADE_ACCOUNT= "18";

    //NEW FEATURE
    public static final String newFeature = "1";

    public static final String CHAT_SERVER_URL = "128.199.223.237";
}
