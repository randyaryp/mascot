package com.rtech.rparkout.component.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
//import com.rtech.onstreet.BuildConfig;
import com.rtech.mascot.BuildConfig;
import com.rtech.rparkout.component.util.PreferenceManager;

import java.io.IOException;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import okhttp3.ConnectionSpec;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.Buffer;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Dhimas on 10/6/17.
 */

public class NetworkManager {
    public static NetworkService instance;
    public static Retrofit retrofit;

    private static final int CONNECT_TIME_OUT = 300 * 1000;
    private static final int READ_TIME_OUT = 300 * 1000;

    public static synchronized NetworkService getInstance(){
        instance = null;
        final String sessionToken = PreferenceManager.getSessionToken();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(CONNECT_TIME_OUT, TimeUnit.MILLISECONDS);
        httpClient.readTimeout(READ_TIME_OUT, TimeUnit.MILLISECONDS);
        httpClient.addNetworkInterceptor(interceptor);

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                String input = bodyToString(chain.request().body());

                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("Authorization", "Bearer 1234567890123")
                        .build();

                return chain.proceed(request);
            }
        });

        ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.CLEARTEXT)
                .build();

        httpClient.connectionSpecs(Collections.singletonList(spec));

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();


        retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(httpClient.build())
                .build();



        instance = retrofit.create(NetworkService.class);
        return instance;
    }

    private static String bodyToString(final RequestBody request) {
        try {
            final Buffer buffer = new Buffer();
            if (request != null)
                request.writeTo(buffer);
            else
                return "";
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }
}
