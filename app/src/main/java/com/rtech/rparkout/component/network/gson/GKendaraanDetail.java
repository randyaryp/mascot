package com.rtech.rparkout.component.network.gson;

public class GKendaraanDetail {
    public String ti_qrcode;
    public String ti_policeno;
    public String ti_datetime;
    public String ti_package;
    public String car_call;
    public String car_color;
    public String car_type;
    public String device_id;
}
