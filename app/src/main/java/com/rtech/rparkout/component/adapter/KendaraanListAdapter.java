package com.rtech.rparkout.component.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.rtech.mascot.R;
import com.rtech.rparkout.component.network.gson.GKendaraanDetail;
import com.rtech.rparkout.component.util.PreferenceManager;
import com.rtech.rparkout.modul.kendaraan.DetailKendaraan;
import com.rtech.rparkout.modul.kendaraan.EditKendaraan;

import java.util.List;
import java.util.Random;

public class KendaraanListAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {
    List<GKendaraanDetail> data;
    Context context;

    public KendaraanListAdapter(List<GKendaraanDetail> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public int getItemViewType(final int position) {
        return R.layout.list_kendaraan;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.getView().setText(data.get(position).ti_qrcode);
        if (!data.get(position).device_id.equalsIgnoreCase(PreferenceManager.getImei())){
            holder.frame_card_kendaraan.setBackground(ContextCompat.getDrawable(context, R.drawable.shape_faded));
        }
        holder.noPolisi.setText(data.get(position).ti_policeno);
        holder.kendaraan.setText(data.get(position).car_type);
        String[] dateTime = data.get(position).ti_datetime.split(" ");
        holder.timeDateIn.setText(dateTime[1]);
        holder.dateIn.setText(dateTime[0]);
        String packageParking = data.get(position).ti_package;
        if (packageParking.equalsIgnoreCase("vip")){
            holder.packagePark.setTextColor(Color.parseColor("#d9534f"));
        }else{
            holder.packagePark.setTextColor(Color.parseColor("#5cb85c"));
        }
        holder.packagePark.setText(packageParking);
        holder.frame_card_kendaraan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(view.getContext(), "posisition" + String.valueOf(position),Toast.LENGTH_LONG).show();
                Intent intent = new Intent(view.getContext(), DetailKendaraan.class);
                intent.putExtra("positions", String.valueOf(position));
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}