package com.rtech.rparkout.component.network.response;
import com.rtech.rparkout.component.network.gson.GSettingData;
import com.rtech.rparkout.component.network.gson.GSettingMessage;

import java.util.List;

/**
 * Created by Dhimas on 11/22/17.
 */

public class CekDeviceResponse {
    public boolean success;
    public String message;
    public GSettingData data;
}
