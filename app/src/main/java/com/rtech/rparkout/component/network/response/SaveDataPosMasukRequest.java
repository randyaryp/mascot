package com.rtech.rparkout.component.network.response;

public class SaveDataPosMasukRequest {
    String site_id;
    String device_id;
    String police_no;
    String vehicle;
    String datetimein;
    String parking_images;
    String lobby_id;
    String car_type;
    String car_color;
    String car_call;

    public SaveDataPosMasukRequest(String site_id,
            String device_id,
            String police_no,
            String vehicle,
            String datetimein,
            String parking_images,
            String lobby_id,
            String car_type,
            String car_color,
            String car_call) {
        this.device_id = device_id;
        this.site_id = site_id;
        this.police_no = police_no;
        this.datetimein = datetimein;
        this.vehicle = vehicle;
        this.parking_images = parking_images;
        this.lobby_id = lobby_id;
        this.car_type = car_type;
        this.car_color = car_color;
        this.car_call = car_call;
    }
}
