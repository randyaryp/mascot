package com.rtech.rparkout.component.network.response;

public class SaveDataPosKeluarRequest {
    String ticket_no;
    String user_id;
    String device_id;
    String site_id;
    String shift_id;
    String lobby_id;
    String datetimeout;
    String duration;
    String parking_fee;
    String isLT;
    String lt_charge;
    String payment_type;
    String cardno;

    public SaveDataPosKeluarRequest(
            String ticket_no,
            String user_id,
            String device_id,
            String site_id,
            String shift_id,
            String lobby_id,
            String datetimeout,
            String duration,
            String parking_fee,
            String isLT,
            String lt_charge,
            String payment_type,
            String cardno) {
        this.ticket_no = ticket_no;
        this.user_id = user_id;
        this.device_id = device_id;
        this.site_id = site_id;
        this.shift_id = shift_id;
        this.lobby_id = lobby_id;
        this.datetimeout = datetimeout;
        this.duration = duration;
        this.parking_fee = parking_fee;
        this.isLT = isLT;
        this.lt_charge = lt_charge;
        this.payment_type = payment_type;
        this.cardno = cardno;
    }
}
