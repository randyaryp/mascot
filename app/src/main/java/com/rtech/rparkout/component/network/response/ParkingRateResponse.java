package com.rtech.rparkout.component.network.response;
import com.rtech.rparkout.component.network.gson.GParkingFee;
import com.rtech.rparkout.component.network.gson.GParkingRateMessage;

import java.util.List;

/**
 * Created by Dhimas on 11/22/17.
 */

public class ParkingRateResponse {
//    public int content;
//    public int code;
//    public List<GParkingRateMessage> message;
    public boolean success;
    public String message;
    public String durasi1;
    public String durasi2;
    public String datetimein;
    public String datetimeout;
    public String policeno;
    public GParkingFee parking_fee;

}
