package com.rtech.rparkout.component.network.response;

/**
 * Created by Dhimas on 11/22/17.
 */

public class SaveTransactionRequest {
    String device_id;
    String site_id;
    String gate;
    String trans_id;
    String policeno;
    String policeno2;
    String vehicle;
    String rate;
    String payment;
    String tr_datetime;
//    "site_id": "SMB",
//            "gate":"1234",
//            "trans_id":"AHUISYIBJDE1",
//            "policeno":"B1234BB",
//            "vehicle":"MOBIL",
//            "rate":"5000",
//            "payment":"EMONEY",
//            "tr_datetime":"2020-07-24 15:00:00",
//            "device_id":"1234"

    public SaveTransactionRequest(String dev_id,
                                  String site,
                                  String gate_id,
                                  String trans,
                                  String polno,
                                  String polnoedited,
                                  String vehi,
                                  String rated,
                                  String pay,
                                  String tr_date) {
        this.device_id = dev_id;
        this.site_id = site;
        this.gate = gate_id;
        this.trans_id = trans;
        this.policeno = polno;
        this.policeno2 = polnoedited;
        this.vehicle = vehi;
        this.rate = rated;
        this.payment = pay;
        this.tr_datetime = tr_date;
    }
}
