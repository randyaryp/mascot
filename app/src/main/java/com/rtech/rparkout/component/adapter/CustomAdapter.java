package com.rtech.rparkout.component.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
//import com.rtech.onstreet.R;
//import com.rtech.onstreet.R;
import com.rtech.mascot.R;
import com.rtech.rparkout.component.network.gson.GParkingRateMessage;

import java.util.List;

public class CustomAdapter extends BaseAdapter {
    Context context;
    int flags[];
    List<GParkingRateMessage> parkingRate;
    LayoutInflater inflter;

    public CustomAdapter(Context applicationContext, int[] flags, List<GParkingRateMessage> parking) {
        this.context = applicationContext;
        this.flags = flags;
        this.parkingRate = parking;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return parkingRate.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.custom_spinner_items, null);
        ImageView icon = (ImageView) view.findViewById(R.id.imageView);
        TextView names = (TextView) view.findViewById(R.id.textView);
//        icon.setImageResource(flags[i]);
        names.setText(parkingRate.get(i).vehicle_id);
//        0. Mobil
//        1. Motor
//        2. BOX
//        3. Bus
//        4. pick up
//        5. truck
        if(parkingRate.get(i).vehicle_id.equalsIgnoreCase("BOX")){
            icon.setImageResource(flags[2]);
        }
        else if(parkingRate.get(i).vehicle_id.equalsIgnoreCase("MOBIL")){
            icon.setImageResource(flags[0]);
        }
        else if(parkingRate.get(i).vehicle_id.equalsIgnoreCase("MOTOR")){
            icon.setImageResource(flags[1]);
        }
        else if(parkingRate.get(i).vehicle_id.equalsIgnoreCase("BUS")){
            icon.setImageResource(flags[3]);
        }
        else if(parkingRate.get(i).vehicle_id.equalsIgnoreCase("PICK UP")){
            icon.setImageResource(flags[4]);
        }
        else if(parkingRate.get(i).vehicle_id.equalsIgnoreCase("TRUCK")){
            icon.setImageResource(flags[5]);
        }
        return view;
    }
}