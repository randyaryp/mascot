package com.rtech.rparkout.component.util;

import android.content.Context;

import com.orhanobut.hawk.Hawk;
import com.rtech.rparkout.component.network.gson.GLobby;
import com.rtech.rparkout.component.network.gson.GParkingRateMessage;
import com.rtech.rparkout.component.network.gson.GVehicle;
import com.rtech.rparkout.component.network.response.AuthResponse;
import com.rtech.rparkout.component.network.response.KendaraanListResponse;
import com.rtech.rparkout.component.network.response.PaymentTypeResponse;
import com.rtech.rparkout.component.network.response.SaveTransactionRequest;
import com.rtech.rparkout.component.network.response.TicketNoteResponse;
import com.rtech.rparkout.component.network.response.VehicleColorResponse;
import com.rtech.rparkout.component.network.response.VehicleTypeResponse;

import java.util.List;
//import GAgent;
//import com.pasyappagent.pasy.component.network.gson.GTopup;
//import com.pasyappagent.pasy.modul.scanqr.QuickResponse;

/**
 * Created by Dhimas on 10/9/17.
 */

public class PreferenceManager {

    private static final String SESSION_TOKEN = "sessionToken";
    private static final String IS_LOGIN = "isLogin";
    private static final String USER_LOGIN = "userLogin";
    private static final String AGENT = "agent";
    private static final String TOPUP = "topup";
    private static final String REFFERAL_ID = "refferalId";
    private static final String IS_AKUPAY = "isAkupay";
    private static final String QR_RESPONSE = "qrResponse";
    private static final String PARKING_ID = "parkingId";
    private static final String IMEI = "imei";
    private static final String AVATAR = "avatar";
    private static final String SAVED_TOKEN = "tokenfirebase";
    private static final String KEY_ACCESS = "token";
    private static final String FB_TOKEN = "firebasetoken";
    private static final String VA_BNI = "va_bni";
    private static final String PASSCODE = "passcode";
    private static final String ParkingRate = "parkingrate";
    private static final String LobbyList = "lobbylist";
    private static final String VehicleList = "vehiclelist";
    private static final String LoggedInLobbyID = "lobbyid";
    private static final String Location = "location";
    private static final String Locationname = "locationname";
    public static  final String listTrx = "listtrx";
    public static final String captureNoPol = "capturenopol";
    public static final String namaPrinter = "namaprinter";
    public static final String alamatPrinter = "alamatprinter";

    public static final String parkingticketno = "parkingticketno";
    public static final String loginauth = "loginauth";

    public static final String ticketnote = "ticketnote";
    public static final String vehicletype = "vehicletype";
    public static final String vehiclecolor = "vehiclecolor";
    public static final String paymenttype = "paymenttype";

    public static final String listkendaraaninside = "listkendaraaninside";
    public static final String defaultFragment = "defaultFragment";
    public static final String jenisTransaksiLogin = "jenisTransaksiLogin";

    private static Context ctx;
    private static PreferenceManager mInstance;

    public PreferenceManager(Context context) {
//        Hawk.init(context)
//                .setEncryptionMethod(HawkBuilder.EncryptionMethod.HIGHEST)
//                .setStorage(HawkBuilder.newSharedPrefStorage(context))
//                .setPassword("P@ssw0rd123")
//                .build();
        Hawk.init(context).build();
    }
    public static synchronized PreferenceManager getInstance(Context context){
        if (mInstance == null)
            mInstance = new PreferenceManager(context);
        return mInstance;
    }
    public static String getSessionToken() {
        return Hawk.get(SESSION_TOKEN, "");
    }

    public static void setSessionToken(String token) {
        Hawk.put(SESSION_TOKEN, token);
    }

    public static void logIn(String username) {
        Hawk.put(IS_LOGIN, true);
        Hawk.put(USER_LOGIN, username);

    }

    public static void setAvatar(String avatarString) {
        Hawk.put(AVATAR,avatarString);
    }

    public static String getAvatar() {
        return Hawk.get(AVATAR);
    }

    public static void logOut() {
        //Hawk.put(USER_LOGIN, null);
        Hawk.put(IS_LOGIN, false);
        Hawk.put(USER_LOGIN, null);
//        Hawk.put(Location, null);
//        Hawk.put(ParkingRate, null);
        Hawk.put(LoggedInLobbyID, null);
    }

    public static String getUser() {
        return Hawk.get(USER_LOGIN);
    }
    public static String getSite() {
        return Hawk.get(Location);
    }
    public static String getSiteName() {
        return Hawk.get(Locationname);
    }

    public static void setSite(String location, String locationname) {
        Hawk.put(Location, location);
        Hawk.put(Locationname, locationname);
    }

    public static Boolean isLogin() {
        return Hawk.get(IS_LOGIN, false);
    }

//    public static void setAgent(GAgent agent) {
//        Hawk.put(AGENT, agent);
//    }

//    public static GAgent getAgent() {
//        return Hawk.get(AGENT);
//    }

    public static String getPasscode() {
        return Hawk.get(PASSCODE);
    }

    public static void setPasscode(String passcode) {
        Hawk.put(PASSCODE, passcode);
    }

//    public static void setTopup(GTopup topup) {
//        Hawk.put(TOPUP, topup);
//    }

//    public static GTopup getTopup() {
//        return Hawk.get(TOPUP);
//    }

    public static void setRefferalId(String id) {
        Hawk.put(REFFERAL_ID, id);
    }

    public static String getRefferalId() {
        return Hawk.get(REFFERAL_ID,"");
    }

    public static void setSavedTransaction(List<SaveTransactionRequest> id) {
        Hawk.put(listTrx, id);
    }

    public static List<SaveTransactionRequest> getSavedTransaction() {
        return Hawk.get(listTrx);
    }

    public static void setStatusAkupay(boolean isAkupay) {
        Hawk.put(IS_AKUPAY, isAkupay);
    }

    public static boolean getStatusAkupay() {
        return Hawk.get(IS_AKUPAY, false);
    }

//    public static void setQrResponse(QuickResponse reseponse) {
//        Hawk.put(QR_RESPONSE, reseponse);
//    }

//    public static  QuickResponse getQrResponse() {
//        return Hawk.get(QR_RESPONSE);
//    }

    public static void setParkingId(String transactionId) {
        Hawk.put(PARKING_ID, transactionId);
    }

    public static String getParkingId() {
        return Hawk.get(PARKING_ID);
    }

    public static void setParkingRate(List<GParkingRateMessage> parkingRates) {
        Hawk.put(ParkingRate, parkingRates);
    }

    public static List<GParkingRateMessage> getParkingRate() {
        return Hawk.get(ParkingRate);
    }

    public static void setLobbyList(List<GLobby> lobbyList) {
        Hawk.put(LobbyList, lobbyList);
    }

    public static List<GLobby> getLobbyList() {
        return Hawk.get(LobbyList);
    }

    public static void setVehicleList(List<GVehicle> lobbyList) {
        Hawk.put(VehicleList, lobbyList);
    }

    public static List<GVehicle> getVehicleList() {
        return Hawk.get(VehicleList);
    }

    public static void emptyParkingId() {
        setParkingId("");
    }

    //public static boolean saveToken(String token){
    //    SharedPreferences sharedPreferences = ctx.getSharedPreferences(SAVED_TOKEN, Context.MODE_PRIVATE);
    //    SharedPreferences.Editor editor = sharedPreferences.edit();
    //    editor.putString(KEY_ACCESS, token);
    //    editor.apply();
    //    return true;
    //}

    //public String getToken(){
    //    SharedPreferences sharedPreferences = ctx.getSharedPreferences(SAVED_TOKEN, Context.MODE_PRIVATE);
    //    return sharedPreferences.getString(KEY_ACCESS.null)
    //}

    public static void setSavedToken(String token){
        Hawk.put(FB_TOKEN,token);
    }
    public static String getSavedToken() {
        return Hawk.get(FB_TOKEN);
    }

	public static String getImei() {
        return Hawk.get(IMEI);
    }

    public static void setImei(String imei) {
        Hawk.put(IMEI, imei);
    }

    public static String getLoggedInLobbyId() {
        return Hawk.get(LoggedInLobbyID);
    }

    public static void setLoggedInLobbyId(String passcode) {
        Hawk.put(LoggedInLobbyID, passcode);
    }

    public static String getCaptureNoPol() {
        return Hawk.get(captureNoPol);
    }

    public static void setCaptureNoPol(String passcode) {
        Hawk.put(captureNoPol, passcode);
    }

    public static void setNamaPrinter(String passcode) {
        Hawk.put(namaPrinter, passcode);
    }
    public static String getNamaPrinter() {
        return Hawk.get(namaPrinter);
    }

    public static void setAlamatPrinter(String passcode) {
        Hawk.put(alamatPrinter, passcode);
    }
    public static String getAlamatPrinter() {
        return Hawk.get(alamatPrinter);
    }

    public static void setParkingTicketNo(String passcode) {
        Hawk.put(parkingticketno, passcode);
    }
    public static String getParkingticketno() {
        return Hawk.get(parkingticketno);
    }

    public static void setLoginAuth(AuthResponse auth){
        Hawk.put(loginauth, auth);
    }
    public static AuthResponse getLoginAuth(){
        return Hawk.get(loginauth);
    }

    public static void setTicketnote(TicketNoteResponse ticketnot){
        Hawk.put(ticketnote, ticketnot);
    }
    public static TicketNoteResponse getTicketNote(){
        return Hawk.get(ticketnote);
    }

    public static void setVehicletype(VehicleTypeResponse ticketnots){
        Hawk.put(vehicletype, ticketnots);
    }
    public static VehicleTypeResponse getVehicleType(){
        return Hawk.get(vehicletype);
    }

    public static void setVehiclecolor(VehicleColorResponse ticketnots){
        Hawk.put(vehiclecolor, ticketnots);
    }
    public static VehicleColorResponse getVehicleColor(){
        return Hawk.get(vehiclecolor);
    }

    public static void setPaymenttype(PaymentTypeResponse ticketnots){
        Hawk.put(paymenttype, ticketnots);
    }
    public static PaymentTypeResponse getPaymenttype(){
        return Hawk.get(paymenttype);
    }

    public static void setListKendaraanInside(KendaraanListResponse kendaraanList){
        Hawk.put(listkendaraaninside, kendaraanList);
    }

    public static KendaraanListResponse getListKendaraanInside(){
        return Hawk.get(listkendaraaninside);
    }

    public static void setDefaultFragment(String kendaraanList){
        Hawk.put(defaultFragment, kendaraanList);
    }

    public static String getDefaultFragment(){
        return Hawk.get(defaultFragment);
    }

    public static void removeDefaultFragment(){
        Hawk.put(defaultFragment, "");
    }

    public static void setJenisTransaksiLogin(String kendaraanList){
        Hawk.put(jenisTransaksiLogin, kendaraanList);
    }

    public static String getJenisTransaksiLogin(){
        return Hawk.get(jenisTransaksiLogin);
    }
}
