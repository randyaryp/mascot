package com.rtech.rparkout.component.network.response;

/**
 * Created by Dhimas on 11/22/17.
 */

public class AuthRequest {
    String site_id;
    String user_id;
    String passwd;
    String device_id;
    int lobby_id;

    public AuthRequest(String siteid,
                       String userid,
                       String passwd, String device_id, int lobby_id) {
        this.site_id = siteid;
        this.user_id = userid;
        this.passwd = passwd;
        this.device_id = device_id;
        this.lobby_id = lobby_id;
    }
}
