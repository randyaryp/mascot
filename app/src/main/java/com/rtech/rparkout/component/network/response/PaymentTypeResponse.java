package com.rtech.rparkout.component.network.response;

import com.rtech.rparkout.component.network.gson.GPaymentType;

import java.util.List;

public class PaymentTypeResponse {
    public boolean success;
    public String message;
    public List<GPaymentType> data;
}
