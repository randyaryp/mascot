package com.rtech.rparkout.component.network.request;

public class KendaraanListRequest {
    String site_id;
    String lobby_id;

    public KendaraanListRequest(String site_id, String lobby_id) {
        this.site_id = site_id;
        this.lobby_id = lobby_id;
    }

}
