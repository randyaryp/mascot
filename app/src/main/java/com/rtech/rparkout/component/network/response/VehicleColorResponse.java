package com.rtech.rparkout.component.network.response;

import com.rtech.rparkout.component.network.gson.GVehicleColor;

import java.util.List;

public class VehicleColorResponse {
    public boolean success;
    public String message;
    public List<GVehicleColor> data;
}
