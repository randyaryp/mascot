package com.rtech.rparkout.component.network;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by Dhimas on 10/9/17.
 */

public class ResponeError {
    private static final String ERROR_BAD_REQUEST = "Ups.. Gagal nih.. Coba hubungi admin ya..";
    private static final String ERROR_UNEXPECTED = "Ups.. Gagal nih.. Coba hubungi admin ya..";
    private static final String ERROR_NO_NETWORK_CONNECTION = "Tidak ada koneksi internet, cek WiFi atau jaringan Internet kamu";

    public static String getErrorMessage(Throwable throwable) {
        throwable.printStackTrace();

//        Log.i("kunam reserror", throwable.getMessage());

        if (throwable instanceof IOException) return ERROR_NO_NETWORK_CONNECTION;

        if (throwable instanceof HttpException) {
//            Log.i("kunam", String.valueOf(((HttpException) throwable).code()));
            if (((HttpException) throwable).code() >= 400) {
                try {
                    return new JSONObject(((HttpException) throwable).response().errorBody().source().readUtf8()).getString("message");
                } catch (IOException | JSONException e) {
                    return ERROR_BAD_REQUEST;
                }
            }
        }

        return ERROR_UNEXPECTED;
    }

    public static String getErrorMessage(NetworkService mNetworkService ,Throwable throwable) {
        throwable.printStackTrace();
        if (throwable instanceof IOException) return ERROR_NO_NETWORK_CONNECTION;

        if (throwable instanceof HttpException) {
            if (((HttpException) throwable).code() >= 400) {
                if (((HttpException) throwable).code() == 403) {

                } else {
                    try {
                        return new JSONObject(((HttpException) throwable).response().errorBody().source().readUtf8()).getString("message");
                    } catch (IOException | JSONException e) {
                        return ERROR_BAD_REQUEST;
                    }
                }

            }
        }

        return ERROR_UNEXPECTED;
    }

    public static String getErrorTokenMessage(Throwable throwable) {
        throwable.printStackTrace();
        try {
            return throwable.getMessage().toString();
        } catch (Exception e) {
            return "";
        }
    }


}
