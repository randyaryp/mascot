package com.rtech.rparkout.component.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.rtech.mascot.R;
import com.rtech.rparkout.component.network.gson.GLobby;

import java.util.List;

public class LobbyListAdapter extends BaseAdapter {
    Context context;
    List<GLobby> countryNames;
    LayoutInflater inflter;

    public LobbyListAdapter(Context applicationContext, List<GLobby> countryNames) {
        this.context = applicationContext;
        this.countryNames = countryNames;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return countryNames.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.custom_spinner_items_no_icon, null);
        TextView names = (TextView) view.findViewById(R.id.textView);
        names.setText(countryNames.get(i).lobby_name);
        return view;
    }
}
